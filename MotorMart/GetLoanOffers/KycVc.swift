//
//  KycVc.swift
//  MotorMart
//
//  Created by Nennu on 14/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import AVFoundation
import GTSheet
class KycVc: UIViewController,QRCodeReaderViewControllerDelegate,XMLParserDelegate,HalfSheetPresentingProtocol {
    var transitionManager: HalfSheetPresentationManager!
    var parseDic:[String:String]?
    var userdetailsdic = [String:String]()
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        print(result.value)
        let file = "file.xml" //this is the file. we will write to and read from it
        
      //  let text = "some text"
 
        print(result.metadataType)
        dismiss(animated: true) { [weak self] in
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                
                let fileURL = dir.appendingPathComponent(file)
                
                //writing
                do {
                    try result.value.write(to: fileURL, atomically: false, encoding: .utf8)
                }
                catch {/* error handling here */}
                
                //reading
                do {
                    if let parser = XMLParser(contentsOf: fileURL) {
                        parser.delegate = self
                        parser.parse()
                    }
                    else
                    {
                        AlWrapper.showAlertMessage(vc: self!, titleStr: "Alert!", messageStr: "You Have Choosen Wrong QR Code!")
                        
                    }
                }
                catch {/* error handling here */}
            }
        }
    }
    
   
    
    @IBAction func didTapOnNextButton(_ sender: Any) {
   //     self.performSegue(withIdentifier: "personalDetailSegue", sender:nil)
        
        
        if let dic = self.parseDic{
            
            if enterPanCardNumTfd.text?.isEmpty != true
            {
                var dic1 = [String:String]()
                dic1 = dic
                dic1["PanNum"] = enterPanCardNumTfd.text
              //  dic["uid"] = adharCardNoTfd.te
                self.performSegue(withIdentifier: "personalDetailSegue", sender:dic1)
                
            }
            else
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Pan Card Number")
            }
            
           //  self.performSegue(withIdentifier: "personalDetailSegue", sender:dic)
        }
        else
        {
            var dic = [String:String]()
            if adharCardNoTfd.text?.isEmpty != true
            {
                if enterPanCardNumTfd.text?.isEmpty != true
                {
                    dic["PanNum"] = enterPanCardNumTfd.text
                    dic["uid"] = adharCardNoTfd.text
                    self.performSegue(withIdentifier: "personalDetailSegue", sender:dic)
                    
                }
                else
                {
                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Pan Card Number")
                }
                
            }
            else
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the aadhar details")
                
            }
            
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "KYC"
        adharCardNoTfd.keyboardType = UIKeyboardType.numberPad
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     //   print(sender)
        navigationItem.title = nil
        if segue.identifier == "personalDetailSegue"{
            let vc = segue.destination as! PersonalDetailsVc
            let keepingCurrent = userdetailsdic.merging(sender as! [String:String]) { (current, _) in current }
            print(keepingCurrent)
            vc.userdetailsdic = keepingCurrent
            //   vc.navigationItem.backBarButtonItem?.title = ""
        }
    }
    
    
    
  // Xml Parser
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        print(attributeDict)
        if elementName == "PrintLetterBarcodeData"
        {
            self.parseDic = attributeDict
        print(attributeDict["state"] as Any)
        var vc = UserProfile()
        vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserProfile") as! UserProfile
       vc.userdic = attributeDict
        presentUsingHalfSheet(
            vc
        )
        }
        else
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "You Have Choosen Wrong QR Code!")
        }
    }
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "You Have Choosen Wrong QR Code!")
    }
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    // MARK: - Actions
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    @IBOutlet weak var adharCardNoTfd: UITextField!
    
    @IBOutlet weak var enterPanCardNumTfd: UITextField!
    
    @IBOutlet weak var qrCodeScanImgView: UIImageView!
    
    @IBOutlet weak var qrCodeScanBgViewObj: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         AlWrapper.setupTextfieldLeftViewwithborder(imageString: "aadhar_card.jpg",textfield:adharCardNoTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "pan_card.jpg",textfield:enterPanCardNumTfd, backgroundcolor: UIColor.blue,backgroundbool: false)

    }
    @IBAction func scanAdharBtnClicked(_ sender: Any) {
        
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    


}

class UserProfile: UIViewController, HalfSheetPresentableProtocol,HalfSheetTopVCProviderProtocol {
    
    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }
    
    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()
    
    private var isKeyboardUp = false {
        didSet {
            let keyboardHeight: CGFloat = isKeyboardUp ? 300.0 : 0.0
            sheetHeight = 250.0 + keyboardHeight
            didUpdateSheetHeight()
        }
    }
    
    @IBOutlet weak var pincodeLable: UILabel!
    @IBOutlet weak var stateLable: UILabel!
    @IBOutlet weak var postOfficeLable: UILabel!
    @IBOutlet weak var currentCityLable: UILabel!
    @IBOutlet weak var disticNameLable: UILabel!
    @IBOutlet weak var houseNumberLable: UILabel!
    @IBOutlet weak var genderLable: UILabel!
    @IBOutlet weak var dobLable: UILabel!
    @IBOutlet weak var parentLable: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var subViewHeightConstraint: NSLayoutConstraint!
    var storedSheetHeight = CGFloat()
    var sheetHeight: CGFloat? = 500.0
    var userdic = [String:String]()
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        ["state": "Andhra Pradesh", "name": "Chalamalasetty Varun Pavan Kanth", "dist": "Krishna", "house": "20/352A", "street": "CHILAKALAPUDI", "gender": "M", "uid": "507710508202", "yob": "1993", "vtc": "Chilakalapudi (rural)", "lm": "OPP. PANDURANGA HIGH SCHOOL", "co": "S/O Chalamalasetty Nagamalleswararao", "loc": "CHILAKALAPUDI", "pc": "521002"]
        self.nameLable.text = userdic["name"]
         self.parentLable.text = userdic["co"]
         self.dobLable.text = userdic["yob"]
         self.genderLable.text = userdic["gender"]
         self.houseNumberLable.text = userdic["house"]
         self.disticNameLable.text = userdic["dist"]
         self.currentCityLable.text = userdic["vtc"]
         self.postOfficeLable.text = userdic[""]
         self.stateLable.text = userdic["state"]
         self.pincodeLable.text = userdic["pc"]
//        submitButton.layer.borderWidth = 0.8
//        submitButton.layer.cornerRadius = 10
//        submitButton.layer.borderColor = UIColor.lightGray.cgColor
        //  subView.isHidden = true
        //  sheetHeight = visittoSellerSwitch.frame.maxY + 100
        // didUpdateSheetHeight()
        
    }
    
    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }
    
    @IBAction func didTapOnSubbmitButton(_ sender: Any) {
    }
    @IBAction func dismiss() {
        dismiss(animated: true)
    }
    
    @IBAction func editingDidBegin(_ sender: Any) {
        isKeyboardUp = true
    }
    
    @IBAction func editingDidEnd(_ sender: Any) {
        isKeyboardUp = false
        //        sheetHeight = storedSheetHeight
        //        didUpdateSheetHeight()
    }
}










