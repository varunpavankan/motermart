//
//  LoanListCell.swift
//  MotorMart
//
//  Created by Nennu on 18/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class LoanListCell: UITableViewCell {

    @IBOutlet weak var getapprovedButton: UIButton!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var amortizationButton: UIButton!
    @IBOutlet weak var loanAmountlable: UILabel!
    
    @IBOutlet weak var otherChargesLable: UILabel!
    @IBOutlet weak var processingFeeLable: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var intrestLable: UILabel!
    @IBOutlet weak var tenureLable: UILabel!
    @IBOutlet weak var emiLable: UILabel!
    @IBOutlet weak var downPaymentLable: UILabel!
    @IBOutlet weak var bankNameLable: UILabel!
    @IBOutlet weak var bankImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
