//
//  LoansListVC.swift
//  MotorMart
//
//  Created by Nennu on 18/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
import GTSheet


class LoansListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,HalfSheetPresentingProtocol{
    @IBOutlet weak var filterButton: UIButton!
    
    @IBOutlet weak var detailsHeaderView: UIView!
    @IBOutlet weak var hraderLable: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var comparButton: UIButton!
    @IBOutlet weak var priceLable: UILabel!
    @IBOutlet weak var colorLable: UILabel!
    @IBOutlet weak var modelLable: UILabel!
    @IBOutlet weak var makeLable: UILabel!
    let setselectediteamsdetails = selectedIteamLaonDetails()
    var butindexPath = IndexPath()
    var addDeleteBtnArr = [Int]()
    var loanlist = JSON()
    var displayArray = JSON()
    var productDetails = [String:String]()
    var transitionManager: HalfSheetPresentationManager!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(loanlist)
        self.priceLable.text = productDetails["Price"]
        self.makeLable.text = productDetails["MaKe"]
        self.modelLable.text = productDetails["Model"]
         self.colorLable.text = productDetails["Color"]
        filterButton.layer.cornerRadius = filterButton.frame.size.width/2
        filterButton.clipsToBounds = true
        
        detailsHeaderView.layer.borderColor = UIColor.lightGray.cgColor
        detailsHeaderView.layer.borderWidth = 0.50
        detailsHeaderView.layer.cornerRadius = 2
        detailsHeaderView.clipsToBounds = true
        
        hraderLable.text = "Hurry ! we found \(loanlist.count) loan offers for you"
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterApply), name: NSNotification.Name(rawValue: "FilterApply"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.filterCancle), name: NSNotification.Name(rawValue: "FilterCancle"), object: nil)
            displayArray = loanlist
        // Do any additional setup after loading the view.
    }
    @objc func filterCancle(notification: NSNotification)
    {
    
        displayArray = JSON.null
        displayArray = loanlist
        hraderLable.text = "Hurry ! we found \(displayArray.count) loan offers for you"
        self.tableView.reloadData()
        
        
    }
    @objc func filterApply(notification: NSNotification)
    {
//        ["FliterbyDownPayment":downPaymentSlider.value,"FilterByTenure":tenureInMonthSlider.value,"tenureBool":tenureBool,"downpaymentBool":downpaymentBool]
        print(notification.userInfo as Any)
        let dic = notification.userInfo
        let tenurefilter = dic!["FilterByTenure"] as! Float
        let tenureBool = dic!["tenureBool"] as! Bool
        
        let downpaymentBool = dic!["downpaymentBool"] as! Bool
        
        if tenureBool == true
        {
        let dic1 = loanlist.array
        let filterArray = dic1?.filter { ($0["Term_in_Months"].string ) == "\(Int(tenurefilter))"}
        print(filterArray as Any)
      //  let filterBool = true
        displayArray = JSON.null
        displayArray = JSON(filterArray as Any)
            
        }
        if downpaymentBool == true
        {
            let dowpaymentamount = dic!["FliterbyDownPayment"] as! Float
            print(displayArray)
            var i = 0
            for subdic in displayArray.array!
            {
                
    let downpaymentpersentage =  (dowpaymentamount / (Float(subdic["GROSS_AMOUNT"].string!)!)) * 100
                print(downpaymentpersentage.roundTo(places: 2))
                displayArray[i]["Down_payment_Pecntage"].string = "\(downpaymentpersentage)"
                
                i = i + 1
                
            }
          //  print(downpaymentpersentage)
           
        }
         hraderLable.text = "Hurry ! we found \(displayArray.count) loan offers for you"
        self.tableView.reloadData()
        

    }

    
    @IBAction func didTapOnComparButton(_ sender: UIButton) {
        
        if addDeleteBtnArr.count < 2
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Reqiured minimum two to compar!")
        }
        else
        {
            var senderarray = [[String]]()
             let titleDic = ["Loan Criteria","Loan Amount","Interest Rate(%)","Tenure(in months)","Down payment","LTV","EMI","Document Charges","Processing Charges","Stamp Cost","Other cost","Foreclosure charges(in %)","Pre payment penalties","Checque bounce charges","Seizure penalty","Guarantor requirement","Total Interest Payable","Total Other one time costs"]
            senderarray.append(titleDic)
        for i in addDeleteBtnArr
        {
           
            let dic = displayArray[i]
            let array = getcompardatStructer(dic: dic)
            senderarray.append(array)
            print(dic)
        }
        self.performSegue(withIdentifier: "ComparDetailsSegueVC", sender:senderarray)
        }
    }
func setselectedLoandeatails(dic:JSON)
{
    
    let downpayment = (Float(dic["GROSS_AMOUNT"].string!)!/100) * Float(dic["Down_payment_Pecntage"].string!)!
    let loanamount = Float(dic["GROSS_AMOUNT"].string!)! - downpayment
    let douAmt = Float(dic["Docmnt_Chrgs_absolute_Amnt"].string!)!
    let stampCost = Float(dic["Stamp_Costs"].string!)!
    let otherCost = Float(dic["Other_Costs"].string!)!
    let Insurance = Float(dic["Insurance"].string!)!
    let processingfee = (loanamount * Float(dic["Processing_Chrgs_Percmtge_loan_amnt"].string!)!) / 100;
    let loanAmmount =  loanamount + douAmt + processingfee + stampCost + otherCost + Insurance
    appdelegate?.selectedIteamlaondetails.LoanAmount = "\(loanAmmount)"
     appdelegate?.selectedIteamlaondetails.ProcessingCharges = "\(processingfee)"
    appdelegate?.selectedIteamlaondetails.DocumentCharges = "\(douAmt)"
      appdelegate?.selectedIteamlaondetails.StampCost = "\(stampCost)"
      appdelegate?.selectedIteamlaondetails.Othercost = "\(douAmt + processingfee + stampCost + otherCost + Insurance)"
    appdelegate?.selectedIteamlaondetails.InterestRate = "\(dic["Interest_Rate_In_Pecntge"].string!)"
    appdelegate?.selectedIteamlaondetails.Tenure = "\(dic["Term_in_Months"].string!)"
    
      appdelegate?.selectedIteamlaondetails.Downpayment = "\(downpayment)"
    
 //  setselectediteamsdetails.othersCharges = douAmt  + stampCost + otherCost + Insurance
    var emi = Float()
    var totalamount = Float()
    if dic["interset_type"].string == "3"
    {
        emi = AlWrapper.emiCalculation_flaterate_typ(loanAmount: loanAmmount, intrest: Float(dic["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(dic["Term_in_Months"].string!)!)
        totalamount = emi *  (Float(dic["Term_in_Months"].string!)!)
        appdelegate?.selectedIteamlaondetails.EMI = "\(emi)"
    }
    else
    {
        emi = AlWrapper.emiCalculation_Reducing_balance_type(loanAmount: loanAmmount, intrest: Float(dic["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(dic["Term_in_Months"].string!)!)
        totalamount = emi *  (Float(dic["Term_in_Months"].string!)!)
        appdelegate?.selectedIteamlaondetails.EMI = "\(emi)"
        
    }
    appdelegate?.selectedIteamlaondetails.TotalInterestPayable = "\(Float(totalamount) - loanAmmount)"
    appdelegate?.selectedIteamlaondetails.Checquebouncecharges  = dic["Cheque_Bounce_Fee"].string!
    appdelegate?.selectedIteamlaondetails.Prepaymentpenalties =  dic["Pre_payment_penalty"].string!
   appdelegate?.selectedIteamlaondetails.Seizurepenalty =  (dic["Seizure_Penalty"].string!)
    appdelegate?.selectedIteamlaondetails.Foreclosurecharges =  (dic["Foreclosure_Charges"].string!)
    //    let Cheque_Bounce_Charges =  (dic["Cheque_Bounce_Fee_Per_day"].string!)
    //   let Term_or_Length_of_Loan =  dic["Term_in_Months"].string!
    appdelegate?.selectedIteamlaondetails.Guarantorrequirement =  dic["Guarantor_Needed"].string!
    
    appdelegate?.selectedIteamlaondetails.loanOrgnizationName = dic["Organization_Name"].string!
    
//     let array2 = ["\(dic["Organization_Name"].string!)","\(Int(loanAmmount))","\(dic["Interest_Rate_In_Pecntge"].string!)","\(dic["Term_in_Months"].string!)","\(downpayment)","90","\(emi)","\(douAmt)","\(processingfee)","\(stampCost)","\(otherCost)",Foreclosure_Charges,Pre_payment_penalties,checkbounce,Seizure_penalty,Guarantor_requirement,"\(TotalInterest)","\(othercharges)"]
    
    }
    
    func getcompardatStructer(dic:JSON)-> [String]
{
    let downpayment = (Float(dic["GROSS_AMOUNT"].string!)!/100) * Float(dic["Down_payment_Pecntage"].string!)!
    let loanamount = Float(dic["GROSS_AMOUNT"].string!)! - downpayment
    let douAmt = Float(dic["Docmnt_Chrgs_absolute_Amnt"].string!)!
    let stampCost = Float(dic["Stamp_Costs"].string!)!
    let otherCost = Float(dic["Other_Costs"].string!)!
    let Insurance = Float(dic["Insurance"].string!)!
    let processingfee = (loanamount * Float(dic["Processing_Chrgs_Percmtge_loan_amnt"].string!)!) / 100;
    let loanAmmount =  loanamount + douAmt + processingfee + stampCost + otherCost + Insurance
    let othercharges = douAmt  + stampCost + otherCost + Insurance
    var emi = Float()
    var totalamount = Float()
    if dic["interset_type"].string == "3"
    {
        emi = AlWrapper.emiCalculation_flaterate_typ(loanAmount: loanAmmount, intrest: Float(dic["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(dic["Term_in_Months"].string!)!)
          totalamount = emi *  (Float(dic["Term_in_Months"].string!)!)
    }
    else
    {
        emi = AlWrapper.emiCalculation_Reducing_balance_type(loanAmount: loanAmmount, intrest: Float(dic["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(dic["Term_in_Months"].string!)!)
          totalamount = emi *  (Float(dic["Term_in_Months"].string!)!)
        
    }
     let TotalInterest = Float(totalamount) - loanAmmount
    let checkbounce = dic["Cheque_Bounce_Fee"].string!
   let Pre_payment_penalties =  dic["Pre_payment_penalty"].string!
    let Seizure_penalty =  (dic["Seizure_Penalty"].string!)
       let Foreclosure_Charges =  (dic["Foreclosure_Charges"].string!)
//    let Cheque_Bounce_Charges =  (dic["Cheque_Bounce_Fee_Per_day"].string!)
//   let Term_or_Length_of_Loan =  dic["Term_in_Months"].string!
    let Guarantor_requirement =  dic["Guarantor_Needed"].string!
    
    let array2 = ["\(dic["Organization_Name"].string!)","\(Int(loanAmmount))","\(dic["Interest_Rate_In_Pecntge"].string!)","\(dic["Term_in_Months"].string!)","\(downpayment)","90","\(emi)","\(douAmt)","\(processingfee)","\(stampCost)","\(otherCost)",Foreclosure_Charges,Pre_payment_penalties,checkbounce,Seizure_penalty,Guarantor_requirement,"\(TotalInterest)","\(othercharges)"]
    
    return array2
    
    }
    
    @IBAction func didTapOnFilterButton(_ sender: Any) {
        
      //  FilterVCSegue
        
        let mapArray = loanlist.array?.map{ $0["Down_payment_Pecntage"] }
        print(mapArray as Any)
        let minValue = mapArray?.min()
       
        let index = mapArray?.index(of:minValue!)
        let listdownPaymentValue = getcompardatStructer(dic: loanlist[index!])[4]
        let minumTenureMonths = loanlist.array?.map{ $0["Term_in_Months"] }.min()?.string
        let maxTenureMonths = loanlist.array?.map{ $0["Term_in_Months"] }.max()?.string
        
let dic = ["MinumtenureMonths":minumTenureMonths!,"MaximumTenureMonths":maxTenureMonths!,"Listdownpayment":listdownPaymentValue,"GrossAmount":productDetails["Price"]!] as [String : Any]
  
   //     print(minimumvalue)
        self.performSegue(withIdentifier: "FilterVCSegue", sender:dic)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapOnCheckButton(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to:self.tableView )
        butindexPath = tableView.indexPathForRow(at: buttonPosition)!
        if addDeleteBtnArr.contains(butindexPath.row)
        {
            let index = addDeleteBtnArr.index(of: butindexPath.row)
            self.addDeleteBtnArr.remove(at:index!)
        }
        else
        {
            if addDeleteBtnArr.count >= 3
            {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Maximum 3 can be choosen to compar")
            }
            else
            {
                self.addDeleteBtnArr.append(butindexPath.row)
            }
        }
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Loans List"
    }
    //EMI caluclation
    func emiCalculation_flaterate_typ(loanAmount:Float,intrest:Float,numberOfmonths:Int) -> Int
    {
     
        let TotalInterest = (loanAmount * intrest/100 * Float(numberOfmonths)) / 12;
        let TotalPayable = loanAmount + TotalInterest;
        let EMI = TotalPayable / Float(numberOfmonths);
        return Int(EMI);
       // return d
    }
    func emiCalculation_Reducing_balance_type(loanAmount:Float,intrest:Float,numberOfmonths:Int) -> Int
    {
        
        
        
        let noof_months = numberOfmonths * 12;
         print(noof_months)
        //Calculate interest  rate per month
        let permonth = (intrest / 100) / 12;
         print(permonth)
//        Decimal Interestrate_powerterms = (decimal)Math.Pow((double)(1+_permonth), (double)noof_terms);///(1 + _permonth) * noof_months;
        
        let Interestrate_powerterms = Float(pow(Double(1+permonth), Double(numberOfmonths)))
        print(Interestrate_powerterms)
        let payment = (Float(loanAmount) * (Interestrate_powerterms * Float(permonth)) ) / (( Interestrate_powerterms) - 1);
       //  print(payment)
        return Int(payment);
        // return d
    }
    
    
    @IBAction func didTapOnViewButton(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to:self.tableView )
        butindexPath = tableView.indexPathForRow(at: buttonPosition)!
        var vc = LoandetailsView()
        vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoandetailsView") as! LoandetailsView
        vc.parsedataList = displayArray[butindexPath.row]
        presentUsingHalfSheet(
            vc
        )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AmortizationSegue"
        {
             let vc = segue.destination as! Amortization
            vc.productDetails = sender as! JSON
        }
        else if segue.identifier == "ComparDetailsSegueVC"
        {
            let vc = segue.destination as! ComparDetailsVC
            vc.datadic = sender as! [[String]]
        }
        else if segue.identifier == "FilterVCSegue"
        {
            let vc = segue.destination as! FilterVC
            vc.dic = sender as! [String:Any]
        }
        
    }
    
    @IBAction func didTapOnamortizationButton(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to:self.tableView )
        butindexPath = tableView.indexPathForRow(at: buttonPosition)!
        
         self.performSegue(withIdentifier: "AmortizationSegue", sender:displayArray[butindexPath.row])
    }
    
    
    @IBAction func didTapOnGetapprovedButton(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to:self.tableView )
        butindexPath = tableView.indexPathForRow(at: buttonPosition)!
        self.setselectedLoandeatails(dic:  displayArray[butindexPath.row])
        appdelegate?.seteselectedbankdeails.selectedBankDetails = displayArray[butindexPath.row]
      //  preapprovalSegue
        let getproductdetais = setproductdetails()
        let productList =  appdelegate?.setproductDetails.productdetails 
        let parseDatadic = appdelegate?.parseDatadic
        let str = "_usercode=\(UserDefaults.standard.string(forKey: "_User_Id")!)&EmailId=\(UserDefaults.standard.string(forKey: "_EmailId")!)&mobilenumber=\(UserDefaults.standard.string(forKey: "_Mobile_No")!)&Productid=\(productList!["prodId"].string!)&Modelid=\(parseDatadic!["modelId"] ?? "1")"
        
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_LoanQuestion_Details?"+str, viewController: self, success: { (json2,responce) in
            print(json2)
            if json2.isEmpty == true
            {
            }
            else
            {
                appdelegate?.setloanQuestiondetails.Loanquestiondetails = json2[0]
                self.performSegue(withIdentifier: "preapprovalSegue", sender:nil)
            }
        }, failure: { (error) in
            
        })

        
    }
    
    
    // MARK: - TableView delegate and data source

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (displayArray.array?.count)!
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:LoanListCell = self.tableView.dequeueReusableCell(withIdentifier: "LoanListCell") as! LoanListCell
        let dic = displayArray[indexPath.row]
        let url = URL(string: "http://dev.loan2buy.com"+(dic["Business_Logo"].string)!)
         cell.bankImageView.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        cell.bankNameLable.text = dic["Organization_Name"].string
        let str = dic["GROSS_AMOUNT"].string!
        print(str)
        
        let downpayment = (Float(dic["GROSS_AMOUNT"].string!)!/100) * Float(dic["Down_payment_Pecntage"].string!)!
        let loanamount = Float(dic["GROSS_AMOUNT"].string!)! - downpayment
        cell.downPaymentLable.text = "\(Int(downpayment))"
        cell.tenureLable.text =  dic["Term_in_Months"].string!
        
        let douAmt = Float(dic["Docmnt_Chrgs_absolute_Amnt"].string!)!
        let stampCost = Float(dic["Stamp_Costs"].string!)!
        let otherCost = Float(dic["Other_Costs"].string!)!
        let Insurance = Float(dic["Insurance"].string!)!
        let processingfee = (loanamount * Float(dic["Processing_Chrgs_Percmtge_loan_amnt"].string!)!) / 100;
        let loanAmmount =  loanamount + douAmt + processingfee + stampCost + otherCost + Insurance
        let othercharges = douAmt  + stampCost + otherCost + Insurance
        if dic["interset_type"].string == "3"
        {
             let emi = AlWrapper.emiCalculation_flaterate_typ(loanAmount: loanAmmount, intrest: Float(dic["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(dic["Term_in_Months"].string!)!)
            cell.emiLable.text = "\(emi)"
        }
        else
        {
             let emi = AlWrapper.emiCalculation_Reducing_balance_type(loanAmount: loanAmmount, intrest: Float(dic["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(dic["Term_in_Months"].string!)!)
            cell.emiLable.text = "\(emi)"
            
        }
        
        if addDeleteBtnArr.contains(indexPath.row)
        {
            // cell.add.image = UIImage(named: "RemoveImg.png")!
            cell.checkButton.setImage(UIImage(named: "check.png"), for: .normal)
            
            
        }else{
            cell.checkButton.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
        cell.intrestLable.text = "\(dic["Interest_Rate_In_Pecntge"].string!) %"
        cell.loanAmountlable.text = "\(Int(loanAmmount))"
       cell.processingFeeLable.text = "\(Int(processingfee))"
        cell.otherChargesLable.text =   "\(Int(othercharges))"
        
        print(dic)
     return cell
        
    }
    
//    "Term_in_Months" : "36",
//    "Foreclosure_Charges" : "6.50",
//    "Down_payment_Pecntage" : "10.00",
//    "Insurance" : "4000.00",
//    "Processing_Chrgs_Percmtge_loan_amnt" : "2.50",
//    "GROSS_AMOUNT" : "62551.00",
//    "Business_Logo" : "\/uploads\/",
//    "Organization_Name" : "Navhind Society",
//    "ciRate" : null,
//    "Name_Of_Loan" : "Two wheeler Mela",
//    "IsNo" : "1320",
//    "Stamp_Costs" : "100.00",
//    "Pre_payment_penalty" : "YES",
//    "cTerms" : null,
//    "Product_id" : "1146",
//    "Cheque_Bounce_Fee_Per_day" : "10.00",
//    "Guarantor_Needed" : "YES",
//    "user_code" : "LID20181381",
//    "Product_Id" : null,
//    "Docmnt_Chrgs_absolute_Amnt" : "600.00",
//    "Cheque_Bounce_Fee" : "700.00",
//    "Seizure_Penalty" : "4000.00",
//    "CDPayment" : null,
//    "Interest_Rate_In_Pecntge" : "11.00",
//    "Reducing_balance" : "Flat Rate",
//    "Other_Costs" : "0.00",
//    "interset_type" : "3",
//    "Sno" : "1609"

    
    
    
    
    /*
    // MARK: - TableView delegate and data source

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class viewdetailsCell: UITableViewCell {
    
    @IBOutlet weak var typeLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var ValueLable: UILabel!
    
    @IBOutlet weak var typelablewidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class viewdetailsHeaderCell: UITableViewCell {
    
    @IBOutlet weak var hraderView: UIView!
    @IBOutlet weak var headerLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class LoandetailsView : UIViewController, HalfSheetPresentableProtocol, HalfSheetTopVCProviderProtocol ,UITableViewDelegate,UITableViewDataSource
{
    var parsedataList = JSON()
    var headerLableArray = ["Processing Charges","Other Charges","Fees,Penalty & Cost","Down Payment & Terms","Eligibility Criteria"]
    var processingCharges = ["Processing Charges"]
    var OtherCharges = ["Document Charges","Stamp Cost","Others Cost","Insurance"]
    var feePenalty = ["Cheque Bounce Fee","Pre-payment penalties","Seizure penalty","Foreclosure Charges in %","Cheque Bounce Charges"]
    var Dowpayment = ["Dowm Payment %","Term or Length of Loan","Guarantor requirement"]
    var eligibilityCriteri = ["Age at least 21 and less than 65","Minimum income required Rs.15,000 per month"]
    var sellerId = String()
    var productId = String()
    var sentArray = Array<[String:String]>()
    
    @IBAction func didTaponSaveButton(_ sender: Any) {

    }
  
    @IBOutlet weak var tableView: UITableView!
    
    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }
    
    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()
    
    private var isKeyboardUp = false {
        didSet {
            let keyboardHeight: CGFloat = isKeyboardUp ? 300.0 : 0.0
            sheetHeight = 250.0 + keyboardHeight
            didUpdateSheetHeight()
        }
    }
    
    
    var storedSheetHeight = CGFloat()
    var sheetHeight: CGFloat? = 500
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        headerLableArray = ["Processing Charges","Other Charges","Fees,Penalty & Cost","Down Payment & Terms","Eligibility Criteria"]
        
    }
    override func viewWillAppear(_ animated: Bool) {
    //    print(parsedataList)
        
    }
   
    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = self.tableView.dequeueReusableCell(withIdentifier: "viewdetailsHeaderCell") as? viewdetailsHeaderCell
        headerView?.headerLable.text = headerLableArray[section]
        return headerView?.contentView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerLableArray.count
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
       return 44
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return processingCharges.count
        case 1:
            return OtherCharges.count
        case 2:
            return feePenalty.count
        case 3:
            return Dowpayment.count
        case 4:
            return eligibilityCriteri.count
        default:
         return   2
        }
       // return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:viewdetailsCell = self.tableView.dequeueReusableCell(withIdentifier:"viewdetailsCell") as! viewdetailsCell
            let dic = parsedataList
        let downpayment = (Float(dic["GROSS_AMOUNT"].string!)!/100) * Float(dic["Down_payment_Pecntage"].string!)!
        let loanamount = Float(dic["GROSS_AMOUNT"].string!)! - downpayment
        let douAmt = Float(dic["Docmnt_Chrgs_absolute_Amnt"].string!)!
        let stampCost = Float(dic["Stamp_Costs"].string!)!
        let otherCost = Float(dic["Other_Costs"].string!)!
        let Insurance = Float(dic["Insurance"].string!)!
        let processingfee = (loanamount * Float(dic["Processing_Chrgs_Percmtge_loan_amnt"].string!)!) / 100;
          let loanAmmount =  loanamount + douAmt + processingfee + stampCost + otherCost + Insurance
        let othercharges = douAmt  + stampCost + otherCost + Insurance
        print(dic)
        switch indexPath.section {
        case 0:
            cell.typeLable.text = processingCharges[indexPath.row]
            
            cell.ValueLable.text = "\(Int(processingfee))"
           // return processingCharges.count
        case 1:
            cell.typeLable.text = OtherCharges[indexPath.row]
            switch OtherCharges[indexPath.row]
            {
            case "Document Charges":
                cell.ValueLable.text = "\(Int(douAmt))"
            case "Stamp Cost":
                cell.ValueLable.text =  "\(Int(stampCost))"
            case "Others Cost":
                cell.ValueLable.text =  "\(Int(otherCost))"
            case "Insurance":
                cell.ValueLable.text =  "\(Int(Insurance))"
                
            default:
                print("default")
            }
            
           // return OtherCharges.count
        case 2:
//              var feePenalty = ["Cheque Bounce Fee","Pre-payment penalties","Seizure penalty","Foreclosure Charges in %","Cheque Bounce Charges"]
            cell.typeLable.text = feePenalty[indexPath.row]
            switch feePenalty[indexPath.row]
            {
            case "Cheque Bounce Fee":
                cell.ValueLable.text = dic["Cheque_Bounce_Fee"].string!
            case "Pre-payment penalties":
                cell.ValueLable.text =  dic["Pre_payment_penalty"].string!
            case "Seizure penalty":
                cell.ValueLable.text =  (dic["Seizure_Penalty"].string!)
            case "Foreclosure Charges in %":
                cell.ValueLable.text =  (dic["Foreclosure_Charges"].string!)
            case "Cheque Bounce Charges":
                cell.ValueLable.text =  (dic["Cheque_Bounce_Fee_Per_day"].string!)
                
            default:
                print("default")
            }
           // cell.ValueLable.text = "1000"
          //  return feePenalty.count
        case 3:
            cell.typeLable.text = Dowpayment[indexPath.row]
           //   var Dowpayment = ["Dowm Payment %","Term or Length of Loan","Guarantor requirement"]
            switch Dowpayment[indexPath.row]
            {
            case "Dowm Payment %":
                cell.ValueLable.text = dic["Down_payment_Pecntage"].string!
            case "Term or Length of Loan":
                cell.ValueLable.text =  dic["Term_in_Months"].string!
            case "Guarantor requirement":
                cell.ValueLable.text =  dic["Guarantor_Needed"].string!
                
            default:
                print("default")
            }
           // cell.ValueLable.text = "1000"
            //return Dowpayment.count
        case 4:
            cell.typeLable.text = eligibilityCriteri[indexPath.row]
            cell.ValueLable.isHidden = true
            cell.widthConstraint.constant = 0
            cell.typelablewidthConstraint.constant = cell.contentView.frame.size.width
            //return eligibilityCriteri.count
        default:
            print("deafult")
        }
        
       // cell.typeLable.text
        
        return cell
        
    }
    
}











