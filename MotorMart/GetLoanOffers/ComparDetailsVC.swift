//
//  ComparDetailsVC.swift
//  MotorMart
//
//  Created by Nennu on 21/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class ComparDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    var datadic = [[String]]()
    var titlearray = [String]()
    var bank1array = [String]()
    var bank2array = [String]()
    var bank3array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(datadic)
       
        titlearray = datadic[0]
        bank1array = datadic[1]
        bank2array = datadic[2]
        if datadic.count > 3
        {
            bank3array = datadic[3]
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "LOAN COMPARISON DETAILS"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = self.tableView.dequeueReusableCell(withIdentifier: "ComparDetailsHeaderCell") as? ComparDetailsHeaderCell
        headerView?.title.layer.borderColor = UIColor.black.cgColor
        headerView?.title.layer.borderWidth = 0.5
        headerView?.title.text = titlearray[0]
        headerView?.Bank1.layer.borderColor = UIColor.black.cgColor
        headerView?.Bank1.layer.borderWidth = 0.5
        headerView?.Bank1.text = bank1array[0]
        headerView?.Bank2.layer.borderColor = UIColor.black.cgColor
        headerView?.Bank2.layer.borderWidth = 0.5
         headerView?.Bank2.text = bank2array[0]
        headerView?.Bank3.layer.borderColor = UIColor.black.cgColor
        headerView?.Bank3.layer.borderWidth = 0.5
        if bank3array.count == 0
        {
             headerView?.Bank3.isHidden = true
        }
        else
        {
             headerView?.Bank3.text = bank3array[0]
            
        }
       
        return headerView?.contentView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return titlearray.count - 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ComparDetailsCell = self.tableView.dequeueReusableCell(withIdentifier:"ComparDetailsCell") as! ComparDetailsCell
        cell.title.layer.borderColor = UIColor.black.cgColor
        cell.title.layer.borderWidth = 0.5
        cell.title.text = titlearray[indexPath.row + 1]
        cell.Bank1.layer.borderColor = UIColor.black.cgColor
        cell.Bank1.layer.borderWidth = 0.5
        cell.Bank1.text = bank1array[indexPath.row + 1]
        cell.Bank2.layer.borderColor = UIColor.black.cgColor
        cell.Bank2.layer.borderWidth = 0.5
         cell.Bank2.text = bank2array[indexPath.row + 1]
        cell.Bank3.layer.borderColor = UIColor.black.cgColor
        cell.Bank3.layer.borderWidth = 0.5
        if bank3array.count == 0
        {
            cell.Bank3.isHidden = true
        }
        else
        {
            cell.Bank3.text = bank3array[indexPath.row + 1]
            
        }
        
       // cell.Bank3.isHidden = true
        
        return cell
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class ComparDetailsCell: UITableViewCell {
    
    @IBOutlet weak var Bank1: UILabel!
    @IBOutlet weak var Bank2: UILabel!
    
    @IBOutlet weak var Bank3: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class ComparDetailsHeaderCell: UITableViewCell {
    @IBOutlet weak var Bank1: UILabel!
    @IBOutlet weak var Bank2: UILabel!
    
    @IBOutlet weak var Bank3: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    
}
