//
//  GetpreapprovalVC.swift
//  MotorMart
//
//  Created by Nennu on 22/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class GetpreapprovalVC: UIViewController {
  
    @IBOutlet weak var numberOfbependesTxfld: UITextField!
    @IBOutlet weak var enterMothersNameTxfld: UITextField!
    
    @IBOutlet weak var enterFathersTxfld: UITextField!
    
    @IBOutlet weak var coApplicationNoRadioButton: KGRadioButton!
    @IBOutlet weak var coApplicationYesRadioButton: KGRadioButton!
    @IBOutlet weak var eduOthersRadioButton: KGRadioButton!
    @IBOutlet weak var postGraduateRadioButton: KGRadioButton!
    
    @IBOutlet weak var graduateRadioButton: KGRadioButton!
    
    @IBOutlet weak var underGraduationRadioButton: KGRadioButton!
    
    @IBOutlet weak var marriedRadioButton: KGRadioButton!
    @IBOutlet weak var singleRadioButton: KGRadioButton!
    @IBOutlet weak var aadharCardRadioButton: KGRadioButton!
    
    @IBOutlet weak var coApplicantIncomeTxfld: UITextField!
    
    @IBOutlet weak var coApllicantaddressTxfld: UITextField!
    @IBOutlet weak var coApplicantNameTxfld: UITextField!
    @IBOutlet weak var coapplicantView: UIView!
    
    @IBOutlet weak var coapplicatHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var voterIdRadioButton: KGRadioButton!
    
    @IBOutlet weak var panRadioButton: KGRadioButton!
    @IBOutlet weak var nationalityOthersRadioButton: KGRadioButton!
    @IBOutlet weak var indiaRadioButton: KGRadioButton!
     var loanapproveldata = loanapproval()
    var nataionality = String()
    var proof = String()
    var Status = String()
    var educolification = String()
    var coapplicant = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indiaRadioButton.isSelected = true
        panRadioButton.isSelected = true
        singleRadioButton.isSelected = true
        graduateRadioButton.isSelected = true
        coApplicationNoRadioButton.isSelected = true
        loanapproveldata.Nationality = "varablew"
        
        nataionality = "India"
        proof = "PAN"
        Status = "Single"
        educolification = "Graduate"
        coapplicant = "No"
        loanapproveldata.Nationality = nataionality
        loanapproveldata.Proof = proof
        loanapproveldata.MStatus = Status
        loanapproveldata.CoApplicant = coapplicant
        
        coapplicantView.isHidden = true
        coapplicatHeightConstraint.constant = 0
         AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:enterMothersNameTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:enterFathersTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:numberOfbependesTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
          AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:coApplicantNameTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:coApplicantIncomeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:coApllicantaddressTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOnCoapplicationButton(_ sender: KGRadioButton) {
         sender.isSelected = !sender.isSelected
        if sender == coApplicationYesRadioButton
        {
            if sender.isSelected == true
            {
                coapplicantView.isHidden = false
                coapplicatHeightConstraint.constant = 250
                
                coApplicationNoRadioButton.isSelected = false
                coapplicant = "Yes"
            }
        }
        else
        {
            if sender.isSelected == true
            {
                coapplicantView.isHidden = true
                coapplicatHeightConstraint.constant = 0
                coApplicationYesRadioButton.isSelected = false
                coapplicant = "No"
            }
        }
    }
    
    
  
    @IBAction func didTapOnEducationDetailsButton(_ sender: KGRadioButton) {
        sender.isSelected = !sender.isSelected
        if sender == postGraduateRadioButton
        {
            if sender.isSelected == true
            {
                graduateRadioButton.isSelected = false
                underGraduationRadioButton.isSelected = false
                eduOthersRadioButton.isSelected = false
                educolification = "Post Gradute"


            }
        }
        else if sender == graduateRadioButton
        {
            if sender.isSelected == true
            {
                postGraduateRadioButton.isSelected = false
                underGraduationRadioButton.isSelected = false
                eduOthersRadioButton.isSelected = false
                educolification = "Gradute"
            }
        }
        else if sender == underGraduationRadioButton
        {
            if sender.isSelected == true
            {
                graduateRadioButton.isSelected = false
                postGraduateRadioButton.isSelected = false
                eduOthersRadioButton.isSelected = false
                educolification = "Undergraduate"
            }
        }
        else
        {
            if sender.isSelected == true
            {
                graduateRadioButton.isSelected = false
                postGraduateRadioButton.isSelected = false
                underGraduationRadioButton.isSelected = false
                educolification = "Others"
            }
        }
    }
    @IBAction func didTapOnStatusButton(_ sender: KGRadioButton) {
        sender.isSelected = !sender.isSelected
        if sender == singleRadioButton
        {
            if sender.isSelected == true
            {
                marriedRadioButton.isSelected = false
                 Status = "Single"
            }
        }
        else
        {
            if sender.isSelected == true
            {
                singleRadioButton.isSelected = false
                 Status = "Married"
            }
        }
    }
    
    @IBAction func didTapOnNationalltyRadioButton(_ sender: KGRadioButton) {
        sender.isSelected = !sender.isSelected
        print(loanapproveldata.Nationality)
        print(loanapproveldata.co_applicant_address)
        if sender == indiaRadioButton
        {
            if sender.isSelected == true
            {
                nationalityOthersRadioButton.isSelected = false
                nataionality = "India"
            }
        }
        else
        {
            if sender.isSelected == true
            {
                indiaRadioButton.isSelected = false
                nataionality = "Others"

            }
        }
    }
    
    @IBAction func didTapOnProofofIdenty(_ sender: KGRadioButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func didTapOnNextButton(_ sender: Any) {
     //   self.performSegue(withIdentifier: "RefferalDetailsSegue",sender:nil)
        if nataionality.isEmpty == true
        {

            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select Nationallity")
        }
        else if enterMothersNameTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter the Mothers's Name")
        }
        else if enterFathersTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter the Father's Name")
        }
        else if Status.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select the Married Status")
        }

        else if Status.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select the Married Status")
        }
        else if educolification.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select the Education Details")
        }
        else if coapplicant.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select the co-applicant status")
        }
        else
        {
            if coapplicant == "Yes"
            {
                if coApplicantNameTxfld.text?.isEmpty == true
                {

                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Co-Applicant Name")
                }
                else if coApllicantaddressTxfld.text?.isEmpty == true
                {

                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Co-Applicant address")
                }
                else if coApplicantIncomeTxfld.text?.isEmpty == true
                {

                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Co-Applicant Income")
                }
                else
                {
                    appdelegate?.setloanapplaydic.Nationality = nataionality
                    appdelegate?.setloanapplaydic.Proof = proof
                    appdelegate?.setloanapplaydic.MStatus = Status
                    appdelegate?.setloanapplaydic.CoApplicant = coapplicant
                    appdelegate?.setloanapplaydic.EduDetails = educolification
                    appdelegate?.setloanapplaydic.Mname = enterMothersNameTxfld.text!
                    appdelegate?.setloanapplaydic.Fname = enterFathersTxfld.text!
                    appdelegate?.setloanapplaydic.co_applicant_name = coApplicantNameTxfld.text!
                    appdelegate?.setloanapplaydic.co_applicant_address = coApllicantaddressTxfld.text!
                    appdelegate?.setloanapplaydic.co_applicant_income = Float(coApplicantIncomeTxfld.text!)!
                    if let depen = numberOfbependesTxfld.text
                    {
                        appdelegate?.setloanapplaydic.Noofdependency = depen
                    }

                    self.performSegue(withIdentifier: "RefferalDetailsSegue",sender:nil)
                }

            }
            else
            {
                appdelegate?.setloanapplaydic.Nationality = nataionality
                appdelegate?.setloanapplaydic.Proof = proof
                appdelegate?.setloanapplaydic.MStatus = Status
                appdelegate?.setloanapplaydic.CoApplicant = coapplicant
                appdelegate?.setloanapplaydic.EduDetails = educolification
                appdelegate?.setloanapplaydic.Mname = enterMothersNameTxfld.text!
                appdelegate?.setloanapplaydic.Fname = enterFathersTxfld.text!
                if let depen = numberOfbependesTxfld.text
                {
                    appdelegate?.setloanapplaydic.Noofdependency = depen
                }
                self.performSegue(withIdentifier: "RefferalDetailsSegue",sender:nil)

            }
        
            
        }
        
        
       //UploadDocumentsSegue
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PRE APPROVAL"
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK: - Referal Controller.

class GetApproval2 : UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    
    
    @IBOutlet weak var adress2Txfld: UITextField!
    @IBOutlet weak var pincode2Txfld: UITextField!
    @IBOutlet weak var moblie2Txfld: UITextField!
    @IBOutlet weak var selectRelation2Txfld: UITextField!
    @IBOutlet weak var referenceName2Txfld: UITextField!
    
    @IBOutlet weak var adress1Txfld: UITextField!
    @IBOutlet weak var mobileTxfld: UITextField!
    @IBOutlet weak var pincodeTxfld: UITextField!
    @IBOutlet weak var selectRelationTxfld: UITextField!
    
    @IBOutlet weak var referenceName1Txfld: UITextField!
    var textfield = UITextField()
    var pickerData = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:referenceName1Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:mobileTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:pincodeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:adress1Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:selectRelationTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
          AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectRelationTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:referenceName2Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:moblie2Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:pincode2Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:adress2Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:selectRelation2Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectRelation2Txfld, backgroundcolor: UIColor.blue,backgroundbool: false)
       
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PRE APPROVAL"
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        //        if textField == selectGenderTxfld || (residencyTypeTfd != nil) || (selectCityTfd != nil) || (dobTfd != nil)
        //        {
        //        return false
        //        }
        //        else
        //        {
        //            return true
        //        }
        self.textfield = textField
            pickerData = ["Father","Mother","Brother","Sister","Cousin","Neighbour","Uncel","Aunt","Friend"]
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        switch textField {
        case selectRelationTxfld:
            selectRelationTxfld.inputView = picker
            return true
        case selectRelation2Txfld:
            selectRelation2Txfld.inputView = picker
            return true
        default:
            return true
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textfield == selectRelationTxfld
        {
            selectRelationTxfld.text = pickerData[row]
             appdelegate?.setloanapplaydic.RefRelationID = "\(row)"
    
        }
        else
        {
            selectRelation2Txfld.text = pickerData[row]
             appdelegate?.setloanapplaydic.Ref1RelationID = "\(row)"
        }
        
        //  textfield?.resignFirstResponder()
        //  picker.isHidden = true
    }
    
    
    @IBAction func didTapOnNextButton(_ sender: Any) {
     //  self.performSegue(withIdentifier: "UploadDocumentsSegue",sender:nil)

        if referenceName1Txfld.text?.isEmpty == true
        {

            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Refferal One Name")
        }
        else if selectRelationTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select Relation of Refferal one")
        }
        else if mobileTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter the Mobile number of Refferal one")
        }
        else if pincodeTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Pincode")
        }

        else if adress1Txfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Address of Refferal One")
        }
        else   if referenceName2Txfld.text?.isEmpty == true
        {

            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Refferal Two Name")
        }
        else if selectRelation2Txfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select Relation of Refferal Two")
        }
        else if moblie2Txfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter the Mobile number of Refferal Two")
        }
        else if pincode2Txfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Pincode")
        }

        else if adress2Txfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Address of Refferal Two")
        }
        else
        {
           // let setloanapplaydic = loanapproval()
            appdelegate?.setloanapplaydic.RefName = referenceName1Txfld.text!
            appdelegate?.setloanapplaydic.RefAdd = adress1Txfld.text!
            appdelegate?.setloanapplaydic.RefMobile = mobileTxfld.text!
            appdelegate?.setloanapplaydic.RefPinCode = pincodeTxfld.text!
            appdelegate?.setloanapplaydic.RefRelation = selectRelationTxfld.text!

            appdelegate?.setloanapplaydic.Ref1Name = referenceName2Txfld.text!
            appdelegate?.setloanapplaydic.Ref1Add = adress2Txfld.text!
            appdelegate?.setloanapplaydic.Ref1Mobile = moblie2Txfld.text!
            appdelegate?.setloanapplaydic.Ref1Pincode = pincode2Txfld.text!
            appdelegate?.setloanapplaydic.Ref1Relation = selectRelation2Txfld.text!

            self.performSegue(withIdentifier: "UploadDocumentsSegue",sender:nil)

        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    @IBAction func didTapOnPreviousButton(_ sender: Any) {
         // self.performSegue(withIdentifier: "UploadDocumentsSegue",sender:nil)
    }
    
}













