//
//  PersonalDetailsVc.swift
//  MotorMart
//
//  Created by Nennu on 14/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class PersonalDetailsVc: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {

    var textfield :UITextField? = UITextField()
    @IBOutlet weak var selectGenderTxfld: UITextField!
    @IBOutlet weak var firstNmeTfd: UITextField!
    
    @IBOutlet weak var middleNmeTfd: UITextField!
    
    @IBOutlet weak var lastNmeTfd: UITextField!
    
    
    @IBOutlet weak var dobTfd: UITextField!
    
    @IBOutlet weak var mobileNumTfd: UITextField!
    
    @IBOutlet weak var emailTfd: UITextField!
    
    @IBOutlet weak var addressTfd: UITextField!
    
    @IBOutlet weak var selectCityTfd: UITextField!
    
    @IBOutlet weak var noOfYrsInCurrentCityTfd: UITextField!
    
    @IBOutlet weak var residencyTypeTfd: UITextField!
    let datePickerView:UIDatePicker = UIDatePicker()
    var userdetailsdic = [String:String]()
    var cityID = String()
    var pickerData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userdetailsdic)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:firstNmeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:middleNmeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:lastNmeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "Calender.png",textfield:dobTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:mobileNumTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "messages.png",textfield:emailTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            "personImage.png",textfield:selectCityTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            "selectgender.png",textfield:selectGenderTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:noOfYrsInCurrentCityTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:addressTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:residencyTypeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectCityTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectGenderTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:residencyTypeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        
        //        [{"_User_Id":"1829","_User_name":"Naveen","_User_code":"UID201818281","_Mobile_No":"9393939393","_EmailId":"kkkk9494456@gmail.com","_Password":null,"_oldPassword":null,"_OTP":null,"_Org_Name":null,"_Partner_Type":null,"_Address":null,"_Role":"4"}]
    
        
        self.firstNmeTfd.text = UserDefaults.standard.string(forKey: "_User_name")
        self.mobileNumTfd.text = UserDefaults.standard.string(forKey: "_Mobile_No")
        self.emailTfd.text = UserDefaults.standard.string(forKey: "_EmailId")
        if userdetailsdic.count < 2
        {
            if userdetailsdic["uid"] != nil
            {
                
            }
            else
            {
                
            }
        }
        else
        {
            print(userdetailsdic)
            
            //            ["state": "Andhra Pradesh", "name": "Chalamalasetty Varun Pavan Kanth", "dist": "Krishna", "house": "20/352A", "street": "CHILAKALAPUDI", "gender": "M", "uid": "507710508202", "yob": "1993", "vtc": "Chilakalapudi (rural)", "lm": "OPP. PANDURANGA HIGH SCHOOL", "co": "S/O Chalamalasetty Nagamalleswararao", "loc": "CHILAKALAPUDI", "pc": "521002"]
            
            
            if userdetailsdic["house"] != nil {
                self.firstNmeTfd.text = userdetailsdic["name"]
                addressTfd.text = "\(userdetailsdic["house"],userdetailsdic["lm"],userdetailsdic["street"],userdetailsdic["dist"],userdetailsdic["state"])"
                dobTfd.text = userdetailsdic["yob"]
           
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy" //Your date format
                dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
                let date = dateFormatter.date(from: userdetailsdic["yob"]!)
                let now = Date()
                let birthday: Date = date!
                let calendar = Calendar.current
                
                let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
                let age = ageComponents.year!
                userdetailsdic["AGE"] = "\(age)"
                
                if userdetailsdic["gender"] == "M"
                {
                    selectGenderTxfld.text = "Male"
                    
                }
                else if userdetailsdic["gender"] == "F"
                {
                    selectGenderTxfld.text = "Female"
                }
                else
                {
                    selectGenderTxfld.text = "Others"
                }
                
            }
            
        }
        
        //setupTextfieldrightViewwithborder
        // Do any additional setup after loading the view.
    }
    
    @IBAction func previousBtnClicked(_ sender: Any) {
        
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
      //  profisinalDetailsSegue
        if firstNmeTfd.text?.isEmpty == true
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter First Name")
            firstNmeTfd.becomeFirstResponder()
            
        }
        else if lastNmeTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Last Name")
            lastNmeTfd.becomeFirstResponder()
        }
        else if selectGenderTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select Gender")
            selectGenderTxfld.becomeFirstResponder()
        }
        else if dobTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select DOB")
            dobTfd.becomeFirstResponder()
        }
        else if mobileNumTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Mobile Number")
            mobileNumTfd.becomeFirstResponder()
        }
        else if emailTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter EmailID")
            emailTfd.becomeFirstResponder()
        }
        else if selectCityTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select City")
            selectCityTfd.becomeFirstResponder()
        }
        else if noOfYrsInCurrentCityTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Number of years you are staying in current city")
            noOfYrsInCurrentCityTfd.becomeFirstResponder()
        }
        else if residencyTypeTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select Residence Type")
            residencyTypeTfd.becomeFirstResponder()
        }
        else
        {
            userdetailsdic["FNAME"] = firstNmeTfd.text
            userdetailsdic["LNAME"] = lastNmeTfd.text
            userdetailsdic["GENDER"] = selectGenderTxfld.text
            // userdetailsdic["AGE"] = dobTfd.text
             userdetailsdic["MOBILE"] = mobileNumTfd.text
             userdetailsdic["EMAIL"] = emailTfd.text
             userdetailsdic["CITY"] = cityID
             userdetailsdic["ResidentType"] = residencyTypeTfd.text
            userdetailsdic["NoOfYearsinCity"] = noOfYrsInCurrentCityTfd.text
            
            
            
          self.performSegue(withIdentifier: "profisinalDetailsSegue", sender:userdetailsdic)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //   print(sender)
        navigationItem.title = nil
        if segue.identifier == "profisinalDetailsSegue"{
            let vc = segue.destination as! ProfessionalDetailsVC
            vc.userdetailsdic = sender as! [String:String]
            //   vc.navigationItem.backBarButtonItem?.title = ""
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
//        if textField == selectGenderTxfld || (residencyTypeTfd != nil) || (selectCityTfd != nil) || (dobTfd != nil)
//        {
//        return false
//        }
//        else
//        {
//            return true
//        }
        self.textfield = textField
        pickerData.removeAll()
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self

        switch textField {
        case selectGenderTxfld:
            pickerData.removeAll()
            pickerData = ["Male","Female","Others"]
            selectGenderTxfld.inputView = picker
            return true
        case residencyTypeTfd:
             pickerData.removeAll()
             pickerData = ["Own","Rented"]
             residencyTypeTfd.inputView = picker
            // self.textfield = sender
            return true
        case selectCityTfd:
             pickerData.removeAll()
             let dic = appdelegate?.citylist.array
             for val in dic!
             {
                pickerData.append(val["CityName"].string!)
             }
              selectCityTfd.inputView = picker
            return true
        case dobTfd:
            datePickerView.datePickerMode = UIDatePickerMode.date
            var components = DateComponents()
            components.year = -100
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = -18
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            dobTfd.inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
            return true
        default:
            return true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PERSONAL DETAILS"
    }
    
    
    @IBAction func didBegainEditing(_ sender: UITextField) {
        
//        self.textfield = nil
//         self.textfield = sender
//         pickerData.removeAll()
//        let picker = UIPickerView()
//        picker.delegate = self
//        picker.dataSource = self
//       if sender == selectGenderTxfld
//       {
//        pickerData = ["Male","Female","Others"]
//        sender.inputView = picker
//        self.textfield = sender
//        }
//      else  if sender == residencyTypeTfd
//        {
//            pickerData = ["Own","Rented"]
//            sender.inputView = picker
//           
//        }
//       else if sender == selectCityTfd {
//            let dic = appdelegate?.citylist.array
//            for val in dic!
//            {
//                pickerData.append(val["CityName"].string!)
//            }
//          //  print(appdelegate?.citylist as Any)
//           // pickerData = ["Bangalor","Belgaum","Chennai","Hyderabad","Mangalore","Mumbai","Vijayawada"]
//            sender.inputView = picker
//            self.textfield = sender
//        }
//        else
//       {
//        datePickerView.datePickerMode = UIDatePickerMode.date
//        var components = DateComponents()
//        components.year = -100
//        let minDate = Calendar.current.date(byAdding: components, to: Date())
//        
//        components.year = -18
//        let maxDate = Calendar.current.date(byAdding: components, to: Date())
//
//        datePickerView.minimumDate = minDate
//        datePickerView.maximumDate = maxDate
//        dobTfd.inputView = datePickerView
//        
//        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
//        }
//        
//       
//        print("sendr selected")
    }
    @IBAction func textfieldendediting(_ sender: UITextField) {
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
         let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: sender.date, to: Date())
        let age = ageComponents.year!
        userdetailsdic["AGE"] = "\(age)"
        self.dobTfd.text = dateFormatter.string(from: sender.date)
        
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - PickerView Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
//private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        print(pickerData[row])
//        return pickerData[row]
//    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textfield == selectCityTfd
        {
             selectCityTfd.text = pickerData[row]
            var city = appdelegate?.citylist[row].dictionary
            cityID = "\(city!["cityId"] ?? 0)"
        }
        
        else if textfield == selectGenderTxfld
        {
            selectGenderTxfld.text = pickerData[row]
        }
        else
        {
            residencyTypeTfd.text = pickerData[row]
           
        }
       
      //  textfield?.resignFirstResponder()
      //  picker.isHidden = true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
