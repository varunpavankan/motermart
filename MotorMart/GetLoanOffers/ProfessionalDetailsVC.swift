//
//  ProfessionalDetailsVC.swift
//  MotorMart
//
//  Created by Nennu on 14/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//
//{"ProductID":"VID2018137220180312011227445","Category_Id":1,"loan_name":"Two wheeler","first_name":"Vadavalli Phani Kumar","last_name":"kk","gender":"Male","DOB":35,"MobileNo":"9101010101","EmailId":"kk949445655@gmail.com","user_city":9,"Selected_Lender_seller":0,"profession":"Salaried","company_name":"coreintelli","joining_date":"2014","annual_income":56000,"annual_bonus":"0","monthly_insentives":0,"emi_monthly":0,"profit":"0.00","salary_bankaccount":"Central Bank","other_income":0,"nameof_business":0,"year_started":0,"net_income":0,"property_city":9,"how_useloan":0,"property_type":0,"builder_name":0,"where_land":0,"cost_flat":0,"exist_bank":"noo","outstanding_amt":0,"exist_loan_date":0,"co_applicant":0,"co_applicant_name":0,"co_applicant_address":0,"co_applicant_income":0,"transaction_type":0,"whois_seller":0,"costof_construction":0,"exp_loan_amount":0,"User_Code":"1866","middle_name":"null","NoOfYrs_city":5,"ResidentType":"Own","AadharNum":"0000 0000 0000 0000","PanNum":"000000000","Cibil":780,"colorID":0}


import UIKit

class ProfessionalDetailsVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {
 
   
    var pickerData = [String]()
    var textfield = UITextField()
    var defaultsalariedViewheight :CGFloat = 0.0
    var defaultbusinessViewheight :CGFloat = 0.0
    var defaultothersViewheight :CGFloat = 0.0
    @IBOutlet weak var othersViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var businessView: UIView!
    @IBOutlet weak var bussinessViewConstraint:
    NSLayoutConstraint!
    @IBOutlet weak var salariedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var salariedView: UIView!
    @IBOutlet weak var selectIncomeTypeCatagory: UITextField!
    @IBOutlet weak var companyNameTxfld: UITextField!
    
    @IBOutlet weak var totalYearsOfexperiencTxfld: UITextField!
    
    @IBOutlet weak var bankAccountTxfld: UITextField!
    
    @IBOutlet weak var monthlyDeductions: UITextField!
    
    @IBOutlet weak var annualBonusTxfld: UITextField!
    
    @IBOutlet weak var otherannualIncomeTxfld: UITextField!
    
    @IBOutlet weak var emifirstVewTxfld: UITextField!
    
    @IBOutlet weak var grossMonthlyIncomeTxfld: UITextField!
    
    @IBOutlet weak var yearOfJoiningTxfld: UITextField!
    
    @IBOutlet weak var nameOfbussinessTxfld2view: UITextField!
    
    @IBOutlet weak var yearOfstartedTxfld: UITextField!
    
    @IBOutlet weak var netIncomeforlastYear3rdView: UITextField!
    
    @IBOutlet weak var existingEmi3rdView: UITextField!
    
    @IBOutlet weak var existingEmiTxfld2view: UITextField!
    
    @IBOutlet weak var profitAfterTaxTxfld2view: UITextField!
    var userdetailsdic = [String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userdetailsdic)
        selectIncomeTypeCatagory.text = "Salaried"
    defaultsalariedViewheight = self.salariedViewHeightConstraint.constant
        defaultothersViewheight = self.othersViewHeightConstraint.constant
        defaultbusinessViewheight = self.bussinessViewConstraint.constant
        if selectIncomeTypeCatagory.text == "Salaried"
        {
            self.othersView.isHidden = true
            self.othersViewHeightConstraint.constant = 0
            self.businessView.isHidden = true
            self.bussinessViewConstraint.constant = 0
           // self.salariedView.isHidden = true
           // self.salariedViewHeightConstraint.constant = 0
        }
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:selectIncomeTypeCatagory, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:companyNameTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:yearOfJoiningTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:totalYearsOfexperiencTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:bankAccountTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: " ",textfield:grossMonthlyIncomeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            "personImage.png",textfield:monthlyDeductions, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            " ",textfield:annualBonusTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: " ",textfield:otherannualIncomeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: " ",textfield:emifirstVewTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
       
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:nameOfbussinessTxfld2view, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:yearOfstartedTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: " ",textfield:existingEmiTxfld2view, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:profitAfterTaxTxfld2view, backgroundcolor: UIColor.blue,backgroundbool: false)
        
         AlWrapper.setupTextfieldLeftViewwithborder(imageString: " ",textfield:netIncomeforlastYear3rdView, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: " ",textfield:existingEmi3rdView, backgroundcolor: UIColor.blue,backgroundbool: false)
//        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:yearOfstartedTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
//        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectGenderTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
//        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:residencyTypeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
//        companyNameTxfld.isHidden = true
//exitesemiTxfd.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapOnNextButton(_ sender: Any) {
        
        let userode = UserDefaults.standard.string(forKey: "_User_Id")
       if selectIncomeTypeCatagory.text == "Salaried"
       {
        if companyNameTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter the Company Name")
            companyNameTxfld.becomeFirstResponder()
            
        }
        else if yearOfJoiningTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Year of Joining")
            yearOfJoiningTxfld.becomeFirstResponder()
        }
        else if totalYearsOfexperiencTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter total work experience")
            totalYearsOfexperiencTxfld.becomeFirstResponder()
        }
        else if bankAccountTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select Bank")
            bankAccountTxfld.becomeFirstResponder()
        }
        else if grossMonthlyIncomeTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Monthly Gross")
            grossMonthlyIncomeTxfld.becomeFirstResponder()
        }
        else if annualBonusTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Annual Bonus")
            annualBonusTxfld.becomeFirstResponder()
        }
        else if monthlyDeductions.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Monthly Deducations")
            monthlyDeductions.becomeFirstResponder()
        }
        else if otherannualIncomeTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter another Annual Income")
            otherannualIncomeTxfld.becomeFirstResponder()
        }
        else if emifirstVewTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter Existing EMI")
            emifirstVewTxfld.becomeFirstResponder()
        }
        else
        {
//            ["Category_Id": "1", "state": "Andhra Pradesh", "NoOfYearsinCity": "20", "house": "20/352A", "GENDER": "Male", "ResidentType": "Rented", "MOBILE": "8686410525", "loan_name": "Two wheeler", "CITY": "Bangalor", "AGE": "1993", "vtc": "Chilakalapudi (rural)", "lm": "OPP. PANDURANGA HIGH SCHOOL", "co": "S/O Chalamalasetty Nagamalleswararao", "pc": "521002", "PanNum": "00000000", "loc": "CHILAKALAPUDI", "name": "Chalamalasetty Varun Pavan Kanth", "dist": "Krishna", "LNAME": "hello", "street": "CHILAKALAPUDI", "gender": "M", "EMAIL": "varunpavankan@gmail.com", "FNAME": "Chalamalasetty Varun Pavan Kanth", "uid": "507710508202", "yob": "1993", "ProductID": "VID2018137220180312011227445"]
           
//            userdetailsdic["FNAME"] = firstNmeTfd.text
//            userdetailsdic["LNAME"] = lastNmeTfd.text
//            userdetailsdic["GENDER"] = selectGenderTxfld.text
//            userdetailsdic["AGE"] = dobTfd.text
//            userdetailsdic["MOBILE"] = mobileNumTfd.text
//            userdetailsdic["EMAIL"] = emailTfd.text
//            userdetailsdic["CITY"] = selectCityTfd.text
//            userdetailsdic["ResidentType"] = residencyTypeTfd.text
//            userdetailsdic["NoOfYearsinCity"] = noOfYrsInCurrentCityTfd.text
//            let now = Date()
//            let birthday: Date =
//            let calendar = Calendar.current
//
//            let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
//            let age = ageComponents.year!
            let uploadData=["ProductID":userdetailsdic["ProductID"]!,"Category_Id":userdetailsdic["Category_Id"]!,"loan_name":userdetailsdic["loan_name"]!,"first_name":userdetailsdic["FNAME"]!,"last_name":userdetailsdic["LNAME"]!,"gender":userdetailsdic["GENDER"]!,"DOB":userdetailsdic["AGE"]!,"MobileNo":userdetailsdic["MOBILE"]!,"EmailId":userdetailsdic["EMAIL"]!,"user_city":userdetailsdic["CITY"]!,"Selected_Lender_seller":0,"profession":"Salaried","company_name":self.companyNameTxfld.text!,"joining_date":self.yearOfJoiningTxfld.text!,"annual_income":self.grossMonthlyIncomeTxfld.text!,"annual_bonus":self.annualBonusTxfld.text!,"monthly_insentives":0,"emi_monthly":self.emifirstVewTxfld.text!,"profit":"0.00","salary_bankaccount":self.bankAccountTxfld.text!,"other_income":self.otherannualIncomeTxfld.text!,"nameof_business":"null","year_started":"null","net_income":"null","property_city":userdetailsdic["CITY"]!,"how_useloan":0,"property_type":0,"builder_name":0,"where_land":0,"cost_flat":0,"exist_bank":"noo","outstanding_amt":0,"exist_loan_date":0,"co_applicant":0,"co_applicant_name":0,"co_applicant_address":0,"co_applicant_income":0,"transaction_type":0,"whois_seller":0,"costof_construction":0,"exp_loan_amount":0,"User_Code":userode!,"middle_name":"null","NoOfYrs_city":5,"ResidentType":userdetailsdic["ResidentType"]!,"AadharNum":userdetailsdic["uid"]!,"PanNum":userdetailsdic["PanNum"]!,"Cibil":0,"colorID":0] as [String : Any]
            print(uploadData)
            AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"Set_KycDetails", viewController: self, params: uploadData as [String : AnyObject], headers: nil, success: { (json, responce) in
                print(json)
                if json == 2
                {
                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "you are alredy registerd")
                }
                else
                {
                    let alert = UIAlertController(title: "Sucess", message: "Your KYC has been complted Sucessfully", preferredStyle: UIAlertControllerStyle.alert);
                    let OK = UIAlertAction(
                        title: "Thankyou",
                        style: .default){ action -> Void in
                            for controller in self.navigationController!.viewControllers as Array {
//                                if controller.isKind(of: ProductSpecificationVC.self) {
//                                    self.navigationController!.popToViewController(controller, animated: true)
//                                    break
//                                }
                            }
                            //other action
                    }
                    alert.addAction(OK)
                    
                    
                    // relate actions to controllers
                    
                    
                    self.present(alert, animated: true, completion: nil)
                  
                    
                }
            }) { (erro) in
                
                print(erro)
            }
            
        }
        }
        else if selectIncomeTypeCatagory.text == "Self Employed"
       {
        
        if nameOfbussinessTxfld2view.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter bussiness Name")
            nameOfbussinessTxfld2view.becomeFirstResponder()
            
        }
        else if yearOfstartedTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter In wich Year you started business")
            yearOfstartedTxfld.becomeFirstResponder()
        }
        else if profitAfterTaxTxfld2view.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Profit")
            profitAfterTaxTxfld2view.becomeFirstResponder()
        }
        else if existingEmiTxfld2view.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter Existing EMI")
            existingEmiTxfld2view.becomeFirstResponder()
        }
        else
        {
          //  var values = productvalues.ProductId()
         //   var producID = values.p
            
            let uploadData=["ProductID":userdetailsdic["ProductID"]!,"Category_Id":userdetailsdic["Category_Id"]!,"loan_name":userdetailsdic["loan_name"]!,"first_name":userdetailsdic["FNAME"]!,"last_name":userdetailsdic["LNAME"]!,"gender":userdetailsdic["GENDER"]!,"DOB":userdetailsdic["AGE"]!,"MobileNo":userdetailsdic["MOBILE"]!,"EmailId":userdetailsdic["EMAIL"]!,"user_city":userdetailsdic["CITY"]!,"Selected_Lender_seller":0,"profession":"Salaried","company_name":0,"joining_date":0,"annual_income":0,"annual_bonus":0,"monthly_insentives":0,"emi_monthly":self.existingEmiTxfld2view.text!,"profit":self.profitAfterTaxTxfld2view.text!,"salary_bankaccount":0,"other_income":0,"nameof_business":self.nameOfbussinessTxfld2view.text!,"year_started":self.yearOfstartedTxfld.text!,"net_income":"null","property_city":userdetailsdic["CITY"]!,"how_useloan":0,"property_type":0,"builder_name":0,"where_land":0,"cost_flat":0,"exist_bank":"noo","outstanding_amt":0,"exist_loan_date":0,"co_applicant":0,"co_applicant_name":0,"co_applicant_address":0,"co_applicant_income":0,"transaction_type":0,"whois_seller":0,"costof_construction":0,"exp_loan_amount":0,"User_Code":userode!,"middle_name":"null","NoOfYrs_city":5,"ResidentType":userdetailsdic["ResidentType"]!,"AadharNum":userdetailsdic["uid"]!,"PanNum":userdetailsdic["PanNum"]!,"Cibil":0,"colorID":0] as [String : Any]
            
            print(uploadData)
            AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"Set_KycDetails", viewController: self, params: uploadData as [String : AnyObject], headers: nil, success: { (json, responce) in
                print(json)
                if json == 400
                {
                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the valid mobile number and password")
                }
                else
                {
                    
                  
                }
            }) { (erro) in
                
                print(erro)
            }
            
        }
        
        }
        else
       {
        if netIncomeforlastYear3rdView.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter net Income")
            netIncomeforlastYear3rdView.becomeFirstResponder()
            
        }
        else if existingEmi3rdView.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter Existing EMI")
            existingEmi3rdView.becomeFirstResponder()
        }
        else
        {
            let uploadData=["ProductID":userdetailsdic["ProductID"]!,"Category_Id":userdetailsdic["Category_Id"]!,"loan_name":userdetailsdic["loan_name"]!,"first_name":userdetailsdic["FNAME"]!,"last_name":userdetailsdic["LNAME"]!,"gender":userdetailsdic["GENDER"]!,"DOB":userdetailsdic["AGE"]!,"MobileNo":userdetailsdic["MOBILE"]!,"EmailId":userdetailsdic["EMAIL"]!,"user_city":userdetailsdic["CITY"]!,"Selected_Lender_seller":0,"profession":"Salaried","company_name":0,"joining_date":0,"annual_income":0,"annual_bonus":0,"monthly_insentives":0,"emi_monthly":self.existingEmi3rdView.text!,"profit":"0.00","salary_bankaccount":0,"other_income":0,"nameof_business":"null","year_started":"null","net_income":self.netIncomeforlastYear3rdView.text!,"property_city":userdetailsdic["CITY"]!,"how_useloan":0,"property_type":0,"builder_name":0,"where_land":0,"cost_flat":0,"exist_bank":"noo","outstanding_amt":0,"exist_loan_date":0,"co_applicant":0,"co_applicant_name":0,"co_applicant_address":0,"co_applicant_income":0,"transaction_type":0,"whois_seller":0,"costof_construction":0,"exp_loan_amount":0,"User_Code":userode!,"middle_name":"null","NoOfYrs_city":5,"ResidentType":userdetailsdic["ResidentType"]!,"AadharNum":userdetailsdic["uid"]!,"PanNum":userdetailsdic["PanNum"]!,"Cibil":0,"colorID":0] as [String : Any]
            
            print(uploadData)
            AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"Set_KycDetails", viewController: self, params: uploadData as [String : AnyObject], headers: nil, success: { (json, responce) in
                print(json)
                if json == 400
                {
                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the valid mobile number and password")
                }
                else
                {
                    
                    
                }
            }) { (erro) in
                
                print(erro)
            }
        }
        
        }
       
    }
    
    //Mark select
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.textfield = textField
         let picker = UIPickerView()
        //        if textField == selectGenderTxfld || (residencyTypeTfd != nil) || (selectCityTfd != nil) || (dobTfd != nil)
        //        {
        //        return false
        //        }
        //        else
        //        {
        //            return true
        //        }
        picker.delegate = self
        picker.dataSource = self
        
        switch textField {
        case selectIncomeTypeCatagory:
            pickerData.removeAll()
            pickerData = ["Salaried","Self Employed","Others"]
            selectIncomeTypeCatagory.inputView = picker
            return true
        case bankAccountTxfld:
            pickerData.removeAll()
            pickerData = ["Andhra Bank","Axis Bank","Bank Of India","Canara Bank","HDFC Bank","State Bannk Of India","ICICI Bank","Induslnd Bank","IOB Bank"]
            bankAccountTxfld.inputView = picker
            return true
        default:
            return true
        }
    }
    
    
    
    // Mark pickerViewdelegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            print(pickerData[row])
            return pickerData[row]
        }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textfield == selectIncomeTypeCatagory
        {
            selectIncomeTypeCatagory.text = pickerData[row]
            //  picker.isHidden = true
            switch selectIncomeTypeCatagory.text {
            case "Salaried":
                if defaultsalariedViewheight != 0
                {
                    self.salariedView.isHidden = false
                    self.salariedViewHeightConstraint.constant = defaultsalariedViewheight
                }
                self.othersView.isHidden = true
                self.othersViewHeightConstraint.constant = 0
                self.businessView.isHidden = true
                self.bussinessViewConstraint.constant = 0
            case "Self Employed":
                if defaultbusinessViewheight != 0
                {
                    self.businessView.isHidden = false
                    self.bussinessViewConstraint.constant = defaultbusinessViewheight
                }
                self.othersView.isHidden = true
                self.othersViewHeightConstraint.constant = 0
                self.salariedView.isHidden = true
                self.salariedViewHeightConstraint.constant = 0
            case "Others":
                if defaultothersViewheight != 0
                {
                    self.othersView.isHidden = false
                    self.othersViewHeightConstraint.constant = defaultothersViewheight
                }
                self.businessView.isHidden = true
                self.bussinessViewConstraint.constant = 0
                self.salariedView.isHidden = true
                self.salariedViewHeightConstraint.constant = 0
            default:
                print("hello")
                //            self.othersView.isHidden = true
                //            self.defaultothersViewheight =   self.othersViewHeightConstraint.constant
                //            self.othersViewHeightConstraint.constant = 0
                //            self.businessView.isHidden = true
                //            self.defaultbusinessViewheight =   self.bussinessViewConstraint.constant
                //            self.bussinessViewConstraint.constant = 0
                
            }
              }
        else
            
        {
            bankAccountTxfld.text = pickerData[row]
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PROFESSIONAL/INCOME DETAILS"
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
