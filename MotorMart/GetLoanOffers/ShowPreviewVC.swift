//
//  ShowPreviewVC.swift
//  MotorMart
//
//  Created by Nennu on 24/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import  SwiftyJSON
class ShowPreviewVC: UIViewController {

    
    @IBOutlet weak var aadharCardNo: UITextField!
    
    @IBOutlet weak var panCardNo: UITextField!
    
    @IBOutlet weak var cibilScore: UITextField!
    
    @IBOutlet weak var NameTxfld: UITextField!
    
    @IBOutlet weak var genderTxfld: UITextField!
    
    @IBOutlet weak var AgeTxfld: UITextField!
    
    @IBOutlet weak var mobileNoTxfld: UITextField!
    
    @IBOutlet weak var emailIdTxfld: UITextField!
    
    @IBOutlet weak var cityTxfld: UITextField!
    
    
    @IBOutlet weak var mstatuTxfld: UITextField!
    
    @IBOutlet weak var fatherNameTxfld: UITextField!
    
    
    @IBOutlet weak var mothersNameTxfld: UITextField!
    
    
    @IBOutlet weak var numberdependeceTxfld: UITextField!
    
    
    @IBOutlet weak var ocupationTxfld: UITextField!
    
    
    @IBOutlet weak var OccupationTxfld: UITextField!
    
    @IBOutlet weak var companyNameTxfld: UITextField!
    
    
    @IBOutlet weak var yearOfJoining: UITextField!
   
    
    @IBOutlet weak var isthereAnyCoApplicant: UITextField!
    
    @IBOutlet weak var salaryBankAccount: UITextField!
    
    @IBOutlet weak var grossMonthlyAmount: UITextField!
    
    @IBOutlet weak var proofOfIdentity: UITextField!
    
    @IBOutlet weak var NationalityTxfld: UITextField!
    
    @IBOutlet weak var educationDetails: UITextField!
    

    @IBOutlet weak var OrganisationNameLable: UILabel!
    
    @IBOutlet weak var makeNameLable: UILabel!
    
    @IBOutlet weak var modelNameLable: UILabel!
    
    @IBOutlet weak var colorLable: UILabel!
    
    @IBOutlet weak var fuleTypeLable: UILabel!
    
    @IBOutlet weak var priceLable: UILabel!
    
    @IBOutlet weak var bankNameLable: UILabel!
    
    @IBOutlet weak var monthlyEMILabel: UILabel!
    
    @IBOutlet weak var downpaymentLabel: UILabel!
    
    @IBOutlet weak var tenureofmonthsLable: UILabel!
    @IBOutlet weak var loanAmountLable: UILabel!
    
    @IBOutlet weak var processingfeeLable: UILabel!
    @IBOutlet weak var interestNameLable: UILabel!
    
    @IBOutlet weak var otherChargesLable: UILabel!
    
    @IBOutlet weak var relationShip1Txfld: UITextField!
    
    
    @IBOutlet weak var refer1moblieTxfld: UITextField!
    @IBOutlet weak var referance1Txfld: UITextField!
    
    @IBOutlet weak var refer1Pincode: UITextField!
    
    @IBOutlet weak var refer1AddressTxfld: UITextField!
    
    @IBOutlet weak var refer2NameTxfld: UITextField!
    
    @IBOutlet weak var relationShip2Txfld: UITextField!
    
    @IBOutlet weak var refer2MobileNumber: UITextField!
    
    @IBOutlet weak var reference2pincode: UITextField!
    
    
    @IBOutlet weak var referance2AddressTxfld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.NationalityTxfld.text = appdelegate?.setloanapplaydic.Nationality
        let dic = appdelegate?.setloanQuestiondetails.Loanquestiondetails
        
        self.aadharCardNo.text = dic!["AadharNum"].string!
        self.panCardNo.text = dic!["PanNum"].string!
        self.cibilScore.text = dic!["Cibil"].string!
        self.NameTxfld.text = dic!["First_Name"].string!
        self.genderTxfld.text = dic!["Gender"].string!
        self.AgeTxfld.text = "\(dic!["Age"].int!)"
         self.mobileNoTxfld.text = dic!["Mobile_No"].string!
        self.emailIdTxfld.text = dic!["Email_Id"].string!
        self.cityTxfld.text = "\(dic!["Current_City"].int!)"
        self.mstatuTxfld.text = appdelegate?.setloanapplaydic.MStatus
        self.fatherNameTxfld.text = appdelegate?.setloanapplaydic.Fname
        self.mothersNameTxfld.text = appdelegate?.setloanapplaydic.Mname
        self.numberdependeceTxfld.text = appdelegate?.setloanapplaydic.Noofdependency
        
        
        self.OccupationTxfld.text = dic!["Income_Type"].string!
        self.companyNameTxfld.text = dic!["Company_Name"].string!
        self.yearOfJoining.text = dic!["Joining_Date_Company"].string!
        self.salaryBankAccount.text = dic!["SalaryBankAccount"].string!
         self.grossMonthlyAmount.text = dic!["GrossMonthlyIncome"].string!
        
        self.proofOfIdentity.text = appdelegate?.setloanapplaydic.Proof
        self.educationDetails.text = appdelegate?.setloanapplaydic.EduDetails
        self.isthereAnyCoApplicant.text = appdelegate?.setloanapplaydic.CoApplicant
        
        self.referance1Txfld.text = appdelegate?.setloanapplaydic.RefName
        self.refer1AddressTxfld.text = appdelegate?.setloanapplaydic.RefAdd
        self.refer1moblieTxfld.text = appdelegate?.setloanapplaydic.RefMobile
        self.refer1Pincode.text = appdelegate?.setloanapplaydic.RefPinCode
        self.relationShip1Txfld.text = appdelegate?.setloanapplaydic.RefRelation
        
        self.refer2NameTxfld.text = appdelegate?.setloanapplaydic.Ref1Name
        self.referance2AddressTxfld.text = appdelegate?.setloanapplaydic.Ref1Add
        self.refer2MobileNumber.text = appdelegate?.setloanapplaydic.Ref1Mobile
        self.reference2pincode.text = appdelegate?.setloanapplaydic.Ref1Pincode
        self.relationShip2Txfld.text = appdelegate?.setloanapplaydic.Ref1Relation
        
        self.OrganisationNameLable.text = appdelegate?.selectedIteamlaondetails.loanOrgnizationName
        
        let dic2 = appdelegate?.setproductDetails.productdetails
        
        self.makeNameLable.text = dic2!["makeName"].string!
        self.modelNameLable.text = dic2!["modelName"].string!
        self.colorLable.text = "white"
         self.fuleTypeLable.text = dic2!["Fuel_type"].string!
        self.priceLable.text = "\(dic2!["grossAmount"].float!)"
        
        self.bankNameLable.text = appdelegate?.selectedIteamlaondetails.Checquebouncecharges
        self.interestNameLable.text = appdelegate?.selectedIteamlaondetails.InterestRate
        self.processingfeeLable.text = appdelegate?.selectedIteamlaondetails.ProcessingCharges
        self.otherChargesLable.text = appdelegate?.selectedIteamlaondetails.Othercost
        self.loanAmountLable.text = appdelegate?.selectedIteamlaondetails.LoanAmount
        self.tenureofmonthsLable.text = appdelegate?.selectedIteamlaondetails.Tenure
        self.monthlyEMILabel.text = appdelegate?.selectedIteamlaondetails.EMI
        self.downpaymentLabel.text = appdelegate?.selectedIteamlaondetails.Downpayment

        
        


        
      //  self.refer
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
