//
//  UploadDocumentsVc.swift
//  MotorMart
//
//  Created by Nennu on 23/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class UploadDocumentsVc: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate{
    @IBOutlet weak var incomeProoof: UITextField!
    
    @IBOutlet weak var paySlipTxfld: UITextField!
    @IBOutlet weak var bankStatement: UITextField!
    @IBOutlet weak var applicantIdProof: UITextField!
    @IBOutlet weak var applicantPhot: UITextField!
    var UploadDocuments = [String:Any]()
   // var ApplicantPhoto = [String:Any]()
    //var IdentityProof = UIImage()
    var IdentityProofExtension = String()
    //var PaySlips = UIImage()
    var PaySlipsExtension = String()
    //var BankStmt = UIImage()
    var BankStmtExtension = String()
    //var IncomeTaxReturn = UIImage()
    var IncomeTaxReturnExtension = String()
    //var AgriLandDoc = UIImage()
    var AgriLandDocExtension = String()
    //var RentalIncomeDoc = UIImage()
    var RentalIncomeDocExtension = String()

    let picker = UIImagePickerController()
    var sendertextfield = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "document.png",textfield:paySlipTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "attachment.png",textfield:paySlipTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "document.png",textfield:incomeProoof, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "attachment.png",textfield:incomeProoof, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "document.png",textfield:bankStatement, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "attachment.png",textfield:bankStatement, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "document.png",textfield:applicantIdProof, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "attachment.png",textfield:applicantIdProof, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "document.png",textfield:applicantPhot, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "attachment.png",textfield:applicantPhot, backgroundcolor: UIColor.blue,backgroundbool: false)

        // Do any additional setup after loading the view.
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        sendertextfield = textField
        //    imageButton = sender
        let alert = UIAlertController(
            title: "Select",
            message:"",
            preferredStyle: .actionSheet)
        let Gallery = UIAlertAction(
            title: "Gallery",
            style: .default){ action -> Void in
                //  self.multipleImagePicker(sender:sender)
                self.picker.allowsEditing = false
                self.picker.sourceType = .photoLibrary
                self.present(self.picker, animated: true, completion: nil)
        }
        let Camera = UIAlertAction(
            title: "Camera",
            style: .default){ action -> Void in
                
                //    self.emailTxfld.becomeFirstResponder()
                if !(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
                    
                    let camalert = UIAlertController(
                        title: "Select",
                        message:"",
                        preferredStyle: .alert)
                    let okay = UIAlertAction(
                        title: "Gallery",
                        style: .default){ action -> Void in
                            
                    }
                    camalert.addAction(okay)
                    self.present(camalert, animated: true, completion: nil)
                }
                else{
                    self.picker.allowsEditing = false
                    self.picker.sourceType = .camera
                    self.picker.cameraCaptureMode = .photo
                    //other action
                    self.present(self.picker, animated: true, completion: nil)
                }
        }
        let Cancle = UIAlertAction(
            title: "Cancel",
            style: .default){ action -> Void in
                
                //other action
        }
        let selectDocument = UIAlertAction(
            title: "Documents",
            style: .default){ action -> Void in
                
                self.mySpecialFunction()
        }
        alert.addAction(selectDocument)
        alert.addAction(Gallery)
        alert.addAction(Camera)
        alert.addAction(Cancle)
        self.present(alert, animated: true, completion: nil)
        
        return false
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
          let assetPath = info[UIImagePickerControllerReferenceURL] as! URL
        let pathextention = assetPath.pathExtension
        var Size = Float()
        var data = Data()
        let data1  = UIImageJPEGRepresentation(chosenImage, 0.5)!
        let strBase64 = data1.base64EncodedString(options: .lineLength64Characters)
        data = UIImageJPEGRepresentation(chosenImage, 0.5)!
        print(data1)
        //Here you get MB size
        Size = Float(Double(data.count)/1024/1024)
         sendertextfield.text = "Image selected"
        if Size <= 1.00{
            // Here your image
            if sendertextfield == applicantPhot
            {
                // self.ApplicantPhoto.append(chosenImage)
                UploadDocuments["ApplicantPhoto"] = strBase64
            }
            else if sendertextfield == paySlipTxfld
            {
                //  self.PaySlips = chosenImage
                UploadDocuments["PaySlips"] = strBase64
                PaySlipsExtension = pathextention
            }
            else if sendertextfield == applicantIdProof
            {
                // self.IdentityProof = chosenImage
                UploadDocuments["IdentityProof"] = strBase64
                IdentityProofExtension = pathextention
            }
            else if sendertextfield == bankStatement
            {
                //  self.BankStmt = chosenImage
                UploadDocuments["BankStmt"] = strBase64
                BankStmtExtension = pathextention
            }
            else if sendertextfield == incomeProoof
            {
                // self.BankStmt = chosenImage
                UploadDocuments["IncomeTaxReturn"] = strBase64
                IncomeTaxReturnExtension = pathextention
            }

        }
       else
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select Image less Than 1MB ")
        }
     
        // use the image
        

        dismiss(animated: true, completion: nil)
        //other action
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func mySpecialFunction(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: ["public.data"],in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }

    
    @IBAction func didTapOnPreviousButton(_ sender: Any) {
    }
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapOnpreviewButton(_ sender: Any) {
         self.performSegue(withIdentifier: "previewSegue",sender:nil)
    }
    //loanapproval : - to get preaprovel form data setloanapplaydic
    // selectedIteamLaonDetails : - to get loan payment and emi details selectedIteamlaondetails
    // setproductdetails : -  to get the product spefication setproductDetails
    // setLoanQuestionDetails : -  loan quations details  setloanQuestiondetails
    // getdetails of the selected bank :seteselectedbankdeails
    
    
    
    @IBAction func didTapOnSubmitButton(_ sender: Any) {
//        let userqutiondetails = appdelegate?.setloanQuestiondetails.Loanquestiondetails
//         let productdetails = appdelegate?.setproductDetails.productdetails
//        let userId = UserDefaults.standard.string(forKey: "_User_Id")!
//        let ProductNumber = (appdelegate?.seteselectedbankdeails.selectedBankDetails["Product_id"].string!)!
//        let ProductID = appdelegate?.setproductDetails.productdetails["prodId"].string!
//        let LenderID = appdelegate?.seteselectedbankdeails.selectedBankDetails["user_code"].string!
//        let ModelName = appdelegate?.setproductDetails.productdetails["modelName"].string!
//        let MakeName = appdelegate?.setproductDetails.productdetails["makeName"].string!
//        let UserName = userqutiondetails!["First_Name"].string!
//         let MobileNo = userqutiondetails!["Mobile_No"].string!
//        let CatName = "Two Wheeler"
//        let Applied3Months = "0"
//        let Lessthan45 = "0"
//        let fourWheeler = "0"
//        let Samebank = "0"
//        let CoApplicant = appdelegate?.setloanapplaydic.CoApplicant
//        let Mname = appdelegate?.setloanapplaydic.Mname
//        let Fname = appdelegate?.setloanapplaydic.Fname
//        let Hname = "null"
//        let Nationality =  appdelegate?.setloanapplaydic.Nationality
//        let Proof = appdelegate?.setloanapplaydic.Proof
//        let Noofdependency = appdelegate?.setloanapplaydic.Noofdependency
//        let EduDetails = appdelegate?.setloanapplaydic.EduDetails
//        let Religion = "0"
//        let RefName = appdelegate?.setloanapplaydic.RefName
//        let RefRelation = appdelegate?.setloanapplaydic.RefRelationID
//        let RefMobile = appdelegate?.setloanapplaydic.RefMobile
//        let RefPinCode = appdelegate?.setloanapplaydic.RefPinCode
//        let RefAdd = appdelegate?.setloanapplaydic.RefAdd
//        let Ref1Name = appdelegate?.setloanapplaydic.Ref1Name
//        let Ref1Relation = appdelegate?.setloanapplaydic.Ref1RelationID
//        let Ref1Mobile = appdelegate?.setloanapplaydic.Ref1Mobile
//        let Ref1PinCode = appdelegate?.setloanapplaydic.Ref1Pincode
//        let Ref1Add = appdelegate?.setloanapplaydic.Ref1Add
//        let GrossAmt = "\(productdetails!["grossAmount"].float!)"
//        let DownPayment = appdelegate?.selectedIteamlaondetails.Downpayment
//        let Tenure = appdelegate?.selectedIteamlaondetails.Tenure
//        let interestRate = appdelegate?.selectedIteamlaondetails.InterestRate
//        let MonthlyEMi = appdelegate?.selectedIteamlaondetails.EMI
//        let whois_seller = appdelegate?.setproductDetails.productdetails["sellerId"].string!
//        let co_applicant_name = appdelegate?.setloanapplaydic.co_applicant_name
//        let co_applicant_address = appdelegate?.setloanapplaydic.co_applicant_address
//        let co_applicant_income = appdelegate?.setloanapplaydic.co_applicant_income
//        let ApplicantPhoto =  UploadDocuments["ApplicantPhoto"]
//        let IdentityProof =  UploadDocuments["IdentityProof"]
//        let IdentityProofExtension = self.IdentityProofExtension
//        let PaySlips = UploadDocuments["PaySlips"]
//        let PaySlipsExtension = self.PaySlipsExtension
//        let BankStmt = UploadDocuments["BankStmt"]
//        let BankStmtExtension = self.BankStmtExtension
//        let IncomeTaxReturn = UploadDocuments["IncomeTaxReturn"]
//        let IncomeTaxReturnExtension = self.IncomeTaxReturnExtension
//        let AgriLandDoc = UploadDocuments["AgriLandDoc"]
//        let AgriLandDocExtension = self.AgriLandDocExtension
//        let RentalIncomeDoc = UploadDocuments["RentalIncomeDoc"]
//        let RentalIncomeDocExtension = self.RentalIncomeDocExtension
//        let Bstatus = "null"
//
//        let parameters = ["UserID":userId,"ProductNumber":ProductNumber,"ProductID":ProductID!,"LenderID":LenderID!,"UserName":UserName,"ModelName":ModelName!,"MakeName":MakeName!,"MobileNo":MobileNo,"CatName":CatName,"Applied3Months":Applied3Months,"Lessthan45":Lessthan45,"fourWheeler":fourWheeler,"Samebank":Samebank,"CoApplicant":CoApplicant!,"Mname":Mname!,"Fname":Fname!,"Hname":Hname,"Nationality":Nationality!,"Proof":Proof!,"Noofdependency":Noofdependency!,"EduDetails":EduDetails!,"Religion":Religion,"RefName":RefName!,"RefRelation":RefRelation!,"RefMobile":RefMobile!,"RefPinCode":RefPinCode!,"RefAdd":RefAdd!,"Ref1Name":Ref1Name!,"Ref1Relation":Ref1Relation!,"Ref1Mobile":Ref1Mobile!,"Ref1PinCode":Ref1PinCode!,"Ref1Add":Ref1Add!,"GrossAmt":GrossAmt,"DownPayment":DownPayment!,"Tenure":Tenure!,"interestRate":interestRate!,"MonthlyEMi":MonthlyEMi!,"whois_seller":whois_seller!,"co_applicant_name":co_applicant_name,"co_applicant_address":co_applicant_address,"co_applicant_income":co_applicant_income,"ApplicantPhoto":ApplicantPhoto,"IdentityProof":IdentityProof,"IdentityProofExtension":IdentityProofExtension,"PaySlips":PaySlips,"PaySlipsExtension":PaySlipsExtension,"BankStmt":BankStmt,"BankStmtExtension":BankStmtExtension,"IncomeTaxReturn":IncomeTaxReturn,"IncomeTaxReturnExtension":IncomeTaxReturnExtension,"AgriLandDoc":AgriLandDoc,"AgriLandDocExtension":AgriLandDocExtension,"RentalIncomeDoc":RentalIncomeDoc,"RentalIncomeDocExtension":RentalIncomeDocExtension,"Bstatus":Bstatus] as [String : Any]
//
//        print(parameters)
//
//        let urlString = My_Apis.MoterMart_api+"setPreApproval"
//        var request = URLRequest(url: URL(string: urlString)!)
//        request.httpMethod = HTTPMethod.post.rawValue
//        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
//         request.httpBody = parameters.description.data(using: .utf8)
//       // request.httpBody = jsonData
//
//        Alamofire.request(request).responseJSON {
//            (response) in
//
//            print(response)
//        }
//

// self.multipartdata()
        self.withparametera() 
    }

 //   }
    
   func multipartdata()
   {
    let userqutiondetails = appdelegate?.setloanQuestiondetails.Loanquestiondetails
    let productdetails = appdelegate?.setproductDetails.productdetails
    let userId = UserDefaults.standard.string(forKey: "_User_Id")!
    let ProductNumber = (appdelegate?.seteselectedbankdeails.selectedBankDetails["Product_id"].string!)!
    let ProductID = appdelegate?.setproductDetails.productdetails["prodId"].string!
    let LenderID = appdelegate?.seteselectedbankdeails.selectedBankDetails["user_code"].string!
    let ModelName = appdelegate?.setproductDetails.productdetails["modelName"].string!
    let MakeName = appdelegate?.setproductDetails.productdetails["makeName"].string!
    let UserName = userqutiondetails!["First_Name"].string!
    let MobileNo = userqutiondetails!["Mobile_No"].string!
    let CatName = "Two Wheeler"
    let Applied3Months = "0"
    let Lessthan45 = "0"
    let fourWheeler = "0"
    let Samebank = "0"
    let CoApplicant = appdelegate?.setloanapplaydic.CoApplicant
    let Mname = appdelegate?.setloanapplaydic.Mname
    let Fname = appdelegate?.setloanapplaydic.Fname
    let Hname = "null"
    let Nationality =  appdelegate?.setloanapplaydic.Nationality
    let Proof = appdelegate?.setloanapplaydic.Proof
    let Noofdependency = appdelegate?.setloanapplaydic.Noofdependency
    let EduDetails = appdelegate?.setloanapplaydic.EduDetails
    let Religion = "0"
    let RefName = appdelegate?.setloanapplaydic.RefName
    let RefRelation = appdelegate?.setloanapplaydic.RefRelationID
    let RefMobile = appdelegate?.setloanapplaydic.RefMobile
    let RefPinCode = appdelegate?.setloanapplaydic.RefPinCode
    let RefAdd = appdelegate?.setloanapplaydic.RefAdd
    let Ref1Name = appdelegate?.setloanapplaydic.Ref1Name
    let Ref1Relation = appdelegate?.setloanapplaydic.Ref1RelationID
    let Ref1Mobile = appdelegate?.setloanapplaydic.Ref1Mobile
    let Ref1PinCode = appdelegate?.setloanapplaydic.Ref1Pincode
    let Ref1Add = appdelegate?.setloanapplaydic.Ref1Add
    let GrossAmt = "\(productdetails!["grossAmount"].float!)"
    let DownPayment = appdelegate?.selectedIteamlaondetails.Downpayment
    let Tenure = appdelegate?.selectedIteamlaondetails.Tenure
    let interestRate = appdelegate?.selectedIteamlaondetails.InterestRate
    let MonthlyEMi = appdelegate?.selectedIteamlaondetails.EMI
    let whois_seller = appdelegate?.setproductDetails.productdetails["sellerId"].string!
    let co_applicant_name = appdelegate?.setloanapplaydic.co_applicant_name
    let co_applicant_address = appdelegate?.setloanapplaydic.co_applicant_address
    let co_applicant_income = appdelegate?.setloanapplaydic.co_applicant_income
    let ApplicantPhoto =  UploadDocuments["ApplicantPhoto"]
    let IdentityProof =  UploadDocuments["IdentityProof"]
    let IdentityProofExtension = self.IdentityProofExtension
    let PaySlips = UploadDocuments["PaySlips"]
    let PaySlipsExtension = self.PaySlipsExtension
    let BankStmt = UploadDocuments["BankStmt"]
    let BankStmtExtension = self.BankStmtExtension
    let IncomeTaxReturn = UploadDocuments["IncomeTaxReturn"]
    let IncomeTaxReturnExtension = self.IncomeTaxReturnExtension
    let AgriLandDoc = UploadDocuments["AgriLandDoc"]
    let AgriLandDocExtension = self.AgriLandDocExtension
    let RentalIncomeDoc = UploadDocuments["RentalIncomeDoc"]
    let RentalIncomeDocExtension = self.RentalIncomeDocExtension
    let Bstatus = "null"
    
    let parameters = ["UserID":userId,"ProductNumber":ProductNumber,"ProductID":ProductID!,"LenderID":LenderID!,"UserName":UserName,"ModelName":ModelName!,"MakeName":MakeName!,"MobileNo":MobileNo,"CatName":CatName,"Applied3Months":Applied3Months,"Lessthan45":Lessthan45,"fourWheeler":fourWheeler,"Samebank":Samebank,"CoApplicant":CoApplicant!,"Mname":Mname!,"Fname":Fname!,"Hname":Hname,"Nationality":Nationality!,"Proof":Proof!,"Noofdependency":Noofdependency!,"EduDetails":EduDetails!,"Religion":Religion,"RefName":RefName!,"RefRelation":RefRelation!,"RefMobile":RefMobile!,"RefPinCode":RefPinCode!,"RefAdd":RefAdd!,"Ref1Name":Ref1Name!,"Ref1Relation":Ref1Relation!,"Ref1Mobile":Ref1Mobile!,"Ref1PinCode":Ref1PinCode!,"Ref1Add":Ref1Add!,"GrossAmt":GrossAmt,"DownPayment":DownPayment!,"Tenure":Tenure!,"interestRate":interestRate!,"MonthlyEMi":MonthlyEMi!,"whois_seller":whois_seller!,"co_applicant_name":co_applicant_name,"co_applicant_address":co_applicant_address,"co_applicant_income":co_applicant_income,"ApplicantPhoto":ApplicantPhoto,"IdentityProof":IdentityProof,"IdentityProofExtension":IdentityProofExtension,"PaySlips":PaySlips,"PaySlipsExtension":PaySlipsExtension,"BankStmt":BankStmt,"BankStmtExtension":BankStmtExtension,"IncomeTaxReturn":IncomeTaxReturn,"IncomeTaxReturnExtension":IncomeTaxReturnExtension,"AgriLandDoc":AgriLandDoc,"AgriLandDocExtension":AgriLandDocExtension,"RentalIncomeDoc":RentalIncomeDoc,"RentalIncomeDocExtension":RentalIncomeDocExtension,"Bstatus":Bstatus] as [String : Any]
    
    print(parameters)
    
    
    Alamofire.upload(multipartFormData: { (multipartFormData) in
        for (key, value) in (parameters) {
            print(key)
            if (key == "ApplicantPhoto" || key == "IdentityProof" || key == "PaySlips" || key == "BankStmt" || key == "IncomeTaxReturn" || key == "AgriLandDoc" || key == "RentalIncomeDoc")
            {
                print(key)
                //          print(value as! Array<UIImage>)
                if ((value) as AnyObject) is UIImage
                {
                    multipartFormData.append(UIImageJPEGRepresentation((((value) as AnyObject) as! UIImage),0.5)!, withName: key,fileName: key, mimeType: "image/jpeg")
                }
                else
                {
                    if ((value as AnyObject?) is NSNull){
                        
                        
                        multipartFormData.append(("" as AnyObject).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue)!, withName: key)
                        
                    }
                    else
                    {
                        let pdfData = try! Data(contentsOf:  (value  as! String).asURL())
                        let data : Data = pdfData
                        print("data:\(data)")
                        
                        multipartFormData.append(data, withName: key, fileName: key, mimeType: "application/pdf")
                    }
                    
                    
                }
                
            }
            else
            {
                multipartFormData.append(("\(value)" as AnyObject).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue)!, withName: key)
            }
            //    multipartFormData.append("".data, withName: key)
        }
    }, to: My_Apis.MoterMart_api+"setPreApproval", headers: nil)
    { (result) in
        
        print(result)
        switch result {
        case .success(let upload, _, _):
            
            upload.uploadProgress(closure: { (Progress) in
                print("Upload Progress: \(Progress.fractionCompleted)")
            })
            //  self.submitButton.isEnabled = true
            upload.responseJSON { response in
                //self.delegate?.showSuccessAlert()
                print(response.request!)  // original URL request
                print(response.response!) // URL response
                print(response.data!)     // server data
                print(response.result)  // result of response serialization
                let statusCode = response.response?.statusCode
                if statusCode == 201 {
                    
                    do{
                        //      self.appdelegate?.activityIndicator.stopAnimating()
                        let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as! NSDictionary
                        //     let swiftyjson = JSON(json)
                        //   UserDefaults.standard.set(json, forKey: "userdetails")
                        let alert = UIAlertController(
                            title: "Sucess!",
                            message:"Document assment Submited Successfully!",
                            preferredStyle: .alert)
                        let okAction = UIAlertAction(
                            title: "Okay",
                            style: .default){ action -> Void in
                                // self.navigationController?.popViewController(animated: true)
                                //                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                //                                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "ManagemyProjectVC") as! ManagemyProjectVC
                                //                                    //   self?.present(newViewController, animated: true, completion: nil)
                                //                                    self.navigationController?.pushViewController(newViewController, animated: true)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    catch
                    {
                        //  self.appdelegate?.activityIndicator.stopAnimating()
                        print("Something went wrong")
                    }
                    
                }
                else if statusCode == 400
                {
                    do{
                        //                            self.appdelegate?.activityIndicator.stopAnimating()
                        let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as! NSDictionary
                        //                            let swiftyjson = JSON(json)
                        //                            let alert = UIAlertController(
                        //                                title: "Alert!",
                        //                                message:"Something went wrong\nPlease try again later",
                        //                                preferredStyle: .alert)
                        //                            let okAction = UIAlertAction(
                        //                                title: "Okay",
                        //                                style: .default){ action -> Void in
                        //                            }
                        //                            alert.addAction(okAction)
                        //                            self.present(alert, animated: true, completion: nil)
                        
                    }
                    catch
                    {
                        //   self.appdelegate?.activityIndicator.stopAnimating()
                        print("Something went wrong")
                    }
                }
                    
                else
                {
                    //  self.appdelegate?.activityIndicator.stopAnimating()
                    let alert = UIAlertController(
                        title: "Alert!",
                        message:"Something went wrong\nPlease try again later",
                        preferredStyle: .alert)
                    let okAction = UIAlertAction(
                        title: "Okay",
                        style: .default){ action -> Void in
                    }
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        case .failure(let encodingError):
            //self.delegate?.showFailAlert()
            //      self.submitButton.isEnabled = true
            print(encodingError)
        }
        
    }
    
    
    }
    
    
    
    func  withparametera()  {
        
        let userqutiondetails = appdelegate?.setloanQuestiondetails.Loanquestiondetails
        let productdetails = appdelegate?.setproductDetails.productdetails
        let userId = UserDefaults.standard.string(forKey: "_User_Id")!
        let ProductNumber = (appdelegate?.seteselectedbankdeails.selectedBankDetails["Product_id"].string!)!
        let ProductID = appdelegate?.setproductDetails.productdetails["prodId"].string!
        let LenderID = appdelegate?.seteselectedbankdeails.selectedBankDetails["user_code"].string!
        let ModelName = appdelegate?.setproductDetails.productdetails["modelName"].string!
        let MakeName = appdelegate?.setproductDetails.productdetails["makeName"].string!
        let UserName = userqutiondetails!["First_Name"].string!
        let MobileNo = userqutiondetails!["Mobile_No"].string!
        let CatName = "Two Wheeler"
        let Applied3Months = "0"
        let Lessthan45 = "0"
        let fourWheeler = "0"
        let Samebank = "0"
        let CoApplicant = appdelegate?.setloanapplaydic.CoApplicant ?? ""
        let Mname = appdelegate?.setloanapplaydic.Mname ?? ""
        let Fname = appdelegate?.setloanapplaydic.Fname ?? ""
        let Hname = "null"
        let Nationality =  appdelegate?.setloanapplaydic.Nationality ?? ""
        let Proof = appdelegate?.setloanapplaydic.Proof ?? ""
        let Noofdependency = appdelegate?.setloanapplaydic.Noofdependency ?? ""
        let EduDetails = appdelegate?.setloanapplaydic.EduDetails ?? ""
        let Religion = "0"
        let RefName = appdelegate?.setloanapplaydic.RefName ?? ""
        let RefRelation = appdelegate?.setloanapplaydic.RefRelationID ?? ""
        let RefMobile = appdelegate?.setloanapplaydic.RefMobile ?? ""
        let RefPinCode = appdelegate?.setloanapplaydic.RefPinCode ?? ""
        let RefAdd = appdelegate?.setloanapplaydic.RefAdd ?? ""
        let Ref1Name = appdelegate?.setloanapplaydic.Ref1Name ?? ""
        let Ref1Relation = appdelegate?.setloanapplaydic.Ref1RelationID ?? ""
        let Ref1Mobile = appdelegate?.setloanapplaydic.Ref1Mobile ?? ""
        let Ref1PinCode = appdelegate?.setloanapplaydic.Ref1Pincode ?? ""
        let Ref1Add = appdelegate?.setloanapplaydic.Ref1Add ?? ""
        let GrossAmt = "\(productdetails!["grossAmount"].float!)"
        let DownPayment = appdelegate?.selectedIteamlaondetails.Downpayment ?? ""
        let Tenure = appdelegate?.selectedIteamlaondetails.Tenure ?? ""
        let interestRate = appdelegate?.selectedIteamlaondetails.InterestRate ?? ""
        let MonthlyEMi = appdelegate?.selectedIteamlaondetails.EMI ?? ""
        let whois_seller = appdelegate?.setproductDetails.productdetails["sellerId"].string! ?? ""
        let co_applicant_name = appdelegate?.setloanapplaydic.co_applicant_name ?? ""
        let co_applicant_address = appdelegate?.setloanapplaydic.co_applicant_address ?? ""
        let co_applicant_income = appdelegate?.setloanapplaydic.co_applicant_income
        let ApplicantPhoto =  UploadDocuments["ApplicantPhoto"] ?? ""
        let IdentityProof =  UploadDocuments["IdentityProof"] ?? ""
//        if let applicationPhoto = UploadDocuments["ApplicantPhoto"]
//        {
//            ApplicantPhoto = UploadDocuments["ApplicantPhoto"] as Any
//        }
//        else
//        {
//            ApplicantPhoto = nil
//        }
//       // let ApplicantPhoto =  UploadDocuments["ApplicantPhoto"]
//        var IdentityProof :NSData?
//        if let idproof = UploadDocuments["IdentityProof"]
//        {
//            IdentityProof =  UploadDocuments["ApplicantPhoto"] as Any
//        }
//        else
//        {
//            IdentityProof = nil
//        }
        let IdentityProofExtensionz = self.IdentityProofExtension
        let PaySlips = UploadDocuments["PaySlips"] ?? ""
        let PaySlipsExtension = self.PaySlipsExtension
        let BankStmt =   UploadDocuments["BankStmt"] ?? ""
        let BankStmtExtension = self.BankStmtExtension
        let IncomeTaxReturn =   UploadDocuments["IncomeTaxReturn"] ?? ""
        let IncomeTaxReturnExtension = self.IncomeTaxReturnExtension
        let AgriLandDoc = UploadDocuments["AgriLandDoc"] ?? ""
        let AgriLandDocExtension = self.AgriLandDocExtension
        let RentalIncomeDoc = UploadDocuments["RentalIncomeDoc"] ?? ""
        let RentalIncomeDocExtension = self.RentalIncomeDocExtension
        let Bstatus = "null"
        
        let parameters = ["UserID":userId,"ProductNumber":ProductNumber,"ProductID":ProductID!,"LenderID":LenderID!,"UserName":UserName,"ModelName":ModelName!,"MakeName":MakeName!,"MobileNo":MobileNo,"CatName":CatName,"Applied3Months":Applied3Months,"Lessthan45":Lessthan45,"fourWheeler":fourWheeler,"Samebank":Samebank,"CoApplicant":CoApplicant,"Mname":Mname,"Fname":Fname,"Hname":Hname,"Nationality":Nationality,"Proof":Proof,"Noofdependency":Noofdependency,"EduDetails":EduDetails,"Religion":Religion,"RefName":RefName,"RefRelation":RefRelation,"RefMobile":RefMobile,"RefPinCode":RefPinCode,"RefAdd":RefAdd,"Ref1Name":Ref1Name,"Ref1Relation":Ref1Relation,"Ref1Mobile":Ref1Mobile,"Ref1PinCode":Ref1PinCode,"Ref1Add":Ref1Add,"GrossAmt":GrossAmt,"DownPayment":DownPayment,"Tenure":Tenure,"interestRate":interestRate,"MonthlyEMi":MonthlyEMi,"whois_seller":whois_seller,"co_applicant_name":co_applicant_name,"co_applicant_address":co_applicant_address,"co_applicant_income":co_applicant_income!,"ApplicantPhoto":ApplicantPhoto,"IdentityProof":IdentityProof,"IdentityProofExtension":IdentityProofExtension,"PaySlips":PaySlips,"PaySlipsExtension":PaySlipsExtension,"BankStmt":BankStmt,"BankStmtExtension":BankStmtExtension,"IncomeTaxReturn":IncomeTaxReturn,"IncomeTaxReturnExtension":IncomeTaxReturnExtension,"AgriLandDoc":AgriLandDoc,"AgriLandDocExtension":AgriLandDocExtension,"RentalIncomeDoc":RentalIncomeDoc,"RentalIncomeDocExtension":RentalIncomeDocExtension,"Bstatus":Bstatus] as [String : Any]
        
        print(parameters)
        AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"setPreApproval", viewController: self, params: parameters as [String : AnyObject], headers: nil, success: { (json, responce) in
            print(json)
            if json == 400
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the valid mobile number and password")
            }
            else
            {
                if json["Status"] == true
                {
                    
                    let alert = UIAlertController(title: "Sucess", message: "Your Loan is Under process", preferredStyle: UIAlertControllerStyle.alert);
                    let OK = UIAlertAction(
                        title: "Thankyou",
                        style: .default){ action -> Void in
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: HomeViewController.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                            //other action
                    }
                    alert.addAction(OK)
                    
                    
                    // relate actions to controllers
                    
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }) { (erro) in
            
            print(erro)
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PRE APPROVAL"
    }
    }
    
    
//    {
//    self.submitButton.isEnabled = false
//    let userdetails = appdelegate?.details
//    let id = "\(userdetails!["id"].int!)"
//    let assessment_master_id = "\(4)"
//    let property_master_string = "\(property_master_id)"
//    let sitePlan = uploadPlanimageArray
//
//    let addpropertyApi = My_Apis.GHP_api+"property_assessment_request_details"
//    let headers: HTTPHeaders = [
//    "X-User-Token": userdetails!["authentication_token"].string!,
//    "Accept": "application/json",
//    "Content-type": "multipart/form-data"
//    ]
//    let parameters = ["property_assessment_request_detail" :["created_by_id":id,"assessment_master_id":assessment_master_id,"property_master_id":property_master_string,"updated_by_id":id,"sitePlan":sitePlan,]]
//
//    Alamofire.upload(multipartFormData: { (multipartFormData) in
//    //            for imageData in self.imageArray {
//    //                                    multipartFormData.append(UIImageJPEGRepresentation(imageData,0.5)!, withName: "attachPlans", fileName: "attachPlans", mimeType: "image/jpeg")
//    //                                }
//    for (key, value) in (parameters["property_assessment_request_detail"])! {
//    if key == "sitePlan"
//    {
//    let imagearray = value as! Array<Any>
//    //          print(value as! Array<UIImage>)
//    for image  in imagearray
//    {
//    if ((image) as AnyObject) is UIImage
//    {
//    multipartFormData.append(UIImageJPEGRepresentation((image as! UIImage),0.5)!, withName: "property_assessment_request_detail[\(key)][]", fileName: "attachPlans", mimeType: "image/jpeg")
//    }
//    else
//    {
//    let pdfData = try! Data(contentsOf:  (image as! String).asURL())
//    let data : Data = pdfData
//    print("data:\(data)")
//    multipartFormData.append(data, withName: "property_assessment_request_detail[\(key)][]", fileName: "attachPlans", mimeType: "application/pdf")
//    }
//    }
//    //
//    }
//    else
//    {
//    multipartFormData.append((value as AnyObject).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue)!, withName: "property_assessment_request_detail[\(key)]")
//    }
//    //    multipartFormData.append("".data, withName: key)
//    }
//    }, to: addpropertyApi, headers: headers)
//    { (result) in
//    switch result {
//    case .success(let upload, _, _):
//
//    upload.uploadProgress(closure: { (Progress) in
//    print("Upload Progress: \(Progress.fractionCompleted)")
//    })
//    self.submitButton.isEnabled = true
//    upload.responseJSON { response in
//    //self.delegate?.showSuccessAlert()
//    print(response.request!)  // original URL request
//    print(response.response!) // URL response
//    print(response.data!)     // server data
//    print(response.result)  // result of response serialization
//    let statusCode = response.response?.statusCode
//    if statusCode == 201 {
//
//    do{
//    self.appdelegate?.activityIndicator.stopAnimating()
//    let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as! NSDictionary
//    //     let swiftyjson = JSON(json)
//    //   UserDefaults.standard.set(json, forKey: "userdetails")
//    let alert = UIAlertController(
//    title: "Sucess!",
//    message:"Document assment Submited Successfully!",
//    preferredStyle: .alert)
//    let okAction = UIAlertAction(
//    title: "Okay",
//    style: .default){ action -> Void in
//    // self.navigationController?.popViewController(animated: true)
//    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//    let newViewController = storyBoard.instantiateViewController(withIdentifier: "ManagemyProjectVC") as! ManagemyProjectVC
//    //   self?.present(newViewController, animated: true, completion: nil)
//    self.navigationController?.pushViewController(newViewController, animated: true)
//    }
//    alert.addAction(okAction)
//    self.present(alert, animated: true, completion: nil)
//
//
//    }
//    catch
//    {
//    self.appdelegate?.activityIndicator.stopAnimating()
//    print("Something went wrong")
//    }
//
//    }
//    else if statusCode == 401
//    {
//    do{
//    self.appdelegate?.activityIndicator.stopAnimating()
//    let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as! NSDictionary
//    let swiftyjson = JSON(json)
//    let alert = UIAlertController(
//    title: "Alert!",
//    message:"Something went wrong\nPlease try again later",
//    preferredStyle: .alert)
//    let okAction = UIAlertAction(
//    title: "Okay",
//    style: .default){ action -> Void in
//    }
//    alert.addAction(okAction)
//    self.present(alert, animated: true, completion: nil)
//
//    }
//    catch
//    {
//    self.appdelegate?.activityIndicator.stopAnimating()
//    print("Something went wrong")
//    }
//    }
//
//    else
//    {
//    self.appdelegate?.activityIndicator.stopAnimating()
//    let alert = UIAlertController(
//    title: "Alert!",
//    message:"Something went wrong\nPlease try again later",
//    preferredStyle: .alert)
//    let okAction = UIAlertAction(
//    title: "Okay",
//    style: .default){ action -> Void in
//    }
//    alert.addAction(okAction)
//    self.present(alert, animated: true, completion: nil)
//    }
//    }
//
//    case .failure(let encodingError):
//    //self.delegate?.showFailAlert()
//    self.submitButton.isEnabled = true
//    print(encodingError)
//    }
//
//    }
//    }


//}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


extension UploadDocumentsVc:UIDocumentMenuDelegate,UIDocumentPickerDelegate
{
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
        
    }
    
    public func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL)
    {
        print("document picked")
         sendertextfield.text = "Document selected"
        let myURL = url as URL
        let uploadPdfFieStr = String(describing: myURL)
      //  let pdfData = try! NSData(contentsOf:  uploadPdfFieStr.asURL())
         let pdfData = try! Data(contentsOf:  uploadPdfFieStr.asURL())
          let strBase64 = pdfData.base64EncodedString(options: .lineLength64Characters)
        print(pdfData)
     //  print()
        let pathextention = myURL.pathExtension
        if pathextention != "pdf"
        {
            let alert = UIAlertController(
                title: "Alert!",
                message:"please chose pdf file only",
                preferredStyle: .alert)
            let okAction = UIAlertAction(
                title: "Okay",
                style: .default){ action -> Void in
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            if sendertextfield == applicantPhot
            {
               // let uploadPdfFieStr = String(describing: myURL)
                // self.ApplicantPhoto.append(chosenImage)
                UploadDocuments["ApplicantPhoto"] = strBase64
               
               // self.uploadPlanimageArray.append(self.self.uploadPdfFieStr)
            }
            else if sendertextfield == paySlipTxfld
            {
                //  self.PaySlips = chosenImage
                UploadDocuments["PaySlips"] = strBase64
                PaySlipsExtension = pathextention
            }
            else if sendertextfield == applicantIdProof
            {
                // self.IdentityProof = chosenImage
                UploadDocuments["IdentityProof"] = strBase64
                IdentityProofExtension = pathextention
            }
            else if sendertextfield == bankStatement
            {
                UploadDocuments["BankStmt"] = strBase64
                BankStmtExtension = pathextention
            }
            else if sendertextfield == incomeProoof
            {
                UploadDocuments["IncomeTaxReturn"] = strBase64
                IncomeTaxReturnExtension = pathextention
            }
           
        }
        
    }
}

