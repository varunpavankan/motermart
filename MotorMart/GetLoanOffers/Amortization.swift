//
//  Amortization.swift
//  MotorMart
//
//  Created by Nennu on 20/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
class Amortization: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var paymentSummeryView: UIView!
    @IBOutlet weak var autoloneSummeryView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var totalamountLable: UILabel!
    @IBOutlet weak var totalintrestLable: UILabel!
    
    @IBOutlet weak var totalPriceValueLable: UILabel!
    
    @IBOutlet weak var otherChargesLable: UILabel!
    @IBOutlet weak var monthlyPaymentLable: UILabel!
    @IBOutlet weak var nopaymentsLable: UILabel!
    
    @IBOutlet weak var downpaymentLable: UILabel!
    @IBOutlet weak var tenureLable: UILabel!
    @IBOutlet weak var intrestRateValueLable: UILabel!
    
    @IBOutlet weak var productPriceValueLable: UILabel!
    var tableData = Array<[String:String]>()
    var productDetails = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()
        autoloneSummeryView.layer.borderColor = UIColor.lightGray.cgColor
        autoloneSummeryView.layer.borderWidth = 0.50
        autoloneSummeryView.layer.cornerRadius = 2
        autoloneSummeryView.clipsToBounds = true
       
        paymentSummeryView.layer.borderColor = UIColor.lightGray.cgColor
        paymentSummeryView.layer.borderWidth = 0.50
        paymentSummeryView.layer.cornerRadius = 2
        paymentSummeryView.clipsToBounds = true
        
        
        print(productDetails)
        productPriceValueLable.text = "₹"+"\(Int(Float(productDetails["GROSS_AMOUNT"].string!)!).withCommas())"
        intrestRateValueLable.text = productDetails["Interest_Rate_In_Pecntge"].string!+"%"
        tenureLable.text = productDetails["Term_in_Months"].string!+"months"
        let downpayment = (Float(productDetails["GROSS_AMOUNT"].string!)!/100) * Float(productDetails["Down_payment_Pecntage"].string!)!
        downpaymentLable.text = "₹"+"\(Int(downpayment).withCommas())"
        nopaymentsLable.text = productDetails["Term_in_Months"].string!
        
        
        let loanamount = Float(productDetails["GROSS_AMOUNT"].string!)! - downpayment
        let douAmt = Float(productDetails["Docmnt_Chrgs_absolute_Amnt"].string!)!
        let stampCost = Float(productDetails["Stamp_Costs"].string!)!
        let otherCost = Float(productDetails["Other_Costs"].string!)!
        let Insurance = Float(productDetails["Insurance"].string!)!
        let processingfee = (loanamount * Float(productDetails["Processing_Chrgs_Percmtge_loan_amnt"].string!)!) / 100;
        let loanAmmount =  loanamount + douAmt + processingfee + stampCost + otherCost + Insurance
        let othercharges = douAmt  + stampCost + otherCost + Insurance
        otherChargesLable.text = "₹\(othercharges.rounded().withCommas())"
        totalPriceValueLable.text = "₹\(loanAmmount.rounded().withCommas())"
        var emi = Float()
        if productDetails["interset_type"].string == "3"
        {
            emi = AlWrapper.emiCalculation_flaterate_typ(loanAmount: loanAmmount, intrest: Float(productDetails["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(productDetails["Term_in_Months"].string!)!)
            monthlyPaymentLable.text = "₹"+"\(emi.rounded().rounded().withCommas())"
            var totalamount = emi *  (Float(productDetails["Term_in_Months"].string!)!)
            totalintrestLable.text = "₹"+"\(Int(Float(totalamount) - loanAmmount).withCommas())"
            totalamountLable.text = "₹"+"\(Int(Float(totalamount)).withCommas())"
            
            let TotalInterest = Float(totalamount) - loanAmmount
            let Term_in_Months = Int(productDetails["Term_in_Months"].string!)!
            
             for notearms in 1...Term_in_Months
            {
                var dtrow = [String:String]()
                let _Monthly_intestAmount = TotalInterest / Float(Term_in_Months)
             //   var dtrow = dt.NewRow();
                dtrow["Payment"] = "\(notearms)"
                dtrow["Amount"] = "\(emi.withCommas())"
                dtrow["Principal"] = "\((emi - _Monthly_intestAmount).withCommas())"
                dtrow["Interest"] = "\(_Monthly_intestAmount.withCommas())";
                let balance = totalamount - emi
                totalamount = balance
                if balance < 0
                {
                     dtrow["Balance"] =  "0.00"
                }
                else
                {
                dtrow["Balance"] =  "\(balance.withCommas())"
                }
                tableData.append(dtrow)
            }
            
        }
        else
        {
            emi = AlWrapper.emiCalculation_Reducing_balance_type(loanAmount: loanAmmount, intrest: Float(productDetails["Interest_Rate_In_Pecntge"].string!)!, numberOfmonths: Int(productDetails["Term_in_Months"].string!)!)
              monthlyPaymentLable.text = "₹"+"\(emi.rounded().withCommas())"
            var totalamount = emi *  (Float(productDetails["Term_in_Months"].string!)!)
            totalintrestLable.text = "₹"+"\(Int(Float(totalamount) - loanAmmount).withCommas())"
            totalamountLable.text = "₹"+"\(Int(Float(totalamount)).withCommas())"
            let Term_in_Months = Int(productDetails["Term_in_Months"].string!)!
            var TotalInterest = Float(totalamount) - loanAmmount
          
            var balanceamt = loanAmmount
            for notearms in 0...Term_in_Months - 1
            {
                let irate = Float(productDetails["Interest_Rate_In_Pecntge"].string!);
                let Interest = balanceamt * ((irate! / 100) * (Float(1.0 / 12.0)));

               // decimal monthprincipal;
                let monthprincipal = Float(emi) - Interest;
                 balanceamt = balanceamt - monthprincipal;
                 //   totalamount = balanceamount

                TotalInterest += Interest;

                var dtrow = [String:String]()
                dtrow["Payment"] = "\(notearms + 1)"
                dtrow["Amount"] =  "\(emi.roundTo(places: 2).withCommas())"
                dtrow["Principal"] = "\(monthprincipal.roundTo(places: 2).withCommas())";
                dtrow["Interest"] = "\(Interest.roundTo(places: 2).withCommas())"
                if (balanceamt < 0)
                {
                dtrow["Balance"] = "0.00"
                }
                else
                {
                dtrow["Balance"] = "\(balanceamt.roundTo(places: 2).withCommas())"
                }
               
                 tableData.append(dtrow)
            }
            
        }
        print(tableData)
        self.tableView.reloadData()
        
//        let totalIntrest = (loanAmmount/100) * Float(productDetails["Interest_Rate_In_Pecntge"].string!)!
//        let r = Double(productDetails["Interest_Rate_In_Pecntge"].string!)!/1200
//        let rPower = pow(1 + Double(r), Double(Int(productDetails["Term_in_Months"].string!)!))
//        let monthlyIntrest = Double(loanAmmount) * r * rPower / (rPower - 1)
       
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "AMORTIZATION"
    }

    
//    for (var notearms = 1; notearms <= Term_in_Months; notearms++)
//    {
//    // decimal irate = interest_rate;
//
//
//    var _Monthly_intestAmount = TotalInterest / Term_in_Months;
//    var dtrow = dt.NewRow();
//    dtrow["Payment"] = notearms;
//    dtrow["Amount"] = IndianFormat(_Monthly_EMI.ToString(),"2");
//    dtrow["Principal"] = IndianFormat((_Monthly_EMI - _Monthly_intestAmount).ToString(), "2") ;
//    dtrow["Interest"] = IndianFormat(_Monthly_intestAmount.ToString(),"2");
//    dtrow["Balance"] = IndianFormat((balanceamount - (notearms * _Monthly_EMI)).ToString(), "2") ;
//    dt.Rows.Add(dtrow);
//    }
//
//
//    for (var notearms = 0; notearms < Term_in_Months; notearms++)
//    {
//
//
//
//
//    decimal irate = interest_rate;
//    var Interest = balanceamount * ((irate / 100) * (Convert.ToDecimal(1.0 / 12.0)));
//
//    decimal monthprincipal;
//    monthprincipal = Convert.ToDecimal(_Monthly_EMI) - Interest;
//    balanceamount = balanceamount - monthprincipal;
//
//    TotalInterest += Interest;
//
//    var dtrow = dt.NewRow();
//    dtrow["Payment"] = notearms + 1;
//    dtrow["Amount"] = IndianFormat(_Monthly_EMI.ToString(), "2");
//    dtrow["Principal"] = IndianFormat(monthprincipal.ToString(), "2");
//    dtrow["Interest"] = IndianFormat(Interest.ToString(), "2");
//    if (balanceamount < 0)
//    dtrow["Balance"] = "0.00";
//    else
//    dtrow["Balance"] = IndianFormat(balanceamount.ToString(), "2");
//    dt.Rows.Add(dtrow);
//    }

    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TABLE VIEW METHODS
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = self.tableView.dequeueReusableCell(withIdentifier: "AmortizationHeaderCell") as? AmortizationHeaderCell
      
        return headerView?.contentView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 30
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
   
        return tableData.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 30
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AmortizationCell = self.tableView.dequeueReusableCell(withIdentifier:"AmortizationCell") as! AmortizationCell

         let dic = tableData[indexPath.row]
        cell.amountLable.text = dic["Amount"]
        cell.priniciplalLable.text = dic["Principal"]
        cell.InterestLable.text = dic["Interest"]
        cell.serialNOLable.text = dic["Payment"]
        cell.balanceLable.text = dic["Balance"]
        return cell
        
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class AmortizationCell: UITableViewCell {
    
    @IBOutlet weak var balanceLable: UILabel!
    @IBOutlet weak var serialNOLable: UILabel!
    
    @IBOutlet weak var InterestLable: UILabel!
    @IBOutlet weak var amountLable: UILabel!
    
    @IBOutlet weak var priniciplalLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class AmortizationHeaderCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
