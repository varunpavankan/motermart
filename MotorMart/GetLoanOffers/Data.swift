//
//  File.swift
//  MotorMart
//
//  Created by Nennu on 23/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import Foundation
import SwiftyJSON
public class loanapproval
{
    public var UserID =  String()
    public var ProductNumber =  String()
    public var ProductID =  String()
    public var LenderID  =  String()
    public var Applied3Months  =  String()
    public var Lessthan45  =  String()
    public var fourWheeler  =  String()
    public var Samebank  =  String()
    public var CoApplicant  =  String()
    public var Mname  =  String()
    public var Fname  =  String()
    public var Hname  =  String()
    public var Nationality  =  String()
    public var Proof  =  String()
    public var Noofdependency  =  String()
    public var EduDetails  =  String()
    public var Religion  =  String()
    public var RefName  =  String()
    public var RefRelation  =  String()
    public var RefRelationID = String()
    public var RefMobile  =  String()
    public var RefPinCode  =  String()
    public var RefAdd  =  String()
    public var Ref1Name  =  String()
    public var Ref1Relation  =  String()
    public var Ref1RelationID = String()
    public var Ref1Mobile  =  String()
    public var Ref1Pincode  =  String()
    public var Ref1Add  =  String()
    public var GrossAmt  =  Float()
    public var DownPayment  =    Float()
    public var Tenure  =  Float()
    public var interestRate  =   Float()
    public var MonthlyEMi  =  Float()
    public var whois_seller  =  String()
    public var co_applicant_name  =  String()
    public var co_applicant_address  =  String()
    public var co_applicant_income  =  Float()
    public var ApplicantPhoto  =  String()
    public var IdentityProof  =  String()
    public var PaySlips  =  String()
    public var BankStmt  =  String()
    public var IncomeTaxReturn  =  String()
    public var AgriLandDoc  =  String()
    public var RentalIncomeDoc  =  String()
    public var MStatus  =  String()
    public var Bstatus  =  Int()
    
}

public class selectedIteamLaonDetails
{
    public var loanOrgnizationName =  String()
    public var LoanAmount =  String()
    public var InterestRate =  String()
    public var ProductID =  String()
    public var Tenure  =  String()
    public var Downpayment  =  String()
    public var LTV  =  String()
    public var EMI  =  String()
    public var DocumentCharges  =  String()
    public var ProcessingCharges  =  String()
    public var StampCost  =  String()
    public var Othercost  =  String()
    public var Foreclosurecharges  =  String()
    public var Prepaymentpenalties  =  String()
    public var Checquebouncecharges  =  String()
    public var Seizurepenalty  =  String()
    public var Guarantorrequirement  =  String()
    public var TotalInterestPayable  =  String()
    public var TotalOtheronetimecosts  =  String()
    
}
public class setselectedbankdetails
{
    var selectedBankDetails = JSON()
}

public class setproductdetails
{
    var productdetails = JSON()
    
}

public class setLoanQuestionDetails
{
    var Loanquestiondetails = JSON()
}
public class setselectionDetails
{
    var dic = [String:String]()
}


