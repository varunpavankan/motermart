//
//  FilterVC.swift
//  MotorMart
//
//  Created by Nennu on 29/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class FilterVC: UIViewController,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var tenureValueLable: UILabel!
    
    @IBOutlet weak var downPaymentValueLable: UILabel!
    @IBOutlet weak var downPaymentSlider: UISlider!
    
    @IBOutlet weak var tenureInMonthSlider: UISlider!
    @IBOutlet weak var tenureInMonthLable: UILabel!
    @IBOutlet weak var DownPayment: UILabel!
    var tenureBool = false
    var downpaymentBool  = false
    var dic = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
//        ["MinumtenureMonths":minumTenureMonths!,"MaximumTenureMonths":maxTenureMonths!,"Listdownpayment":listdownPaymentValue,"GrossAmount":productDetails["Price"]!] as [String : Any]
        print(dic)
        tenureInMonthSlider.minimumValue = Float(dic["MinumtenureMonths"] as! String)!
        tenureInMonthSlider.maximumValue = Float(dic["MaximumTenureMonths"] as! String)!
        
        downPaymentSlider.minimumValue =  Float(dic["Listdownpayment"] as! String)!
        downPaymentSlider.maximumValue =  Float(dic["GrossAmount"] as! String)!
        
        
        downPaymentValueLable.text = "\(Int(downPaymentSlider.minimumValue))/\(Int(downPaymentSlider.maximumValue))"
        tenureValueLable.text = "\(Int(tenureInMonthSlider.minimumValue))/\(Int(tenureInMonthSlider.maximumValue))"
        // Do any additional setup after loading the v
       
    }
    @IBAction func didSlideOnTenureInMoths(_ sender: UISlider) {
    print(tenureInMonthSlider.value)
        tenureBool = true
        let value = ((Int(tenureInMonthSlider.value) + (Int(tenureInMonthSlider.minimumValue)/2))/Int(tenureInMonthSlider.minimumValue)) * Int(tenureInMonthSlider.minimumValue)
        tenureInMonthSlider.value = Float(value)
        tenureValueLable.text = "\(Int(value))/\(Int(tenureInMonthSlider.maximumValue))"
        }
    
    @IBAction func didTapOnDownPaymentSlider(_ sender: Any) {
      //   print(downPaymentSlider.value)
        downpaymentBool = true
     //   downPaymentSlider.value = Float(value)
        downPaymentValueLable.text = "\(Int( downPaymentSlider.value))/\(Int(downPaymentSlider.maximumValue))"
    }
    
    @IBAction func didTapClearbutton(_ sender: UIButton) {
    //    print("Cancle button tapped")
      //  FilterCancle
        NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "FilterCancle"), object:nil, userInfo:nil)
        self.navigationController?.popViewController(animated: true)
    }

    
    
    @IBAction func didTapOnApplyButton(_ sender: UIButton) {
        NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "FilterApply"), object:nil, userInfo: ["FliterbyDownPayment":downPaymentSlider.value,"FilterByTenure":tenureInMonthSlider.value,"tenureBool":tenureBool,"downpaymentBool":downpaymentBool])
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
