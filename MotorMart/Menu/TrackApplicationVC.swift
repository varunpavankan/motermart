//
//  TrackApplicationVC.swift
//  MotorMart
//
//  Created by Nennu on 03/08/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class TrackApplicationVC: UIViewController {
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLable: UILabel!
    @IBOutlet weak var commentsLable: UILabel!
    
    @IBOutlet weak var monthlyEmiLabel: UILabel!
    @IBOutlet weak var ProductNameLabl: UILabel!
    
    @IBOutlet weak var downpaymentLable: UILabel!
    @IBOutlet weak var tenurelable: UILabel!
    @IBOutlet weak var loanamountlable: UILabel!
    @IBOutlet weak var interestlable: UILabel!
    @IBOutlet weak var sellerlable: UILabel!
    @IBOutlet weak var warningView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        statusView.layer.cornerRadius = 5
        statusView.clipsToBounds = true
        
        getapplicationStatus()

        // Do any additional setup after loading the view.
    }
    func getapplicationStatus()
    {
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_PreApprovalStatus?Userid=\(UserDefaults.standard.string(forKey: "_User_Id")!)", viewController: self, success: { (json2,responce) in
            print(json2)
            if json2.isEmpty == true
            {
                self.warningView.isHidden = false
                self.statusView.isHidden = true
            }
            else{
                
                self.warningView.isHidden = true
                self.statusView.isHidden = false
                
                print(json2)
                let dic = json2[0].dictionary
                self.loanamountlable.text = " ₹"+(dic?["GROSS_AMOUNT"]?.string)!
                self.statusLable.text = " "+(dic?["Status"]?.string)!
                self.tenurelable.text = " "+(dic?["Term_in_Months"]?.string)!
                self.sellerlable.text = " "+(dic?["SellerName"]?.string)!
                self.monthlyEmiLabel.text = " ₹"+(dic?["MonthlyEMI"]?.string)!
                self.commentsLable.text = " "+(dic?["Comments"]?.string)!
                self.ProductNameLabl.text = " "+(dic?["Model_name"]?.string)!
                self.interestlable.text = "%"+(dic?["Interest_Rate_In_Pecntge"]?.string)!
                self.downpaymentLable.text = " ₹"+(dic?["Down_payment_Pecntage"]?.string)!
//                    {
//                        "GROSS_AMOUNT" : "63551.00",
//                        "Status" : "In process",
//                        "Term_in_Months" : "12",
//                        "SellerName" : "Multibrand Dealer Sole Proprietorship",
//                        "LenderID" : "LID20181408",
//                        "Product_id" : "1129",
//                        "Interest_Rate_In_Pecntge" : "9.00",
//                        "Model_name" : "ACTIVA 4G",
//                        "Down_payment_Pecntage" : "12510.20",
//                        "Name_of_loan" : "Auto Loans",
//                        "Comments" : "",
//                        "MonthlyEMI" : "5163.28"
//                }
              //  self.performSegue(withIdentifier: "loanList1", sender:nil)
            }
        },failure: { (error) in
            
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
