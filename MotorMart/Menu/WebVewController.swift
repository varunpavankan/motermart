
//
//  WebVewController.swift
//  MotorMart
//
//  Created by Nennu on 10/08/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class WebVewController: UIViewController,UIScrollViewDelegate {
let webView = UIWebView()
    var urlString = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: urlString)
      //  let url = userinfo!["FilePath"] as! URL
        let req = URLRequest(url: url!)
        DispatchQueue.main.async {
            self.webView.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.webView.loadRequest(req)
            self.webView.scalesPageToFit = true
            self.webView.scrollView.delegate = self; // set delegate method of UISrollView
            self.webView.scrollView.maximumZoomScale = 20; // set as you want.
            self.webView.scrollView.minimumZoomScale = 1;
            self.view.addSubview(self.webView)
            self.view.removeBluerLoader()
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
