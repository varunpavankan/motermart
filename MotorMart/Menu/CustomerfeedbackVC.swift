//
//  CustomerfeedbackVC.swift
//  MotorMart
//
//  Created by Nennu on 03/08/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class CustomerfeedbackVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var sendbutton: UIButton!
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        textView.text = "Give Feedback Hear"
        textView.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Give Feedback Hear"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func didTapOnSendButton(_ sender: Any) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
