//
//  UpdateKYC.swift
//  MotorMart
//
//  Created by Nennu on 03/08/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class UpdateKYC: UIViewController {
  //  var transitionManager: HalfSheetPresentationManager!
    var parseDic:[String:String]?
    var userdetailsdic = [String:String]()
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        print(result.value)
        let file = "file.xml" //this is the file. we will write to and read from it
        
        //  let text = "some text"
        
        print(result.metadataType)
        dismiss(animated: true) { [weak self] in
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                
                let fileURL = dir.appendingPathComponent(file)
                
                //writing
                do {
                    try result.value.write(to: fileURL, atomically: false, encoding: .utf8)
                }
                catch {/* error handling here */}
                
                //reading
                do {
                    if let parser = XMLParser(contentsOf: fileURL) {
                     //   parser.delegate = self
                        parser.parse()
                    }
                    else
                    {
                        AlWrapper.showAlertMessage(vc: self!, titleStr: "Alert!", messageStr: "You Have Choosen Wrong QR Code!")
                        
                    }
                }
                catch {/* error handling here */}
            }
        }
    }
    
    
    @IBAction func didTapOnUpdateButton(_ sender: Any) {
        
    }
    
    
    
    @IBAction func didTapOnNextButton(_ sender: Any) {
        //     self.performSegue(withIdentifier: "personalDetailSegue", sender:nil)
        
        
        if let dic = self.parseDic{
            
            if enterPanCardNumTfd.text?.isEmpty != true
            {
                var dic1 = [String:String]()
                dic1 = dic
                dic1["PanNum"] = enterPanCardNumTfd.text
                //  dic["uid"] = adharCardNoTfd.te
                self.performSegue(withIdentifier: "personalDetailSegue", sender:dic1)
                
            }
            else
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Pan Card Number")
            }
            
            //  self.performSegue(withIdentifier: "personalDetailSegue", sender:dic)
        }
        else
        {
            var dic = [String:String]()
            if adharCardNoTfd.text?.isEmpty != true
            {
                if enterPanCardNumTfd.text?.isEmpty != true
                {
                    dic["PanNum"] = enterPanCardNumTfd.text
                    dic["uid"] = adharCardNoTfd.text
                     let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                //    self.performSegue(withIdentifier: "personalDetailSegue", sender:dic)
                    let mainViewController = storyBoard.instantiateViewController(withIdentifier: "UpdatePersonalDetailsVc") as! UpdatePersonalDetailsVc
                    mainViewController.title = "KYC"
                    // UIApplication.shared.delegate?.window??.rootViewController = nvc
                    self.navigationController?.pushViewController(mainViewController, animated: false)
                    
                }
                else
                {
                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Pan Card Number")
                }
                
            }
            else
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the aadhar details")
                
            }
            
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "KYC"
        adharCardNoTfd.keyboardType = UIKeyboardType.numberPad
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //   print(sender)
        navigationItem.title = nil
        if segue.identifier == "personalDetailSegue"{
            let vc = segue.destination as! PersonalDetailsVc
            let keepingCurrent = userdetailsdic.merging(sender as! [String:String]) { (current, _) in current }
            print(keepingCurrent)
            vc.userdetailsdic = keepingCurrent
            //   vc.navigationItem.backBarButtonItem?.title = ""
        }
    }
    
    
    @IBOutlet weak var adharCardNoTfd: UITextField!
    
    @IBOutlet weak var enterPanCardNumTfd: UITextField!
    
    @IBOutlet weak var qrCodeScanImgView: UIImageView!
    
    @IBOutlet weak var qrCodeScanBgViewObj: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "aadhar_card.jpg",textfield:adharCardNoTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "pan_card.jpg",textfield:enterPanCardNumTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        
    }
   
    @IBAction func nextBtnClicked(_ sender: Any) {
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}


//
//  PersonalDetailsVc.swift
//  MotorMart
//
//  Created by Nennu on 14/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

class UpdatePersonalDetailsVc: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {
    
    var textfield :UITextField? = UITextField()
    @IBOutlet weak var selectGenderTxfld: UITextField!
    @IBOutlet weak var firstNmeTfd: UITextField!
    
    @IBOutlet weak var middleNmeTfd: UITextField!
    
    @IBOutlet weak var lastNmeTfd: UITextField!
    
    
    @IBOutlet weak var dobTfd: UITextField!
    
    @IBOutlet weak var mobileNumTfd: UITextField!
    
    @IBOutlet weak var emailTfd: UITextField!
    
    @IBOutlet weak var addressTfd: UITextField!
    
    @IBOutlet weak var selectCityTfd: UITextField!
    
    @IBOutlet weak var noOfYrsInCurrentCityTfd: UITextField!
    
    @IBOutlet weak var residencyTypeTfd: UITextField!
    let datePickerView:UIDatePicker = UIDatePicker()
    var userdetailsdic = [String:String]()
    var cityID = String()
    var pickerData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userdetailsdic)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:firstNmeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:middleNmeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:lastNmeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "Calender.png",textfield:dobTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:mobileNumTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "messages.png",textfield:emailTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            "personImage.png",textfield:selectCityTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            "selectgender.png",textfield:selectGenderTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:noOfYrsInCurrentCityTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:addressTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:residencyTypeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectCityTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectGenderTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:residencyTypeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        
        //        [{"_User_Id":"1829","_User_name":"Naveen","_User_code":"UID201818281","_Mobile_No":"9393939393","_EmailId":"kkkk9494456@gmail.com","_Password":null,"_oldPassword":null,"_OTP":null,"_Org_Name":null,"_Partner_Type":null,"_Address":null,"_Role":"4"}]
        
        
        self.firstNmeTfd.text = UserDefaults.standard.string(forKey: "_User_name")
        self.mobileNumTfd.text = UserDefaults.standard.string(forKey: "_Mobile_No")
        self.emailTfd.text = UserDefaults.standard.string(forKey: "_EmailId")
        if userdetailsdic.count < 2
        {
            if userdetailsdic["uid"] != nil
            {
                
            }
            else
            {
                
            }
        }
        else
        {
            print(userdetailsdic)
            
            //            ["state": "Andhra Pradesh", "name": "Chalamalasetty Varun Pavan Kanth", "dist": "Krishna", "house": "20/352A", "street": "CHILAKALAPUDI", "gender": "M", "uid": "507710508202", "yob": "1993", "vtc": "Chilakalapudi (rural)", "lm": "OPP. PANDURANGA HIGH SCHOOL", "co": "S/O Chalamalasetty Nagamalleswararao", "loc": "CHILAKALAPUDI", "pc": "521002"]
            
            
            if userdetailsdic["house"] != nil {
                self.firstNmeTfd.text = userdetailsdic["name"]
                addressTfd.text = "\(userdetailsdic["house"],userdetailsdic["lm"],userdetailsdic["street"],userdetailsdic["dist"],userdetailsdic["state"])"
                dobTfd.text = userdetailsdic["yob"]
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy" //Your date format
                dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
                let date = dateFormatter.date(from: userdetailsdic["yob"]!)
                let now = Date()
                let birthday: Date = date!
                let calendar = Calendar.current
                
                let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
                let age = ageComponents.year!
                userdetailsdic["AGE"] = "\(age)"
                
                if userdetailsdic["gender"] == "M"
                {
                    selectGenderTxfld.text = "Male"
                    
                }
                else if userdetailsdic["gender"] == "F"
                {
                    selectGenderTxfld.text = "Female"
                }
                else
                {
                    selectGenderTxfld.text = "Others"
                }
                
            }
            
        }
        
        //setupTextfieldrightViewwithborder
        // Do any additional setup after loading the view.
    }
    
    @IBAction func previousBtnClicked(_ sender: Any) {
        
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
        //  profisinalDetailsSegue
        if firstNmeTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter First Name")
            firstNmeTfd.becomeFirstResponder()
            
        }
        else if lastNmeTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Last Name")
            lastNmeTfd.becomeFirstResponder()
        }
        else if selectGenderTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select Gender")
            selectGenderTxfld.becomeFirstResponder()
        }
        else if dobTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select DOB")
            dobTfd.becomeFirstResponder()
        }
        else if mobileNumTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Mobile Number")
            mobileNumTfd.becomeFirstResponder()
        }
        else if emailTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter EmailID")
            emailTfd.becomeFirstResponder()
        }
        else if selectCityTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select City")
            selectCityTfd.becomeFirstResponder()
        }
        else if noOfYrsInCurrentCityTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Number of years you are staying in current city")
            noOfYrsInCurrentCityTfd.becomeFirstResponder()
        }
        else if residencyTypeTfd.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select Residence Type")
            residencyTypeTfd.becomeFirstResponder()
        }
        else
        {
            userdetailsdic["FNAME"] = firstNmeTfd.text
            userdetailsdic["LNAME"] = lastNmeTfd.text
            userdetailsdic["GENDER"] = selectGenderTxfld.text
            // userdetailsdic["AGE"] = dobTfd.text
            userdetailsdic["MOBILE"] = mobileNumTfd.text
            userdetailsdic["EMAIL"] = emailTfd.text
            userdetailsdic["CITY"] = cityID
            userdetailsdic["ResidentType"] = residencyTypeTfd.text
            userdetailsdic["NoOfYearsinCity"] = noOfYrsInCurrentCityTfd.text
            
            
            
            self.performSegue(withIdentifier: "profisinalDetailsSegue", sender:userdetailsdic)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //   print(sender)
        navigationItem.title = nil
        if segue.identifier == "profisinalDetailsSegue"{
            let vc = segue.destination as! ProfessionalDetailsVC
            vc.userdetailsdic = sender as! [String:String]
            //   vc.navigationItem.backBarButtonItem?.title = ""
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        //        if textField == selectGenderTxfld || (residencyTypeTfd != nil) || (selectCityTfd != nil) || (dobTfd != nil)
        //        {
        //        return false
        //        }
        //        else
        //        {
        //            return true
        //        }
        self.textfield = textField
        pickerData.removeAll()
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        switch textField {
        case selectGenderTxfld:
            pickerData.removeAll()
            pickerData = ["Male","Female","Others"]
            selectGenderTxfld.inputView = picker
            return true
        case residencyTypeTfd:
            pickerData.removeAll()
            pickerData = ["Own","Rented"]
            residencyTypeTfd.inputView = picker
            // self.textfield = sender
            return true
        case selectCityTfd:
            pickerData.removeAll()
            let dic = appdelegate?.citylist.array
            for val in dic!
            {
                pickerData.append(val["CityName"].string!)
            }
            selectCityTfd.inputView = picker
            return true
        case dobTfd:
            datePickerView.datePickerMode = UIDatePickerMode.date
            var components = DateComponents()
            components.year = -100
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = -18
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            dobTfd.inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
            return true
        default:
            return true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PERSONAL DETAILS"
    }
    
    
    @IBAction func didBegainEditing(_ sender: UITextField) {
        
        //        self.textfield = nil
        //         self.textfield = sender
        //         pickerData.removeAll()
        //        let picker = UIPickerView()
        //        picker.delegate = self
        //        picker.dataSource = self
        //       if sender == selectGenderTxfld
        //       {
        //        pickerData = ["Male","Female","Others"]
        //        sender.inputView = picker
        //        self.textfield = sender
        //        }
        //      else  if sender == residencyTypeTfd
        //        {
        //            pickerData = ["Own","Rented"]
        //            sender.inputView = picker
        //
        //        }
        //       else if sender == selectCityTfd {
        //            let dic = appdelegate?.citylist.array
        //            for val in dic!
        //            {
        //                pickerData.append(val["CityName"].string!)
        //            }
        //          //  print(appdelegate?.citylist as Any)
        //           // pickerData = ["Bangalor","Belgaum","Chennai","Hyderabad","Mangalore","Mumbai","Vijayawada"]
        //            sender.inputView = picker
        //            self.textfield = sender
        //        }
        //        else
        //       {
        //        datePickerView.datePickerMode = UIDatePickerMode.date
        //        var components = DateComponents()
        //        components.year = -100
        //        let minDate = Calendar.current.date(byAdding: components, to: Date())
        //
        //        components.year = -18
        //        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        //
        //        datePickerView.minimumDate = minDate
        //        datePickerView.maximumDate = maxDate
        //        dobTfd.inputView = datePickerView
        //
        //        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        //        }
        //
        //
        //        print("sendr selected")
    }
    @IBAction func textfieldendediting(_ sender: UITextField) {
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: sender.date, to: Date())
        let age = ageComponents.year!
        userdetailsdic["AGE"] = "\(age)"
        self.dobTfd.text = dateFormatter.string(from: sender.date)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - PickerView Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    //private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        print(pickerData[row])
    //        return pickerData[row]
    //    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textfield == selectCityTfd
        {
            selectCityTfd.text = pickerData[row]
            var city = appdelegate?.citylist[row].dictionary
            cityID = "\(city!["cityId"] ?? 0)"
        }
            
        else if textfield == selectGenderTxfld
        {
            selectGenderTxfld.text = pickerData[row]
        }
        else
        {
            residencyTypeTfd.text = pickerData[row]
            
        }
        
        //  textfield?.resignFirstResponder()
        //  picker.isHidden = true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
class UpdateProfessionalDetailsVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {
    
    
    var pickerData = [String]()
    var textfield = UITextField()
    var defaultsalariedViewheight :CGFloat = 0.0
    var defaultbusinessViewheight :CGFloat = 0.0
    var defaultothersViewheight :CGFloat = 0.0
    @IBOutlet weak var othersViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var businessView: UIView!
    @IBOutlet weak var bussinessViewConstraint:
    NSLayoutConstraint!
    @IBOutlet weak var salariedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var salariedView: UIView!
    @IBOutlet weak var selectIncomeTypeCatagory: UITextField!
    @IBOutlet weak var companyNameTxfld: UITextField!
    
    @IBOutlet weak var totalYearsOfexperiencTxfld: UITextField!
    
    @IBOutlet weak var bankAccountTxfld: UITextField!
    
    @IBOutlet weak var monthlyDeductions: UITextField!
    
    @IBOutlet weak var annualBonusTxfld: UITextField!
    
    @IBOutlet weak var otherannualIncomeTxfld: UITextField!
    
    @IBOutlet weak var emifirstVewTxfld: UITextField!
    
    @IBOutlet weak var grossMonthlyIncomeTxfld: UITextField!
    
    @IBOutlet weak var yearOfJoiningTxfld: UITextField!
    
    @IBOutlet weak var nameOfbussinessTxfld2view: UITextField!
    
    @IBOutlet weak var yearOfstartedTxfld: UITextField!
    
    @IBOutlet weak var netIncomeforlastYear3rdView: UITextField!
    
    @IBOutlet weak var existingEmi3rdView: UITextField!
    
    @IBOutlet weak var existingEmiTxfld2view: UITextField!
    
    @IBOutlet weak var profitAfterTaxTxfld2view: UITextField!
    var userdetailsdic = [String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userdetailsdic)
        selectIncomeTypeCatagory.text = "Salaried"
        defaultsalariedViewheight = self.salariedViewHeightConstraint.constant
        defaultothersViewheight = self.othersViewHeightConstraint.constant
        defaultbusinessViewheight = self.bussinessViewConstraint.constant
        if selectIncomeTypeCatagory.text == "Salaried"
        {
            self.othersView.isHidden = true
            self.othersViewHeightConstraint.constant = 0
            self.businessView.isHidden = true
            self.bussinessViewConstraint.constant = 0
            // self.salariedView.isHidden = true
            // self.salariedViewHeightConstraint.constant = 0
        }
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:selectIncomeTypeCatagory, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:companyNameTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:yearOfJoiningTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:totalYearsOfexperiencTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:bankAccountTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "indian_rupee_symbol.jpg",textfield:grossMonthlyIncomeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            "personImage.png",textfield:monthlyDeductions, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString:
            "indian_rupee_symbol.jpg",textfield:annualBonusTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "indian_rupee_symbol.jpg",textfield:otherannualIncomeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "indian_rupee_symbol.jpg",textfield:emifirstVewTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:nameOfbussinessTxfld2view, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:yearOfstartedTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "indian_rupee_symbol.jpg",textfield:existingEmiTxfld2view, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "personImage.png",textfield:profitAfterTaxTxfld2view, backgroundcolor: UIColor.blue,backgroundbool: false)
        
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "indian_rupee_symbol.jpg",textfield:netIncomeforlastYear3rdView, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftViewwithborder(imageString: "indian_rupee_symbol.jpg",textfield:existingEmi3rdView, backgroundcolor: UIColor.blue,backgroundbool: false)
        //        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:yearOfstartedTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        //        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectGenderTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        //        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:residencyTypeTfd, backgroundcolor: UIColor.blue,backgroundbool: false)
        //        companyNameTxfld.isHidden = true
        //exitesemiTxfd.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapOnNextButton(_ sender: Any) {
        
        let userode = UserDefaults.standard.string(forKey: "_User_Id")
        if selectIncomeTypeCatagory.text == "Salaried"
        {
            if companyNameTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter the Company Name")
                companyNameTxfld.becomeFirstResponder()
                
            }
            else if yearOfJoiningTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Year of Joining")
                yearOfJoiningTxfld.becomeFirstResponder()
            }
            else if totalYearsOfexperiencTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter total work experience")
                totalYearsOfexperiencTxfld.becomeFirstResponder()
            }
            else if bankAccountTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select Bank")
                bankAccountTxfld.becomeFirstResponder()
            }
            else if grossMonthlyIncomeTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Monthly Gross")
                grossMonthlyIncomeTxfld.becomeFirstResponder()
            }
            else if annualBonusTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Annual Bonus")
                annualBonusTxfld.becomeFirstResponder()
            }
            else if monthlyDeductions.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Monthly Deducations")
                monthlyDeductions.becomeFirstResponder()
            }
            else if otherannualIncomeTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter another Annual Income")
                otherannualIncomeTxfld.becomeFirstResponder()
            }
            else if emifirstVewTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter Existing EMI")
                emifirstVewTxfld.becomeFirstResponder()
            }
            else
            {
                //            ["Category_Id": "1", "state": "Andhra Pradesh", "NoOfYearsinCity": "20", "house": "20/352A", "GENDER": "Male", "ResidentType": "Rented", "MOBILE": "8686410525", "loan_name": "Two wheeler", "CITY": "Bangalor", "AGE": "1993", "vtc": "Chilakalapudi (rural)", "lm": "OPP. PANDURANGA HIGH SCHOOL", "co": "S/O Chalamalasetty Nagamalleswararao", "pc": "521002", "PanNum": "00000000", "loc": "CHILAKALAPUDI", "name": "Chalamalasetty Varun Pavan Kanth", "dist": "Krishna", "LNAME": "hello", "street": "CHILAKALAPUDI", "gender": "M", "EMAIL": "varunpavankan@gmail.com", "FNAME": "Chalamalasetty Varun Pavan Kanth", "uid": "507710508202", "yob": "1993", "ProductID": "VID2018137220180312011227445"]
                
                //            userdetailsdic["FNAME"] = firstNmeTfd.text
                //            userdetailsdic["LNAME"] = lastNmeTfd.text
                //            userdetailsdic["GENDER"] = selectGenderTxfld.text
                //            userdetailsdic["AGE"] = dobTfd.text
                //            userdetailsdic["MOBILE"] = mobileNumTfd.text
                //            userdetailsdic["EMAIL"] = emailTfd.text
                //            userdetailsdic["CITY"] = selectCityTfd.text
                //            userdetailsdic["ResidentType"] = residencyTypeTfd.text
                //            userdetailsdic["NoOfYearsinCity"] = noOfYrsInCurrentCityTfd.text
                //            let now = Date()
                //            let birthday: Date =
                //            let calendar = Calendar.current
                //
                //            let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
                //            let age = ageComponents.year!
                let uploadData=["ProductID":userdetailsdic["ProductID"]!,"Category_Id":userdetailsdic["Category_Id"]!,"loan_name":userdetailsdic["loan_name"]!,"first_name":userdetailsdic["FNAME"]!,"last_name":userdetailsdic["LNAME"]!,"gender":userdetailsdic["GENDER"]!,"DOB":userdetailsdic["AGE"]!,"MobileNo":userdetailsdic["MOBILE"]!,"EmailId":userdetailsdic["EMAIL"]!,"user_city":userdetailsdic["CITY"]!,"Selected_Lender_seller":0,"profession":"Salaried","company_name":self.companyNameTxfld.text!,"joining_date":self.yearOfJoiningTxfld.text!,"annual_income":self.grossMonthlyIncomeTxfld.text!,"annual_bonus":self.annualBonusTxfld.text!,"monthly_insentives":0,"emi_monthly":self.emifirstVewTxfld.text!,"profit":"0.00","salary_bankaccount":self.bankAccountTxfld.text!,"other_income":self.otherannualIncomeTxfld.text!,"nameof_business":"null","year_started":"null","net_income":"null","property_city":userdetailsdic["CITY"]!,"how_useloan":0,"property_type":0,"builder_name":0,"where_land":0,"cost_flat":0,"exist_bank":"noo","outstanding_amt":0,"exist_loan_date":0,"co_applicant":0,"co_applicant_name":0,"co_applicant_address":0,"co_applicant_income":0,"transaction_type":0,"whois_seller":0,"costof_construction":0,"exp_loan_amount":0,"User_Code":userode!,"middle_name":"null","NoOfYrs_city":5,"ResidentType":userdetailsdic["ResidentType"]!,"AadharNum":userdetailsdic["uid"]!,"PanNum":userdetailsdic["PanNum"]!,"Cibil":0,"colorID":0] as [String : Any]
                print(uploadData)
                AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"Set_KycDetails", viewController: self, params: uploadData as [String : AnyObject], headers: nil, success: { (json, responce) in
                    print(json)
                    if json == 2
                    {
                        AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "you are alredy registerd")
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Sucess", message: "Your KYC has been complted Sucessfully", preferredStyle: UIAlertControllerStyle.alert);
                        let OK = UIAlertAction(
                            title: "Thankyou",
                            style: .default){ action -> Void in
                                for controller in self.navigationController!.viewControllers as Array {
//                                    if controller.isKind(of: ProductSpecificationVC.self) {
//                                        self.navigationController!.popToViewController(controller, animated: true)
//                                        break
//                                    }
                                }
                                //other action
                        }
                        alert.addAction(OK)
                        
                        
                        // relate actions to controllers
                        
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                }) { (erro) in
                    
                    print(erro)
                }
                
            }
        }
        else if selectIncomeTypeCatagory.text == "Self Employed"
        {
            
            if nameOfbussinessTxfld2view.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter bussiness Name")
                nameOfbussinessTxfld2view.becomeFirstResponder()
                
            }
            else if yearOfstartedTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter In wich Year you started business")
                yearOfstartedTxfld.becomeFirstResponder()
            }
            else if profitAfterTaxTxfld2view.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Profit")
                profitAfterTaxTxfld2view.becomeFirstResponder()
            }
            else if existingEmiTxfld2view.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter Existing EMI")
                existingEmiTxfld2view.becomeFirstResponder()
            }
            else
            {
                //  var values = productvalues.ProductId()
                //   var producID = values.p
                
                let uploadData=["ProductID":userdetailsdic["ProductID"]!,"Category_Id":userdetailsdic["Category_Id"]!,"loan_name":userdetailsdic["loan_name"]!,"first_name":userdetailsdic["FNAME"]!,"last_name":userdetailsdic["LNAME"]!,"gender":userdetailsdic["GENDER"]!,"DOB":userdetailsdic["AGE"]!,"MobileNo":userdetailsdic["MOBILE"]!,"EmailId":userdetailsdic["EMAIL"]!,"user_city":userdetailsdic["CITY"]!,"Selected_Lender_seller":0,"profession":"Salaried","company_name":0,"joining_date":0,"annual_income":0,"annual_bonus":0,"monthly_insentives":0,"emi_monthly":self.existingEmiTxfld2view.text!,"profit":self.profitAfterTaxTxfld2view.text!,"salary_bankaccount":0,"other_income":0,"nameof_business":self.nameOfbussinessTxfld2view.text!,"year_started":self.yearOfstartedTxfld.text!,"net_income":"null","property_city":userdetailsdic["CITY"]!,"how_useloan":0,"property_type":0,"builder_name":0,"where_land":0,"cost_flat":0,"exist_bank":"noo","outstanding_amt":0,"exist_loan_date":0,"co_applicant":0,"co_applicant_name":0,"co_applicant_address":0,"co_applicant_income":0,"transaction_type":0,"whois_seller":0,"costof_construction":0,"exp_loan_amount":0,"User_Code":userode!,"middle_name":"null","NoOfYrs_city":5,"ResidentType":userdetailsdic["ResidentType"]!,"AadharNum":userdetailsdic["uid"]!,"PanNum":userdetailsdic["PanNum"]!,"Cibil":0,"colorID":0] as [String : Any]
                
                print(uploadData)
                AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"Set_KycDetails", viewController: self, params: uploadData as [String : AnyObject], headers: nil, success: { (json, responce) in
                    print(json)
                    if json == 400
                    {
                        AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the valid mobile number and password")
                    }
                    else
                    {
                        
                        
                    }
                }) { (erro) in
                    
                    print(erro)
                }
                
            }
            
        }
        else
        {
            if netIncomeforlastYear3rdView.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter net Income")
                netIncomeforlastYear3rdView.becomeFirstResponder()
                
            }
            else if existingEmi3rdView.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter Existing EMI")
                existingEmi3rdView.becomeFirstResponder()
            }
            else
            {
                let uploadData=["ProductID":userdetailsdic["ProductID"]!,"Category_Id":userdetailsdic["Category_Id"]!,"loan_name":userdetailsdic["loan_name"]!,"first_name":userdetailsdic["FNAME"]!,"last_name":userdetailsdic["LNAME"]!,"gender":userdetailsdic["GENDER"]!,"DOB":userdetailsdic["AGE"]!,"MobileNo":userdetailsdic["MOBILE"]!,"EmailId":userdetailsdic["EMAIL"]!,"user_city":userdetailsdic["CITY"]!,"Selected_Lender_seller":0,"profession":"Salaried","company_name":0,"joining_date":0,"annual_income":0,"annual_bonus":0,"monthly_insentives":0,"emi_monthly":self.existingEmi3rdView.text!,"profit":"0.00","salary_bankaccount":0,"other_income":0,"nameof_business":"null","year_started":"null","net_income":self.netIncomeforlastYear3rdView.text!,"property_city":userdetailsdic["CITY"]!,"how_useloan":0,"property_type":0,"builder_name":0,"where_land":0,"cost_flat":0,"exist_bank":"noo","outstanding_amt":0,"exist_loan_date":0,"co_applicant":0,"co_applicant_name":0,"co_applicant_address":0,"co_applicant_income":0,"transaction_type":0,"whois_seller":0,"costof_construction":0,"exp_loan_amount":0,"User_Code":userode!,"middle_name":"null","NoOfYrs_city":5,"ResidentType":userdetailsdic["ResidentType"]!,"AadharNum":userdetailsdic["uid"]!,"PanNum":userdetailsdic["PanNum"]!,"Cibil":0,"colorID":0] as [String : Any]
                
                print(uploadData)
                AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"Set_KycDetails", viewController: self, params: uploadData as [String : AnyObject], headers: nil, success: { (json, responce) in
                    print(json)
                    if json == 400
                    {
                        AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the valid mobile number and password")
                    }
                    else
                    {
                        
                        
                    }
                }) { (erro) in
                    
                    print(erro)
                }
            }
            
        }
        
    }
    
    //Mark select
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.textfield = textField
        let picker = UIPickerView()
        //        if textField == selectGenderTxfld || (residencyTypeTfd != nil) || (selectCityTfd != nil) || (dobTfd != nil)
        //        {
        //        return false
        //        }
        //        else
        //        {
        //            return true
        //        }
        picker.delegate = self
        picker.dataSource = self
        
        switch textField {
        case selectIncomeTypeCatagory:
            pickerData.removeAll()
            pickerData = ["Salaried","Self Employed","Others"]
            selectIncomeTypeCatagory.inputView = picker
            return true
        case bankAccountTxfld:
            pickerData.removeAll()
            pickerData = ["Andhra Bank","Axis Bank","Bank Of India","Canara Bank","HDFC Bank","State Bannk Of India","ICICI Bank","Induslnd Bank","IOB Bank"]
            bankAccountTxfld.inputView = picker
            return true
        default:
            return true
        }
    }
    
    
    
    // Mark pickerViewdelegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print(pickerData[row])
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textfield == selectIncomeTypeCatagory
        {
            selectIncomeTypeCatagory.text = pickerData[row]
            //  picker.isHidden = true
            switch selectIncomeTypeCatagory.text {
            case "Salaried":
                if defaultsalariedViewheight != 0
                {
                    self.salariedView.isHidden = false
                    self.salariedViewHeightConstraint.constant = defaultsalariedViewheight
                }
                self.othersView.isHidden = true
                self.othersViewHeightConstraint.constant = 0
                self.businessView.isHidden = true
                self.bussinessViewConstraint.constant = 0
            case "Self Employed":
                if defaultbusinessViewheight != 0
                {
                    self.businessView.isHidden = false
                    self.bussinessViewConstraint.constant = defaultbusinessViewheight
                }
                self.othersView.isHidden = true
                self.othersViewHeightConstraint.constant = 0
                self.salariedView.isHidden = true
                self.salariedViewHeightConstraint.constant = 0
            case "Others":
                if defaultothersViewheight != 0
                {
                    self.othersView.isHidden = false
                    self.othersViewHeightConstraint.constant = defaultothersViewheight
                }
                self.businessView.isHidden = true
                self.bussinessViewConstraint.constant = 0
                self.salariedView.isHidden = true
                self.salariedViewHeightConstraint.constant = 0
            default:
                print("hello")
                //            self.othersView.isHidden = true
                //            self.defaultothersViewheight =   self.othersViewHeightConstraint.constant
                //            self.othersViewHeightConstraint.constant = 0
                //            self.businessView.isHidden = true
                //            self.defaultbusinessViewheight =   self.bussinessViewConstraint.constant
                //            self.bussinessViewConstraint.constant = 0
                
            }
        }
        else
            
        {
            bankAccountTxfld.text = pickerData[row]
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PROFESSIONAL/INCOME DETAILS"
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}




