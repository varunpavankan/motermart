//
//  EMICaluclatorVC.swift
//  MotorMart
//
//  Created by Nennu on 03/08/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class EMICaluclatorVC: UIViewController {

    
    @IBOutlet weak var intrestRateTxfld: ACFloatingTextfield!
    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var lowerButtonView: UIStackView!
    @IBOutlet weak var totalInterestLable: UILabel!
    
    @IBOutlet weak var emiMonthlyLable: UILabel!
    @IBOutlet weak var numberOfYearsTxfld: ACFloatingTextfield!
  
    @IBOutlet weak var principlalAmount: ACFloatingTextfield!
    override func viewDidLoad() {
        super.viewDidLoad()
        resultView.isHidden = true
        lowerButtonView.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapOnDetailedreportButton(_ sender: Any) {
    }
    
    
    @IBAction func didTapOnResetButton(_ sender: Any) {
        resultView.isHidden = true
           lowerButtonView.isHidden = true
        principlalAmount.text = nil
        intrestRateTxfld.text = nil
        numberOfYearsTxfld.text = nil
        
    }
    
    
    @IBAction func didTapOnCalculateButton(_ sender: Any) {
        if principlalAmount.text?.isEmpty == true
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Principlal Amount")
            
        }
        else if intrestRateTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Interest rate per Year")
            
        }
        else if numberOfYearsTxfld.text?.isEmpty == true
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Number of Years")
        }
        else
        {
            
            let P = Float("\(principlalAmount?.text! ?? "0")")!
            let r = Float("\(intrestRateTxfld?.text! ?? "0")")!
            let t = Float("\(numberOfYearsTxfld?.text! ?? "0")")!
            
           
            let emi = AlWrapper.emiCalculation_Reducing_balance_type(loanAmount: P, intrest:r , numberOfmonths: Int(t * 12))
            
             let A = emi * (t * 12)
            print(A)
            let monthlyEMI = emi
            print(monthlyEMI)
            let totalIntrestPerLoan =   A - P
            print(totalIntrestPerLoan)
            
            emiMonthlyLable.text = "\(emi)"
            totalInterestLable.text = "\(totalIntrestPerLoan)"
            resultView.isHidden = false
            lowerButtonView.isHidden = false
            
        }
        
    }
    
    @IBAction func didTapOnWantaLoanButton(_ sender: Any) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
