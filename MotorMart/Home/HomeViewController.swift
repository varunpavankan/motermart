//
//  HomeViewController.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
class HomeViewController: UIViewController,ENSideMenuDelegate{
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }
    
   
    @IBOutlet weak var towwheelerLable: UILabel!
    @IBOutlet weak var sellButton: UIButton!
    @IBOutlet weak var bikeHorizontalCOnstraint: NSLayoutConstraint!
    @IBOutlet weak var carButton: UIButton!
    @IBOutlet weak var bikeButton: UIButton!
    let appdelegate = UIApplication.shared.delegate as? AppDelegate
    var chooseArticleDropDown = DropDown()
    var mobileNumber = String()
    let addRemind_button = UIButton()
    let menu_button = UIButton()
    let iamgeArray = ["car2.png","biker.png","truck.png"]
    var parseDatadic = Dictionary<String,String>()
    var  bikeconstant = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
     //   self.setupSideMenu()
        self.sideMenuController()?.sideMenu?.delegate = self

        if (UserDefaults.standard.object(forKey: "_User_name") != nil)
        {
            
            print("data existing")
        }
        else
        {
        
        AlWrapper.requestGETURL("http://loan2buy.com/api/master/GET_USERDETAILS?MobileNo=\(mobileNumber)", viewController: self, success: { (json,responce) in
            print(json)
            if json == JSON.null
            {
                
            }
            else
            {
                let dic = AlWrapper.replaceNullValues(dictionary: json[0])
                print(dic)
         //       let userDefaults = UserDefaults.standard
                for (key,value) in dic
                {
                     UserDefaults.standard.set(value, forKey:key)
                    print( UserDefaults.standard.string(forKey: key) as Any)
                }

            }
            //  self.performSegue(withIdentifier: "towWheeler", sender:"2")
        }) { (error) in
            
        }
        }
//ic_menu_black_24dp
        
        let widthConstraint = addRemind_button.widthAnchor.constraint(equalToConstant: 30)
        let heightConstraint = addRemind_button.heightAnchor.constraint(equalToConstant: 30)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        addRemind_button.setImage(UIImage(named: "ic_more_vert_white_48pt.png"), for: .normal)
        addRemind_button.addTarget(self, action: #selector(didTapAddReminderButton), for: .touchUpInside)
        let add_Button = UIBarButtonItem(customView: addRemind_button)
        navigationItem.rightBarButtonItem = add_Button
        
        
        let widthConstraint1 = menu_button.widthAnchor.constraint(equalToConstant: 30)
        let heightConstraint1 = menu_button.heightAnchor.constraint(equalToConstant: 30)
        heightConstraint1.isActive = true
        widthConstraint1.isActive = true
        menu_button.setImage(UIImage(named: "menu_button.png"), for: .normal)
        menu_button.addTarget(self, action: #selector(didOnMenuButton), for: .touchUpInside)
        let menu_Button = UIBarButtonItem(customView: menu_button)
        menu_Button.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = menu_Button
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didtapOncacelmenu))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        self.bikeconstant = Int(self.bikeHorizontalCOnstraint.constant)
        self.bikeButton.layer.cornerRadius = bikeButton.frame.width/2
        self.carButton.layer.cornerRadius = carButton.frame.width/2
        
    //    self.sellButton.layer.cornerRadius = sellButton.frame.width/2
     
        self.bikeButton.layer.borderColor = UIColor.black.cgColor
        self.carButton.layer.borderColor = UIColor.black.cgColor
        
        self.bikeButton.layer.borderWidth = 1
        self.carButton.layer.borderWidth = 1
        
        self.bikeButton.clipsToBounds = true
        self.carButton.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("DashBoardIdentifier"), object: nil)
        bikeButton.transform = CGAffineTransform(scaleX: 0.10, y: 0.10)
        carButton.transform = CGAffineTransform(scaleX: 0.10, y: 0.10)
        
        
        
        UIView.animate(withDuration: 5.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.bikeButton.transform = .identity
            },
                       completion: nil)

        
        UIView.animate(withDuration: 5.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.carButton.transform = .identity
            },
                       completion: nil)
        
        self.setupChooseArticleDropDown()
        // Do any additional setup after loading the view.
    }
    @IBAction func didTapOnbikeButton(_ sender: Any) {
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
//            self.towwheelerLable.isHidden = true
//            self.bikeHorizontalCOnstraint.constant -= self.view.bounds.width
//            self.view.layoutIfNeeded()
//        }, completion: { _ in
//
//
//
//        })
        
        let srt = "1"
        self.appdelegate?.parseDatadic["Pcno"] = srt
        self.parseDatadic["Pcno"] = srt
        AlWrapper.requestGETURL("http://loan2buy.com/api/master/getmake?catid=\(srt)", viewController: self, success: { (json,responce)  in
            print(json)
            if json == JSON.null
            {
              
            }
            else
            {
                let dic = ["senderString":srt,"senderdic":json] as [String : Any]
                self.performSegue(withIdentifier: "towWheeler", sender:dic)
            }
           
        }) { (error) in
            
            print("error")
        }
        
        
        
    //  self.performSegue(withIdentifier: "towWheeler", sender: nil)
        
    }
    @IBAction func didTapONCarButton(_ sender: Any) {
        
        let srt = "2"
        self.appdelegate?.parseDatadic["Pcno"] = srt
        self.parseDatadic["Pcno"] = srt
        AlWrapper.requestGETURL("http://loan2buy.com/api/master/getmake?catid=\(srt)", viewController: self, success: { (json,responce) in
            print(json)
            if json == JSON.null
            {
                
            }
            else
            {
                let dic = ["senderString":srt,"senderdic":json] as [String : Any]
                self.performSegue(withIdentifier: "towWheeler", sender:dic)
            }
          //  self.performSegue(withIdentifier: "towWheeler", sender:"2")
        }) { (error) in
            
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
        navigationItem.title = nil
        
        if (segue.identifier == "towWheeler")
        {
            let dic = sender as! [String : Any]
            let str = dic["senderString"] as! String
            if str == "1"
            {
                let vc = segue.destination as! TwoWheelerDetailsVC
                vc.navigationItem.title = ""
                vc.vahiclesdic = dic["senderdic"] as! JSON
                vc.parseDatadic = (self.appdelegate?.parseDatadic)!
                // vc.navigationController?.navigationBar.backItem?.title = ""
                vc.navigationItem.title = "Two Wheeler Details"
                
            }else
            {
                let vc = segue.destination as! TwoWheelerDetailsVC
                vc.navigationItem.title = ""
                vc.vahiclesdic = dic["senderdic"] as! JSON
                vc.parseDatadic = (self.appdelegate?.parseDatadic)!                // vc.navigationController?.navigationBar.backItem?.title = ""
                vc.navigationItem.title = "Three Wheeler Details"
            }
            
        }
        else
        {
            
        }
      
//        if segue.identifier == "twowheeler"{
//            let vc = segue.destination as UIViewController
//            vc.navigationItem.title = ""
//           // vc.navigationController?.navigationBar.backItem?.title = ""
//            vc.navigationItem.title = "Two Wheeler Details"
//          //   vc.navigationItem.backBarButtonItem?.title = ""
//        }
    }
    
    
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
         self.hideSideMenuView ()
        print("yes its coming: \(String(describing: notification.userInfo?["sucess"]))")
        var selected_nm = notification.userInfo?["sucess"] as? String
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        if selected_nm == "Home"
        {
             self.hideSideMenuView ()
        }
           else if selected_nm == "WHAT WE DO"
        {
          //  WhatweDoVC
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "WhatweDoVC") as! WhatweDoVC
            mainViewController.title = "WHAT WE DO"
           // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else if selected_nm == "TERRMS OF SERVICE"
        {
            //  WhatweDoVC
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "WhatweDoVC") as! WhatweDoVC
            mainViewController.title = "Terms of Service"
            // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else if selected_nm == "PRIVACY POLICY"
        {
            //  WhatweDoVC
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "WhatweDoVC") as! WhatweDoVC
            mainViewController.title = "Privacy Policy"
            // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else if selected_nm == "REPORT ABUSE POLICY"
        {
            //  WhatweDoVC
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "WhatweDoVC") as! WhatweDoVC
            mainViewController.title = "Report Abuse Policy"
            // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else if selected_nm == "CUSTOMER FEEDBACK"
        {
            //  WhatweDoVC
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "CustomerfeedbackVC") as! CustomerfeedbackVC
            mainViewController.title = "Customer Feedback"
            // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else if selected_nm == "FINANCIAL CALUCLATORS"
        {
            //  WhatweDoVC
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "EMICaluclatorVC") as! EMICaluclatorVC
            mainViewController.title = "Caluculate Your EMI"
            // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else if selected_nm == "TRACK APPLICATION"
        {
            //  WhatweDoVC
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "TrackApplicationVC") as! TrackApplicationVC
            mainViewController.title = "Track Applivation"
            // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else if selected_nm == "Share"
        {
           
            let text = "This is some text that I want to share."
            
            // set up activity view controller
            let textToShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
        else if selected_nm == "Send"
        {
            
            let text = "This is some text that I want to share."
            
            // set up activity view controller
            let textToShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
        else if selected_nm == "UPDATE PROFILE"
        {
            
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateKYC") as! UpdateKYC
            mainViewController.title = "KYC"
            // UIApplication.shared.delegate?.window??.rootViewController = nvc
            self.navigationController?.pushViewController(mainViewController, animated: false)
        }
            
           // FINANCIAL CALUCLATORS
           // TrackApplicationVC
        else if selected_nm == "Logout" {
          // self.navigationController?.popViewController(animated: true)
            self.hideSideMenuView ()
            
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nvc: UINavigationController = UINavigationController.init(rootViewController: mainViewController)
             UIApplication.shared.delegate?.window??.rootViewController = nvc
        //    self.navigationController?.pushViewController(mainViewController, animated: false)
        }
        else
        {
           
        }
        //   let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileViewController
        //  let navcontroller = UINavigationController(rootViewController: nextViewController)
        //     self.navigationController?.pushViewController(nextViewController, animated: true)
        //   self.performSegue(withIdentifier: "profileVC", sender: nil)
        //        let selected_nm = notification.userInfo?["sucess"] as? String
        //
        //        if selected_nm == "My Account" {
        //
        //            self.performSegue(withIdentifier: "profileVC", sender: nil)
        //
        //        }else if selected_nm == "Settings" {
        //
        //            self.performSegue(withIdentifier: "settingsVC", sender: nil)
        //
        //        }else if selected_nm == "Help" {
        //
        //            self.performSegue(withIdentifier: "helpVc", sender: nil)
        //        }
        //        else if selected_nm == "Share Profile"{
        //
        //            self.performSegue(withIdentifier: "shareProfileVc", sender: nil)
        //        }
        //        else if selected_nm == "Contact Us" {
        //
        //            self.performSegue(withIdentifier: "contactVc", sender: nil)
        //
        //        }else if selected_nm == "Terms and Conditions" {
        //
        //            self.performSegue(withIdentifier: "termsCondVC", sender: nil)
        //
        //        }else if selected_nm == "Privacy Policy" {
        //
        //            self.performSegue(withIdentifier: "PrivacyPolicyVc", sender: nil)
        //
        //        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
     //   super.viewWillAppear(animated)
        self.bikeHorizontalCOnstraint.constant = CGFloat(bikeconstant)
        self.towwheelerLable.isHidden = false
        self.title = "Motormart.in"
        self.navigationController?.navigationBar.titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedStringKey : Any]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    override func viewDidDisappear(_ animated: Bool) {
      //  super.viewDidDisappear(true)
        self.title = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       //  self.title = ""
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didtapOncacelmenu()
    {
        self.hideSideMenuView ()
    }
    @objc func didOnMenuButton()
    {
        toggleSideMenuView()

    }
    
 /*Drop Down functions starts from here*/
    @objc func didTapAddReminderButton()
    {
        chooseArticleDropDown.show()
        
    }
    
    func setupChooseArticleDropDown() {
        chooseArticleDropDown.anchorView = addRemind_button
        
        // Will set a custom with instead of anchor view width
        //        dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: addRemind_button.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseArticleDropDown.dataSource = [
            "Customer Feedback",
            "Exit MotorMart"
        ]
        
        // Action triggered on selection
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            self?.addRemind_button.setTitle(item, for: .normal)
            if (item == "Customer Feedback")
            {
               print("my accout is taped")
            }
            else{
                print("logout is tapped")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self?.navigationController?.pushViewController(mainViewController, animated: false)
                self?.resetDefaults()
            //    UserDefaults.standard.set(false, forKey: "Login")
                
               
            }
            self?.chooseArticleDropDown.hide()
        }
        
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                self?.addRemind_button.setTitle("", for: .normal)
            }
        }
    }
  
}


//extension HomeViewController: UISideMenuNavigationControllerDelegate {
//    
//    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
//        print("SideMenu Appearing! (animated: \(animated))")
//    }
//    
//    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
//        print("SideMenu Appeared! (animated: \(animated))")
//    }
//    
//    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
//        print("SideMenu Disappearing! (animated: \(animated))")
//    }
//    
//    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
//        print("SideMenu Disappeared! (animated: \(animated))")
//    }
//    
//}
