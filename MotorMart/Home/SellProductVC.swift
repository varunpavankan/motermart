//
//  SellProductVC.swift
//  MotorMart
//
//  Created by Nennu on 07/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
class SellProductVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    
     let picker = UIImagePickerController()
  
    
    
    @IBOutlet weak var warrantryDiscriptionTxfld: UITextField!
    
    @IBOutlet weak var insuranceValidtilldateTxfld: UITextField!
    
    @IBOutlet weak var colortxfld: UITextField!
    
    
    @IBOutlet weak var InsuranceTxfld: UITextField!
    
    @IBOutlet weak var rcupload: UITextField!
    
    @IBOutlet weak var vechiclepic4: UITextField!
    @IBOutlet weak var vechiclepic3: UITextField!
    @IBOutlet weak var vechiclepic2: UITextField!
    @IBOutlet weak var vehiclepic1: UITextField!
    
    @IBOutlet weak var selectCityTxfld: UITextField!
    @IBOutlet weak var selectVechicalTxfld: UITextField!
    @IBOutlet weak var selectModelTxfld: UITextField!
    @IBOutlet weak var selectMakeTxfld: UITextField!
    @IBOutlet weak var selectVechicaltypeTxfld: UITextField!
    @IBOutlet weak var KMPLTxfld: UITextField!
    @IBOutlet weak var cityavailableDropDownButton: UIButton!
    
    @IBOutlet weak var ReasonforSellingTxfld: UITextField!
    
    @IBOutlet weak var TaxPaidTxfld: UITextField!
    
    @IBOutlet weak var InsuranceTypeTxfld: UITextField!
    
    @IBOutlet weak var PlaceofRegistrationTxfld: UITextField!
    
    @IBOutlet weak var OwnerNameTxfld: UITextField!
    
    @IBOutlet weak var ExpectedPriceTxfld: UITextField!
    @IBOutlet weak var Kmsruntxfld: UITextField!
    
    
    @IBOutlet weak var YearofManufactureTxfld: UITextField!
    
    @IBOutlet weak var vehicleConditiondropdownButton: UIButton!
    
    @IBOutlet weak var ccTexfld: UITextField!
    @IBOutlet weak var vehicleNumber: UITextField!
    @IBOutlet weak var vehicleConditionLable: UITextField!
    
    @IBOutlet weak var warrantydescriptionTxfld: UITextField!
    @IBOutlet weak var warrantyTxfld: UITextField!
    
    @IBOutlet weak var manufacturerNameTxfld: UITextField!
    @IBOutlet weak var productLongdescriptionTxfld: UITextField!
    
    @IBOutlet weak var productShortdecpTxfld: UITextField!
    
    
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var selectcategoryLable: UILabel!
    
    @IBOutlet weak var enterAddress: UITextField!
    @IBOutlet weak var enterEmailIDTxtField: UITextField!
    @IBOutlet weak var enterNameTxfild: UITextField!
    @IBOutlet weak var enterMobileNumberTxtfild: UITextField!
    @IBOutlet weak var hiddenView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
     var pickerData = [String]()
    var textfield = UITextField()
    var UploadDocuments = [String:Any]()
    var productID = String()
    var prodCaltd = String()
    var prodType = String()
    var makeId = String()
    var modelNo = String()
    var isactive = String()
    var cityId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenView.isHidden = true
       scrollView.isScrollEnabled = false
        picker.delegate = self
        AlWrapper.setupTextfieldLeftView(imageString: "if_Mail_613533-2.png",textfield:enterEmailIDTxtField, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftView(imageString: "if_Mail_613533-2.png",textfield:enterAddress, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftView(imageString: "if_Mail_613533-2.png",textfield:enterNameTxfild, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldLeftView(imageString: "if_Mail_613533-2.png",textfield:enterMobileNumberTxtfild, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectVechicaltypeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectCityTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectModelTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectMakeTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
 AlWrapper.setupTextfieldrightViewwithborder(imageString: "dropDownView.png",textfield:selectVechicalTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "Camera.png",textfield:vehiclepic1, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "Camera.png",textfield:vechiclepic2, backgroundcolor: UIColor.blue,backgroundbool: false)
        AlWrapper.setupTextfieldrightViewwithborder(imageString: "Camera.png",textfield:vechiclepic3, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "Camera.png",textfield:vechiclepic4, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "Camera.png",textfield:rcupload, backgroundcolor: UIColor.blue,backgroundbool: false)
         AlWrapper.setupTextfieldrightViewwithborder(imageString: "Camera.png",textfield:InsuranceTxfld, backgroundcolor: UIColor.blue,backgroundbool: false)
        selectModelTxfld.isEnabled = false
        self.title = "Easy way to sell your Vcehicle"
        
        enterNameTxfild.text = UserDefaults.standard.string(forKey: "_User_name")
        enterMobileNumberTxtfild.text = UserDefaults.standard.string(forKey: "_Mobile_No")
        enterEmailIDTxtField.text = UserDefaults.standard.string(forKey: "_EmailId")
        getcity()
        getmakeId()
//        let imageView = UIImageView()
//        let image = UIImage(named: "if_Mail_613533-2.png")
//        imageView.image = image
//        imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        enterEmailIDTxtField.leftView = imageView
//        enterEmailIDTxtField.leftViewMode = .always
        //enterEmailIDTxtField.setLeftImage(imageString:"if_Mail_613533-2.png")
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var cityAvaiableLable: UILabel!
    @IBAction func didtapOncityavailableDropDownButton(_ sender: Any) {
    }
    @IBAction func didtapOnvehicleConditiondropdownButton(_ sender: Any) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var city = JSON()
    func getcity()
    {
        AlWrapper.requestGETURL("http://loan2buy.com/api/master/getcity", viewController: self, success: { (json,responce) in
       
           self.city = json
            
        }, failure: { (error) in
            
        })
    }
    var makeName = JSON()
    var modelName = JSON()
    func getmakeId()
    {
        AlWrapper.requestGETURL("http://loan2buy.com/api/master/getmake?catid=\(1)", viewController: self, success: { (json,responce)  in
            print(json)
            if json == JSON.null
            {
                
            }
            else
            {
                self.makeName = json
            }
            
        }) { (error) in
            
            print("error")
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      
        if self.textfield == selectVechicaltypeTxfld
        {
             selectVechicaltypeTxfld.text = pickerData[row]
            if selectVechicaltypeTxfld.text == "Two wheeler"
            {
                selectVechicaltypeTxfld.resignFirstResponder()
                hiddenView.isHidden = false
                scrollView.isScrollEnabled = true
            }
            else
            {
                  AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Service is not avilable")
            }
           
            
        }
        else  if self.textfield == selectModelTxfld
        {
//            var prodCaltd = String()
//            var prodType = String()
//            var makeId = String()
//            var modelNo = String()
//            var isactive = String()
            selectModelTxfld.text = pickerData[row]
            
           modelNo = "\(modelName[row]["modelId"].int!)"
            isactive = modelName[row]["isActive"].string!
            prodType = "2"
            
            
        }
        else  if self.textfield == selectCityTxfld
        {
            selectCityTxfld.text = pickerData[row]
            self.cityId = "\(city[row]["cityId"].int!)"
        }
        else  if self.textfield == selectMakeTxfld
        {
            selectMakeTxfld.text = pickerData[row]
             makeId = "\(makeName[row]["makeId"].int!)"
            prodCaltd = "\(makeName[row]["catId"].int!)"
            AlWrapper.requestGETURL("http://loan2buy.com/api/master/getmodel?makeid=\(String(describing: makeName.array![row]["makeId"]))", viewController: self, success: { (json,responce) in
                self.modelName  = json
                print(json)
                self.selectModelTxfld.text = "Select Model"
                self.selectModelTxfld.isEnabled = true
            }, failure: { (error) in
                
            })
        }
        else  if self.textfield == selectVechicalTxfld
        {
            selectVechicalTxfld.text = pickerData[row]
            
        }

    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        switch textField {
        case selectVechicaltypeTxfld:
            self.textfield = selectVechicaltypeTxfld
            pickerData.removeAll()
            pickerData = ["Two wheeler","Cars","Tractors","Passenger Autos","Trucks","Vans and Mini Vans"]
            selectVechicaltypeTxfld.inputView = picker
            self.selectVechicaltypeTxfld.tintColor = .clear
            return true
        case selectCityTxfld:
            pickerData.removeAll()
            self.textfield = selectCityTxfld
            pickerData = getcityName(dic1: city)
            selectCityTxfld.inputView = picker
            self.selectCityTxfld.tintColor = .clear
            return true
        case selectMakeTxfld:
            pickerData.removeAll()
            self.textfield = selectMakeTxfld
            pickerData = getmakename(dic1: makeName)
            selectMakeTxfld.inputView = picker
            self.selectMakeTxfld.tintColor = .clear
            return true
        case selectModelTxfld:
            pickerData.removeAll()
             self.textfield = selectModelTxfld
            pickerData = getmodelName(dic1: modelName)
            selectModelTxfld.inputView = picker
            self.selectModelTxfld.tintColor = .clear
            return true
        case selectVechicalTxfld:
            pickerData.removeAll()
             self.textfield = selectVechicalTxfld
            pickerData = ["Good","Fair","Excellent"]
            selectVechicalTxfld.inputView = picker
            self.selectVechicalTxfld.tintColor = .clear
            return true
        case vehiclepic1:
             self.textfield = vehiclepic1
            self.didselecttheuploadImage(vehiclepic1)
            self.vehiclepic1.tintColor = .clear
            return false
        case vechiclepic2:
            self.textfield = vechiclepic2
            self.didselecttheuploadImage(vechiclepic2)
            self.vechiclepic2.tintColor = .clear
            return false
        case vechiclepic3:
            self.textfield = vechiclepic3
            self.didselecttheuploadImage(vechiclepic3)
            self.vechiclepic3.tintColor = .clear
            return false
        case vechiclepic4:
            self.textfield = vechiclepic4
            self.didselecttheuploadImage(vechiclepic4)
            self.vechiclepic4.tintColor = .clear
            return false
        case rcupload:
            self.textfield = rcupload
            self.didselecttheuploadImage(rcupload)
            self.rcupload.tintColor = .clear
            return false
        case InsuranceTxfld:
            self.textfield = InsuranceTxfld
            self.didselecttheuploadImage(InsuranceTxfld)
            self.InsuranceTxfld.tintColor = .clear
            return false
        default:
            return true
        }
      //  return true
    }
    func getcityName (dic1:JSON) -> [String]
    {
        var dic = [String]()
        for val in dic1.array!{
            let str = val["CityName"].string
            dic.append(str!)
            
        }
        
        return dic
    }
    
func getmakename (dic1:JSON) -> [String]
 {
    var dic = [String]()
    for val in dic1.array!{
         let str = val["makeName"].string
        dic.append(str!)
        
    }
    
    return dic
}
    func getmodelName (dic1:JSON) -> [String]
    {
        var dic = [String]()
        for val in dic1.array!{
            let str = val["modelName"].string
            dic.append(str!)
            
        }
        
        return dic
    }
    
    @IBAction func textfieldendediting(_ sender: UITextField) {
        
    }
    @IBAction func didtapOnScrollButton(_ sender: Any) {
        //  self.view.addSubview(hiddenView)
       
     //   selectcategoryLable.inputView = picker
     //    pickerData = ["Male","Female","Others"]
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
      @IBOutlet weak var chooseFileLable: UILabel!
    
    func didselecttheuploadImage(_ sender: UITextField)
    {
        let alert = UIAlertController(
            title: "Select",
            message:"",
            preferredStyle: .actionSheet)
        let Gallery = UIAlertAction(
            title: "Gallery",
            style: .default){ action -> Void in
                //           self.multipleImagePicker(sender:sender)
                self.picker.allowsEditing = false
                self.picker.sourceType = .photoLibrary
                self.present(self.picker, animated: true, completion: nil)
                //self.picker.cameraCaptureMode = .photo
        }
        let Camera = UIAlertAction(
            title: "Camera",
            style: .default){ action -> Void in
                
                //    self.emailTxfld.becomeFirstResponder()
                if !(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
                    
                    let camalert = UIAlertController(
                        title: "Select",
                        message:"",
                        preferredStyle: .alert)
                    let okay = UIAlertAction(
                        title: "Gallery",
                        style: .default){ action -> Void in
                            
                    }
                    camalert.addAction(okay)
                    self.present(camalert, animated: true, completion: nil)
                }
                else{
                    self.picker.allowsEditing = false
                    self.picker.sourceType = .camera
                    self.picker.cameraCaptureMode = .photo
                    //other action
                    self.present(self.picker, animated: true, completion: nil)
                }
        }
        let Cancle = UIAlertAction(
            title: "Cancel",
            style: .default){ action -> Void in
                
                //other action
        }
        alert.addAction(Gallery)
        alert.addAction(Camera)
        alert.addAction(Cancle)
        self.present(alert, animated: true, completion: nil)
    }
   
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let assetPath = info[UIImagePickerControllerReferenceURL] as! URL
        let pathextention = assetPath.pathExtension
        var Size = Float()
        var data = Data()
        let data1  = UIImageJPEGRepresentation(chosenImage, 0.5)!
        let strBase64 = data1.base64EncodedString(options: .lineLength64Characters)
        data = UIImageJPEGRepresentation(chosenImage, 0.5)!
        print(data1)
        //Here you get MB size
        Size = Float(Double(data.count)/1024/1024)
//
//        var prodCaltd = String()
//        var prodType = String()
//        var makeId = String()
//        var modelNo = String()
//        var isactive = String()
        
        if Size <= 1.00
        {
            if textfield == vehiclepic1
            {
                UploadDocuments["VehPic1"] = strBase64
                vehiclepic1.text = "Image selected"
              //  IncomeTaxReturnExtension = pathextention
            }
            else if textfield == vechiclepic2
            {
                UploadDocuments["VehPic2"] = strBase64
                 vechiclepic2.text = "Image selected"
            }
            else if textfield == vechiclepic3
            {
                UploadDocuments["VehPic3"] = strBase64
                vechiclepic3.text = "Image selected"
            }
            else if textfield == vechiclepic4
            {
                UploadDocuments["VehPic4"] = strBase64
                vechiclepic4.text = "Image selected"
            }
            else if textfield == rcupload
            {
                 UploadDocuments["rcdoc"] = strBase64
                rcupload.text = "Image selected"
            }
            else if textfield == InsuranceTxfld
            {
                UploadDocuments["InsDoc"] = strBase64
                 InsuranceTxfld.text = "Image selected"
            }
            
            
        }
        else
        {
           AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please select Image less Than 1MB ")
        }
        
        dismiss(animated: true, completion: nil)
        //other action
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
   
    @IBAction func didTapOnSellButton(_ sender: Any) {
        
        if enterEmailIDTxtField.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the Email Id")
        }
        else if enterMobileNumberTxtfild.text?.isEmpty == true
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter Mobile Number")
        }
        else if enterNameTxfild.text?.isEmpty == true
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter your Name")
        }
        else if enterAddress.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter your Address")
        }

        else if enterAddress.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter your Address")
        }
        else if selectVechicaltypeTxfld.text == "Select Category"
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Product Type")
        }
        else if selectMakeTxfld.text == "Select Make"
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Make Type")
        }
        
        else if selectModelTxfld.text == "Select Model"
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the model Type")
        }
        else if productShortdecpTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Short discription")
        }
        else if warrantyTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Warranty Description")
        }
        
        else if warrantydescriptionTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Warranty Description")
        }
        
        else if vehicleNumber.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter vehicleNumber")
        }
        else if ccTexfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter CC Details")
        }
        else if YearofManufactureTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Entet year of Manufacture details")
        }
        
        else if Kmsruntxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Entet year KMS")
        }
        
        else
        {
            
            let User_Code = UserDefaults.standard.string(forKey: "_User_Id")
            let prodid = ""
            let prodCatid = self.prodCaltd
            let prodType = self.prodType
            let makeId = self.makeId
            let UserName = self.enterNameTxfild.text
            let modelNo = self.modelNo
            let isActive = self.isactive
            let validForm = ""
            let ValidTo = ""
            let ProdShortDesc = self.productShortdecpTxfld.text!
            let prodLongDes = self.productLongdescriptionTxfld.text!
            let manufacture_name = self.selectModelTxfld.text!
            let manufacture_namemanufacture_name = self.warrantyTxfld.text!
            let warranty_details = self.warrantydescriptionTxfld.text!
            let cityAvailable = self.selectCityTxfld.text!
            let MobileNo = self.enterMobileNumberTxtfild.text!
            let EmailId = self.enterEmailIDTxtField.text!
            let Original_BasePrice = self.ExpectedPriceTxfld.text!
            let discount_type = "0"
            let discount_amount = "0"
            let ExshowrumPrice = "0"
            let Temp_passing = "0"
            let RoadtaxType = "False"
            let road_tax = "0"
            let RoadTaxAmount = "0"
            let RegsAmount = "0"
            let HypotCharegs = "0"
            let InsurnceAmt = "0"
            let misccharges = "0"
            let is_accessories = "False"
            let is_fitting = "False"
            let Vehpics = ""
            let VehNumber = self.vehicleNumber.text ?? ""
            let cc1 = self.ccTexfld.text ?? ""
            let ConditionofVeh = self.selectVechicalTxfld.text ?? ""
            let YMfg = self.YearofManufactureTxfld.text ?? ""
            let KumRun = self.Kmsruntxfld.text ?? ""
            let ePrice = self.ExpectedPriceTxfld.text ?? ""
            let ownername = self.OwnerNameTxfld.text ?? ""
            let rcdoc =  UploadDocuments["rcdoc"] ?? "null"
            let PlaceofReg = self.PlaceofRegistrationTxfld.text ?? ""
            let InsType = self.InsuranceTxfld.text ?? ""
            let InsDoc = UploadDocuments["InsDoc"] ?? "null"
            let insuranceValidity = insuranceValidtilldateTxfld.text ?? ""
            let TaxPaid = TaxPaidTxfld.text ?? ""
            let Reason = self.ReasonforSellingTxfld.text ?? ""
            let kmPl = self.KMPLTxfld.text ?? ""
            let ColorDesc = self.colortxfld.text ?? ""
            let VehPic1 = UploadDocuments["VehPic1"] ?? ""
             let VehPic2 = UploadDocuments["VehPic2"] ?? ""
            let VehPic3 = UploadDocuments["VehPic3"] ?? ""
             let VehPic4 = UploadDocuments["VehPic4"] ?? ""
            let Fuel_type = "petrol"
            
            
            let parsedata = ["User_Code":User_Code!,"prodid":prodid,"prodCatid":prodCatid,"prodType":prodType, "makeId":makeId,"UserName":UserName!,"modelNo":modelNo,"isActive":isActive,"validForm":validForm,"ValidTo":ValidTo,"ProdShortDesc":ProdShortDesc,"prodLongDes":prodLongDes,"manufacture_name":manufacture_name,"warranty_details":warranty_details,"cityAvailable":cityAvailable,"MobileNo":MobileNo,"EmailId":EmailId,"Original_BasePrice":Original_BasePrice,"discount_type":discount_type,"discount_amount":discount_amount,"ExshowrumPrice":ExshowrumPrice,"Temp_passing":Temp_passing,"RoadtaxType":RoadtaxType,"road_tax":road_tax,"RoadTaxAmount":RoadTaxAmount,"RegsAmount":RegsAmount,"HypotCharegs":HypotCharegs,"InsurnceAmt":InsurnceAmt,"misccharges":misccharges,"is_accessories":is_accessories,"is_fitting":is_fitting,"Vehpics":Vehpics,"VehNumber":VehNumber ,"cc1":cc1 ,"ConditionofVeh":ConditionofVeh,"YMfg":YMfg ,"KumRun":KumRun ,"ePrice":ePrice,"ownername":ownername ,"rcdoc":rcdoc,"PlaceofReg":PlaceofReg,"InsType":InsType ,"InsDoc":InsDoc,"insuranceValidity":insuranceValidity,"TaxPaid":TaxPaid ,"Reason":Reason,"kmPl":kmPl,"BookingCharges":"0","Fuel_type":Fuel_type,"ColorDesc":ColorDesc ,"VehPic1":VehPic1,"VehPic2":VehPic2,"VehPic3":VehPic3,"VehPic4":VehPic4]as [String : Any]
            print(parsedata)
            
            AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"set_IndividualSeller", viewController: self, params: parsedata as [String : AnyObject], headers: nil, success: { (json, responce) in
                print(json)
                if json == 400
                {
                    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the valid mobile number and password")
                }
                else
                {
                    if json["Status"] == true
                    {
                        
                        let alert = UIAlertController(title: "Sucess", message: "Your request is taken secussfully", preferredStyle: UIAlertControllerStyle.alert);
                        let OK = UIAlertAction(
                            title: "Thankyou",
                            style: .default){ action -> Void in
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: HomeViewController.self) {
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                                //other action
                        }
                        alert.addAction(OK)
                        
                        
                        // relate actions to controllers
                        
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }) { (erro) in
                
                print(erro)
            }
            
        }

    }
    /*

     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
