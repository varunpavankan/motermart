//
//  MenuVC.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class MenuVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
     var isDarkModeEnabled = false
    private var themeColor = UIColor.white
    @IBOutlet weak var powerdbyLable: UILabel!
    let userDefaults = Foundation.UserDefaults.standard
    
    @IBOutlet weak var slideMenuTableView_obj: UITableView!
    let imageArray = ["menu_home.png","menu_financial.png","menu_financial.png","menu_company.png","menu_company.png","menu_company.png","menu_company.png","menu_contact.png","menu_company.png","menu_free_cridetscoreboard.png","menu_how_it_works.png","menu_financial.png"]
    let slideMenuNameArr = ["Home","WHAT WE DO","FINANCIAL CALUCLATORS","TERRMS OF SERVICE","PRIVACY POLICY","REPORT ABUSE POLICY","UPDATE PROFILE","TRACK APPLICATION","CUSTOMER FEEDBACK","Share","Send","Logout"]
    
    //  let imageArr = ["Userr.png","settings.png","f-help.png","f-phone.png","f-note.png","f-privacy-500px.png","f-logout.png"]
    
    // var slideMenuImageArr = [String]()
    
    let cellReuseIdentifier = "cell"
    
    @IBOutlet weak var tableHeaderView: UIView!
    
    @IBOutlet weak var selectionMenuTrailingConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var slideMenuImage_view: UIImageView!
    
    @IBOutlet weak var slideMenuName_lbl: UILabel!
    
    // @IBOutlet weak var slideMenuNumber_lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slideMenuImage_view.layer.cornerRadius = 10
        
        self.slideMenuTableView_obj.tableFooterView = UIView.init()
          slideMenuTableView_obj.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0)
        slideMenuName_lbl.text = userDefaults.string(forKey: "_User_name")
        // use itttttt...
        /*
         let userDefaults = Foundation.UserDefaults.standard
         let userImgUrl = userDefaults.string(forKey: "imgUrlStr")!
         
         let frstNameStr = userDefaults.string(forKey: "userFirstName")!
         let lstNameStr = userDefaults.string(forKey: "lstNameStr")!
         self.slideMenuName_lbl.text = "\(frstNameStr) \(lstNameStr)"
         
         self.slideMenuNumber_lbl.text = userDefaults.string(forKey: "userMobileStr") */
        
        
        
        //  powerdbyLable.font = UIFont(name:"Lato-Semibold", size: 12)
        
        
        //        DispatchQueue.global().async {
        //
        //            self.slideMenuImage_view.sd_setImage(with: URL(string: userImgUrl ), placeholderImage: UIImage(named: "stamp.png"))
        
        
        //            let url = NSURL(string: userImgUrl)
        //            let data = try? Data(contentsOf: url! as URL)
        //
        //            if data != nil
        //            {
        //                DispatchQueue.main.async {
        //                    self.slideMenuImage_view.image = UIImage(data: data!)
        //                }
        //            }
        //            else{
        //
        //            }
        
        // }
    }
//    private func configureView() {
//        if isDarkModeEnabled {
//            themeColor = UIColor(red:0.03, green:0.04, blue:0.07, alpha:1.00)
//          // selectionTableViewHeader.textColor = .white
//        } else {
//            selectionMenuTrailingConstraint.constant = 0
//            themeColor = UIColor(red:0.98, green:0.97, blue:0.96, alpha:1.00)
//        }
//
//        let showPlaceTableOnLeft = (SideMenuController.preferences.basic.position == .under) != (SideMenuController.preferences.basic.direction == .right)
//        if showPlaceTableOnLeft {
//            selectionMenuTrailingConstraint.constant = SideMenuController.preferences.basic.menuWidth - view.frame.width
//        }
//
//        view.backgroundColor = themeColor
//       // tableView.backgroundColor = themeColor
//    }
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//
//        let showPlaceTableOnLeft = (SideMenuController.preferences.basic.position == .under) != (SideMenuController.preferences.basic.direction == .right)
//        selectionMenuTrailingConstraint.constant = showPlaceTableOnLeft ? SideMenuController.preferences.basic.menuWidth - size.width : 0
//        view.layoutIfNeeded()
//    }
    
    @IBAction func didTapOnHeaderButton(_ sender: Any) {
        let sampleDict:[String: String] = ["sucess": "ProfileVC" ]
        NotificationCenter.default.post(name: Notification.Name("DashBoardIdentifier"), object: nil, userInfo: sampleDict)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        /*
         let frstNameStr = userDefaults.string(forKey: "userFirstName")!
         let lstNameStr = userDefaults.string(forKey: "lstNameStr")!
         
         self.slideMenuName_lbl.text = "\(frstNameStr) \(lstNameStr)"
         
         let userImgUrl = userDefaults.string(forKey: "imgUrlStr")!
         
         slideMenuImage_view.layer.cornerRadius = slideMenuImage_view.frame.height/2
         slideMenuImage_view.clipsToBounds = true
         
         DispatchQueue.global().async {
         
         self.slideMenuImage_view.sd_setImage(with: URL(string: userImgUrl ), placeholderImage: UIImage(named: "stamp.png"))
         
         } */
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return slideMenuNameArr.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return slideMenuNameArr.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SlideMenuCell = self.slideMenuTableView_obj.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier) as! SlideMenuCell
        
        cell.selectionStyle = .blue
        
        cell.name_lbl.text = self.slideMenuNameArr[indexPath.row]
        
        //cell.customImg_view.image = self.imageArray[indexPath.row] as UIImage
        
        cell.customImg_view?.image = UIImage(named: self.imageArray[indexPath.row])
        
        cell.selectionStyle = .none
        
        // cell.name_lbl.text = self.slideMenuNameArr[indexPath.row]
        
        //  cell.name_lbl.text = self.slideMenuImageArr[indexPath.row]
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let sampleDict:[String: String] = ["sucess": slideMenuNameArr[indexPath.row] ]
        NotificationCenter.default.post(name: Notification.Name("DashBoardIdentifier"), object: nil, userInfo: sampleDict)
        
    //    self.slideMenuController()?.closeLeft()
    }
}


















