//
//  ProductListVC.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
class ProductListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var productList = JSON()
    
    @IBOutlet weak var headerLable: UILabel!
    @IBOutlet weak var displyerrorLable: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    var parseDatadic = Dictionary<String, String>()
    var imageArray = ["bikesample.jpg","bikesample.jpg","bikesample.jpg","bikesample.jpg"]
    override func viewDidLoad() {
        super.viewDidLoad()
    //   self.navigationController?.navigationBar.backItem?.title = ""
        self.displyerrorLable.text = "we are very sorry that we do not have the product that you are looking for at this time.\nwe are working digently with sellers to include the products Please check back in few weeks."
        innerView.layer.borderWidth = 0.5
        innerView.layer.borderColor = UIColor.white.cgColor
        innerView.clipsToBounds = true
        innerView.layer.masksToBounds = true
    //    innerView.layer.shadowOffset = CGSizeMake(-15, 20);
        innerView.layer.shadowRadius = 5;
        innerView.layer.shadowOpacity = 0.5
 self.navigationItem.title = "Product List"
        tableView.autoresizingMask = .flexibleHeight
        tableView.autoresizesSubviews = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        headerLable.text = "Hi \(UserDefaults.standard.string(forKey: "_User_name")!),We are Displaying HONDA ACTIVA 4G"
        if productList.count == 0
        {
            tableView.isHidden = true
        }
        else
        {
            noDataView.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
     //   self.navigationController?.navigationBar.backItem?.title = ""
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.productList = JSON.null
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      //  self.navigationController?.navigationBar.backItem?.title = ""
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return (productList.array?.count)!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ProductionListTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductionListTableViewCell
        
        let dic = productList[indexPath.section].dictionary
        print(dic as Any)
    //    cell.contenImageView.image = UIImage(named:imageArray[indexPath.section])
        let url = URL(string: (dic!["imaGe"]?.string)!)
        cell.providerLable.text = dic!["organizationName"]?.string
        cell.bikeNameLable.text = (dic!["makeName"]?.string)! + (dic!["modelName"]?.string)!
        let str = dic!["grossAmount"]?.int
        cell.priceLable.text = "₹" + "\(str ?? 100000)"
        cell.mileageLable.text = "Mileage" + " " + (dic!["kmPl"]?.string)! + " KMPL"
        cell.contenImageView.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if productList == JSON.null
        {
            productList = (appdelegate?.productList)!
        }
        let dic = productList[indexPath.section].dictionary
        print(productList)
        let modelid = parseDatadic["modelId"]
        let userId = UserDefaults.standard.string(forKey: "_User_Id")
        let sellerId = dic!["sellerId"]?.string
        let productID = dic!["prodId"]?.string
        
        print(parseDatadic)
        // ["Pro_Used_New": "1", "modelId": "1", "cityId": "14", "makeID": "1", "Pmttype": "1"]
        let dataString = "SellerId=\(sellerId!)&ModelNo=\(modelid!)&UserId=\(userId!)&PId=\(productID!)"
        
        print(dataString)
       // http://loan2buy.com/api/master/ GET_PRODUCT_DETAILSbyPId?SelleID=&ModelId=&UserId=9&PId=1
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_PRODUCT_DETAILSbyPId?"+dataString, viewController: self, success: { (json,responce) in
            print(json)
            
        //    ModelNo=1&SellerId=VID20181372&UserId=1829&PID=VID2018137220180312011227445
          //  self.performSegue(withIdentifier: "ProductSummeryVC", sender: json)
            let accessoriesData = "SellerId=\(sellerId!)&ModelNo=\(modelid!)&PId=\(productID!)"
            
            AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_Accessories?"+dataString, viewController: self, success: { (json2,responce) in
                print(json2)
                
                let sendr2 = [json,json2]
                self.performSegue(withIdentifier: "ProductSummeryVC", sender: sendr2)
                
                
                //                if json.array?.count != 0
                //                {
                //                    //self.prepare(for: "", sender: nil)
                //
                //                }
                //                else
                //                {
                //                    AlWrapper.showAlertMessage(vc: self, titleStr:"Alert!", messageStr:"Nodata Avilable")
                //                }
                
            }, failure: { (error) in
                
            })
            //                if json.array?.count != 0
            //                {
            //                    //self.prepare(for: "", sender: nil)
            //
            //                }
            //                else
            //                {
            //                    AlWrapper.showAlertMessage(vc: self, titleStr:"Alert!", messageStr:"Nodata Avilable")
            //                }
            
        }, failure: { (error) in
            
        })
        
       
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(sender)
        navigationItem.title = nil
//        if segue.identifier == "ProductSummeryVC"{
//            let vc = segue.destination as! ProductSpecificationVC
//            vc.navigationItem.title = ""
//            vc.productList = sender as! [JSON]
//            // vc.navigationController?.navigationBar.backItem?.title = ""
//            vc.navigationItem.title = "Product List"
//            //   vc.navigationItem.backBarButtonItem?.title = ""
//        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
