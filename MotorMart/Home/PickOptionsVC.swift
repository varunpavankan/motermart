//
//  PickOptionsVC.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
 let appdelegate = UIApplication.shared.delegate as? AppDelegate
class PickOptionsVC: UIViewController {

    @IBOutlet weak var FinanceButton: UIButton!
    @IBOutlet weak var cashButton: UIButton!
    @IBOutlet weak var usedButton: UIButton!
    @IBOutlet weak var newButton: UIButton!
    var finance:Bool = false
    var used:Bool = false
    var parseDatadic = Dictionary<String, String>()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        usedButton.layer.cornerRadius = 3
        usedButton.clipsToBounds = true
        newButton.layer.cornerRadius = 3
        newButton.clipsToBounds = true
        FinanceButton.layer.cornerRadius = 3
        FinanceButton.clipsToBounds = true
        cashButton.layer.cornerRadius = 3
        cashButton.clipsToBounds = true
     //   self.navigationItem.title = "Please Pick Your Options"
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOnProcedButton(_ sender: Any) {
        if finance == false
        {
            AlWrapper.showAlertMessage(vc: self, titleStr:"Alert!", messageStr:"Please select Finance Option")
        }
        else if used == false
        {
            AlWrapper.showAlertMessage(vc: self, titleStr:"Alert!", messageStr:"Please select BUY Option")

        }
        else{
            print(appdelegate?.parseDatadic)
            if (parseDatadic).isEmpty == true
            {
                parseDatadic = (appdelegate?.parseDatadic)!
            }
              parseDatadic = (appdelegate?.parseDatadic)!
           // ["Pro_Used_New": "1", "modelId": "1", "cityId": "14", "makeID": "1", "Pmttype": "1"]
            let dataString = "Pcno=\(parseDatadic["Pcno"]!)&Mno=\(parseDatadic["makeID"]!)&locno=\(parseDatadic["cityId"]!)&Pmttype=\(parseDatadic["Pmttype"]!)&Pro_Used_New=\(parseDatadic["Pro_Used_New"]!)"
            AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GETPRODUCTLIST?"+dataString, viewController: self, success: { (json,responce) in
              print(json)
                self.performSegue(withIdentifier: "ProductListVC", sender:json)
//                if json.array?.count != 0
//                {
//                    //self.prepare(for: "", sender: nil)
//                    
//                }
//                else
//                {
//                    AlWrapper.showAlertMessage(vc: self, titleStr:"Alert!", messageStr:"Nodata Avilable")
//                }
                
            }, failure: { (error) in
                
            })
        }
    }
    @IBAction func didTapOnCashFinanceButton(_ sender: UIButton) {
        finance = true
        if sender.tag == 1
        {
            // usedButton.isSelected = true
            appdelegate?.parseDatadic["Pmttype"] = "\(1)"
            FinanceButton.setTitleColor(UIColor.white, for: .normal)
            FinanceButton.backgroundColor = UIColor.lightGray
            cashButton.setTitleColor(UIColor.black, for: .normal)
            cashButton.backgroundColor = UIColor.white
            //  newButton.isSelected = false
        }
        else
        {
            cashButton.setTitleColor(UIColor.white, for: .normal)
            cashButton.backgroundColor = UIColor.lightGray
            FinanceButton.setTitleColor(UIColor.black, for: .normal)
            FinanceButton.backgroundColor = UIColor.white
             appdelegate?.parseDatadic["Pmttype"] = "\(0)"
            
        }
    }
    @IBAction func didTapOnUsedNewButton(_ sender: UIButton) {
        used = true
        if sender.tag == 1
        {
           // usedButton.isSelected = true
            
            usedButton.setTitleColor(UIColor.white, for: .normal)
            usedButton.backgroundColor = UIColor.lightGray
            newButton.setTitleColor(UIColor.black, for: .normal)
            newButton.backgroundColor = UIColor.white
              appdelegate?.parseDatadic["Pro_Used_New"] = "\(2)"
          //  newButton.isSelected = false
        }
        else
        {
            newButton.setTitleColor(UIColor.white, for: .normal)
            newButton.backgroundColor = UIColor.lightGray
            usedButton.setTitleColor(UIColor.black, for: .normal)
            usedButton.backgroundColor = UIColor.white
             appdelegate?.parseDatadic["Pro_Used_New"] = "\(1)"
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(sender)
        navigationItem.title = nil
        if segue.identifier == "ProductListVC"{
            let vc = segue.destination as! ProductListVC
            vc.navigationItem.title = ""
            appdelegate?.productList = sender as! JSON
            vc.productList = sender as! JSON
            vc.parseDatadic = (appdelegate?.parseDatadic)!
            // vc.navigationController?.navigationBar.backItem?.title = ""
            vc.navigationItem.title = "Product List"
            //   vc.navigationItem.backBarButtonItem?.title = ""
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationItem.title = "Please Pick Your Options"
  
    }
    override func didReceiveMemoryWarning() {
       
        super.didReceiveMemoryWarning()
   //     self.navigationController?.navigationBar.backItem?.title = ""
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
