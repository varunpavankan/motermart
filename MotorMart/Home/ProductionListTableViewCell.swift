//
//  ProductionListTableViewCell.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class ProductionListTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var usedNewImageView: UIImageView!
    
    @IBOutlet weak var mileageLable: UILabel!
    @IBOutlet weak var priceLable: UILabel!
    @IBOutlet weak var OnroadLable: UILabel!
    @IBOutlet weak var bikeNameLable: UILabel!
    @IBOutlet weak var providerLable: UILabel!
    @IBOutlet weak var contenImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        let screenSize = UIScreen.main.bounds
//        let separatorHeight = CGFloat(3.0)
//        let additionalSeparator = UIView.init(frame: CGRect(x: 0, y: self.frame.size.height-separatorHeight, width: screenSize.width, height: separatorHeight))
//        additionalSeparator.backgroundColor = UIColor.gray
//        self.addSubview(additionalSeparator)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
