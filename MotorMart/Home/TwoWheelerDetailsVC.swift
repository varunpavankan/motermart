//
//  TwoWheelerDetailsVC.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation
class TwoWheelerDetailsVC: UIViewController {

    @IBOutlet weak var HiddenLocationLable: UILabel!
    @IBOutlet weak var SelectMakeImage: UIImageView!
    @IBOutlet weak var selecVechicelLable: UILabel!
    @IBOutlet weak var selectLocationLable: UILabel!
    @IBOutlet weak var selectLocationButton: UIButton!
    @IBOutlet weak var selectModelLabel: UILabel!
    @IBOutlet weak var selectModelButton: UIButton!
    @IBOutlet weak var selectMakeLable: UILabel!
    @IBOutlet weak var selectMake: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var selectLocation: UIView!
    @IBOutlet weak var selectVehicalType: UIView!
    @IBOutlet weak var selectCompaynyView: UIView!
    var selectCompanyDropDown = DropDown()
    var selectModelDropDown = DropDown()
    var selectCityDropDown = DropDown()
   var vahiclesdic = JSON()
    var dropdownData = [String]()
    var parseDatadic = Dictionary<String, String>()
    let appdelegate = UIApplication.shared.delegate as? AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupChooseArticleDropDown()
//        self.title = "Two Wheeler Details"
//        self.navigationController?.navigationBar.backItem?.title = ""
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
       // selectVehicalType.isHidden = true
      //  selectLocation.isHidden = true
        proceedButton.isHidden = true
//        self.title = "Two Wheeler Details"
//        self.navigationController?.navigationBar.backItem?.title = ""
       
      //  self.navigationController?.navigationBar.backItem?.title = ""
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
         self.navigationItem.title = "Two Wheeler Details"
      //  self.navigationItem.backBarButtonItem?.title = ""
//        self.navigationController?.navigationBar.backItem?.title = ""
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        navigationItem.title = nil
        if segue.identifier == "pickOptionsVC"{
            let vc = segue.destination as! PickOptionsVC
            vc.parseDatadic = (self.appdelegate?.parseDatadic)!
            vc.navigationItem.title = ""
            // vc.navigationController?.navigationBar.backItem?.title = ""
            vc.navigationItem.title = "Please Pick Your Options"
            //   vc.navigationItem.backBarButtonItem?.title = ""
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    //    self.navigationController?.navigationBar.backItem?.title = ""
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectOnNextArrow(_ sender: Any) {
        selectVehicalType.isHidden = false
        
    }
    
    @IBAction func didTapOnselectVehicalArrow(_ sender: Any) {
          selectLocation.isHidden = false
        
    }
    
    @IBAction func didTapOnSelectMakeButton(_ sender: Any) {
        selectCompanyDropDown.show()
    }
    @IBAction func didTapOnLocationArrow(_ sender: Any) {
        
      
    }
    @IBAction func didTapOnModelButton(_ sender: Any) {
        selectModelDropDown.show()
    }
    @IBAction func didTapOnLocationButton(_ sender: Any) {
          selectCityDropDown.show()
    }
    /*
   
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
/*Drop Down extension*/
extension TwoWheelerDetailsVC
{
    func setupChooseArticleDropDown() {
        selectCompanyDropDown.anchorView = selectMake
        
        // Will set a custom with instead of anchor view width
        //        dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        var imagedataSource = [String]()
        selectCompanyDropDown.bottomOffset = CGPoint(x: -50, y: selectMake.bounds.height)
        
        let locaarray = vahiclesdic.array
        if locaarray == nil || locaarray?.count == 0
        {
            
        }
            
        else
        {
         
            for val in locaarray!
            {
                let str = val["makeName"].string
                let image = val["image"].string
//                let id = val["id"].int
//                let dic = ["property_address" : str!, "property_master_id" : id!] as [String : Any]
             //   self.placedicArray.append(dic as NSDictionary)
                imagedataSource.append(image!)
                dropdownData.append(str!)
            }
            // You can also use localizationKeysDataSource instead. Check the docs.
            selectCompanyDropDown.dataSource = dropdownData
            selectCompanyDropDown.imageSource = imagedataSource
            
            // Action triggered on selection
            selectCompanyDropDown.selectionAction = { [weak self] (index, item) in
                
                print(index)
                let MakeId:Int = (self?.vahiclesdic[index]["makeId"].int)!
                let makeIDStr =  "\(MakeId)"
                self?.selectMakeLable.text = self?.dropdownData[index]
                let url = URL(string: imagedataSource[index])
                //self?.parseDatadic = ["maakeID":makeIDStr]
              //  self?.parseDatadic.set
             //   self?.parseDatadic["makeID"]!.append(makeIDStr)
                self?.appdelegate?.parseDatadic["makeID"] = makeIDStr
                self?.parseDatadic["makeID"] = makeIDStr
                
                print(self?.parseDatadic as Any)
                self?.SelectMakeImage.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                print(makeIDStr)
             //   let val = self?.placedicArray[index]
             //   self?.property_master_id =  val!["property_master_id"] as! Int
            //    self!.selectPropertyLable.text = item
                print("http://loan2buy.com/api/master/getmodel?makeid=\(makeIDStr)")
                AlWrapper.requestGETURL("http://loan2buy.com/api/master/getmodel?makeid=\(String(describing: MakeId))", viewController: self!, success: { (json,responce) in
                    
                    print(json)
                    self?.selecVechicelLable.isHidden = true
                    self?.setupModelDropDown(dic: json)
                    
                }, failure: { (error) in
                    
                })
                self?.selectCompanyDropDown.hide()
            }
            
        }
        
        selectCompanyDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
            }
        }
    }
    
    func setupModelDropDown(dic:JSON)
    {
        selectModelDropDown.anchorView = selectModelButton
        
        // Will set a custom with instead of anchor view width
        //        dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        selectModelDropDown.bottomOffset = CGPoint(x: 0, y: selectModelButton.bounds.height)
        var selectStringDic = [String]()
        let locaarray = dic.array
        if locaarray == nil || locaarray?.count == 0
        {
            
        }
            
        else
        {
            
            for val in locaarray!
            {
                let str = val["modelName"].string
                //                let id = val["id"].int
                //                let dic = ["property_address" : str!, "property_master_id" : id!] as [String : Any]
                //   self.placedicArray.append(dic as NSDictionary)
                selectStringDic.append(str!)
            }
            // You can also use localizationKeysDataSource instead. Check the docs.
            selectModelDropDown.dataSource = selectStringDic
            
            // Action triggered on selection
            selectModelDropDown.selectionAction = { [weak self] (index, item) in
                
                self?.selectModelLabel.text = selectStringDic[index]
                AlWrapper.requestGETURL("http://loan2buy.com/api/master/getcity", viewController: self!, success: { (json,responce) in
                    print(locaarray![index]["modelId"])
                    let str = "\(locaarray![index]["modelId"])"
                     self?.appdelegate?.parseDatadic["modelId"] = str
                    self?.parseDatadic["modelId"] = str
                 //    self?.parseDatadic.updateValue("modelId", forKey: str)
                   // (self?.parseDatadic["modelId"]! as AnyObject).append(str)
                    print(self?.parseDatadic as Any)
                    print(json)
                    self?.appdelegate?.citylist = json
                    self?.HiddenLocationLable.isHidden = true
                    self?.setupCityDropDown(dic: json)
                    
                }, failure: { (error) in
                    
                })
                self?.selectModelDropDown.hide()
            }
            
        }
        
        selectCompanyDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
            }
        }
    }

    func setupCityDropDown(dic:JSON)
    {
        selectCityDropDown.anchorView = selectLocationButton
        
        // Will set a custom with instead of anchor view width
        //        dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        selectCityDropDown.bottomOffset = CGPoint(x: 0, y: selectLocationButton.bounds.height)
        var selectCitydic = [String]()
        let locaarray = dic.array
        if locaarray == nil || locaarray?.count == 0
        {
            
        }
            
        else
        {
            
            for val in locaarray!
            {
                let str = val["CityName"].string
                //                let id = val["id"].int
                //                let dic = ["property_address" : str!, "property_master_id" : id!] as [String : Any]
                //   self.placedicArray.append(dic as NSDictionary)
                selectCitydic.append(str!)
            }
            // You can also use localizationKeysDataSource instead. Check the docs.
            selectCityDropDown.dataSource = selectCitydic
            
            // Action triggered on selection
            selectCityDropDown.selectionAction = { [weak self] (index, item) in
                self?.proceedButton.isHidden = false
                self?.selectLocationLable.text = selectCitydic[index]
                let str = "\(locaarray![index]["cityId"])"
                self?.appdelegate?.parseDatadic["cityId"] = str
                self?.parseDatadic["cityId"] = str
                self?.selectCityDropDown.hide()
            }
            
        }
        
        selectCompanyDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
            }
        }
    }
    
    
}




