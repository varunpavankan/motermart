//
//  ProductSpecificationVC.swift
//  MotorMart
//
//  Created by Nennu on 09/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import GTSheet
import SwiftyJSON
import Alamofire
struct productvalues {
    var ProductID: String
    var Category_Id: String
    var loan_name: String
    func ProductId()->String
    {
       return self.ProductID
    }
    func CategoryId()->String
    {
        return self.Category_Id
    }
    func loanname()->String
    {
        return self.loan_name
    }
}

struct summaryonvales {
    var ExshowrumPrice: String
    var InsurnceAmt: String
    var RoadTaxAmount: String
    var HypotCharegs: String
    var Temp_passing: String
    var HandlingCharges: String
    var RegsAmount: String
    var Accessories : String
    var Total :  String
    
}
class ProductSpecificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITabBarDelegate ,HalfSheetPresentingProtocol,Accessoriesdelegate{
    func didtapOnSavebutton() {
        print("done")
    }
    
    let vc = Accessories()
   

    func didtapOnSavebutton(_ sender: Any) {
        print("didtapon")
    }
      var productList = [JSON]()
    var transitionManager: HalfSheetPresentationManager!
    let notificationName = Notification.Name("NotificationIdentifier")
    
    // Register to receive notification
    
    @IBOutlet weak var usedbikedownloadButton: UITabBarItem!
    
    @IBOutlet weak var usedbikeTabBar: UITabBar!
    @IBOutlet weak var bookaTestdriveBarbutton: UITabBarItem!
    @IBOutlet weak var downloadbarButton: UITabBarItem!
    @IBOutlet weak var makeanOfferbarbutton: UITabBarItem!
    
    @IBOutlet weak var appointmentbarButton: UITabBarItem!
    @IBOutlet weak var tableView: UITableView!
    var imageArray = ["bikesample.jpg","bikesample.jpg","bikesample.jpg","bikesample.jpg"]
    var empArray = ["EX Showroom price":"5000","Insurance":"3000","Road Tax":"4000","Hypothication":"500"]
    var summaryONroadPrice = ["EX Showroom price","Insurance","Road Tax","Hypothication","Temp Passing","Handling Charges","Registration","Accessories","On Road Price"]
    var headersArray = ["Summary Of On Road Price","Specification","Capacity & Mileage","Body Dimensions","Engine","Transmission","Electricals","Tyres & Breaks","Frame & Suspension"]
    var CapacityArray = ["Mileage","Capacity","Fuel Type"]
    var Bodydimensions = ["Leanth in (mm)","Width in (mm)","Height in (mm)","Wheel Base","Ground Clearance","Kerb Weight","Fuel Tank Capacity"]
    var engiearray = ["Type","Emission Standard","Displacement","Bore","Stroke","Max net Torque","Compression Ratio","Air Filter Type","Starting Method"]
    var TransmissionArray = ["No of Gears","Grear Pattern","Max Speed"]
    var electricalsArray = ["Battery","Head Lamp"]
    var tyresBreaksArray = ["Tyre Size Front","Tyre Size Rare","Tyre Size Front","Brake Type Size \n(front)","Break Type Size \n (Rear)"]
    var framesuspension = ["Frame Type","Frame Rear","Frame Front"]
    var oldvechicleArray = ["Capacity","Milleage kmpl","Fuel Type","Owner Name","Vechicle Numaber",]
    
    var summaryonvalr = [String]()
    var capasityValuesarray = [String]()
    var bodydimesionsValuearray = [String]()
    var enginValueValuearray = [String]()
    var transmissionValuearray = [String]()
    var eletriclesValuearray = [String]()
    var tyresbrakeValueArray = [String]()
    var frameandsuspensionValuearray = [String]()
    var oldvechicalValueArray = [String]()
     let webView = UIWebView()
    
    var expandbool:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBarItem.appearance().setTitleTextAttributes([kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white], for: .normal)
       print(productList)
        let seteproductDetails = setproductdetails()
        appdelegate?.setproductDetails.productdetails = productList[0][0]
        print(seteproductDetails.productdetails)
        let dictionary = replaceNullValues(dictionary: productList[0][0])
        let defaultaccprice  = caluclateccessories(dic: productList[1])
        print(defaultaccprice)
    //    var dic1 = (productList[0].dictionary as Dictionary<String,String>)!
//        let dic = productList[0] as JSON
//        
//        print(dic["ExshowrumPrice"].string!)
        summaryonvalr = [dictionary["ExshowrumPrice"],dictionary["InsurnceAmt"],dictionary["RoadTaxAmount"],dictionary["HypotCharegs"],dictionary["Temp_passing"],dictionary["HandlingCharges"],dictionary["RegsAmount"],"\(defaultaccprice)",dictionary["Total"]] as! [String]
        capasityValuesarray = [dictionary["kmPl"],dictionary["cc"],dictionary["fuelType"]] as! [String]
        bodydimesionsValuearray = [dictionary["BodyLength"],dictionary["BodyWidth"],dictionary["BodyHeight"],dictionary["BodyWheelbase"],dictionary["BodygroundClearnece"],dictionary["BodyKerbWeight"],dictionary["BodyfuelTank"]] as! [String]
        enginValueValuearray = [dictionary["EngineType"],dictionary["EngineEmission"],dictionary["EngineDisplacement"],dictionary["EngineBore"],dictionary["EngineStroke"],dictionary["EngineMAxnetTorque"],dictionary["EngineCompression"],dictionary["EngineAirFilterType"],dictionary["EngineStarting"]] as! [String]
         transmissionValuearray = [dictionary["TransmissionNofoGears"],dictionary["TransmissionGearPattern"],dictionary["TransissionMAxSpeed"]] as! [String]
        eletriclesValuearray = [dictionary["ElectricalsBattery"],dictionary["ElectricalHeadLamp"]] as! [String]
        tyresbrakeValueArray = [dictionary["TyreSizeFront"],dictionary["TyreSizeRear"],dictionary["TyreTypeFront"],dictionary["TyreTypeRear"],dictionary["BreakTypeFront"],dictionary["BreakTypeRear"]] as! [String]
        frameandsuspensionValuearray =  [dictionary["FrameType"],dictionary["FrameRear"],dictionary["FrameFront"]] as! [String]
        oldvechicalValueArray = [dictionary["kmPl"],dictionary["cc"],dictionary["fuelType"],dictionary["ownername"],dictionary["VehNumber"]] as! [String]
        usedbikeTabBar.delegate = self
        if appdelegate?.parseDatadic["Pro_Used_New"] == "1"
        {
            usedbikeTabBar.isHidden = true
        }
        else
        {
            usedbikeTabBar.isHidden = false

        }
        print(appdelegate?.parseDatadic)
        
          NotificationCenter.default.addObserver(self, selector: #selector(self.dissmissAnimation), name: NSNotification.Name(rawValue: "TabBarDissmissed"), object: nil)
        
      //  usedbikeTabBar
       //  Do any additional setup after loading the view.
    }
    
    @objc func dissmissAnimation(notification: NSNotification)
    {
        
        var userinfo = notification.userInfo
        if userinfo!["ClassName"] as! String == "Download"
        {
           // let url = userinfo!["FilePath"] as! URL
       //     let req = URLRequest(url: url)
            DispatchQueue.main.async {
                self.view.showBlurLoader()
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                //    self.performSegue(withIdentifier: "personalDetailSegue", sender:dic)
                let mainViewController = storyBoard.instantiateViewController(withIdentifier: "WebVewController") as! WebVewController
                mainViewController.title = "Downloaded In File App"
                mainViewController.urlString = userinfo!["FilePath"] as! String
                
                // UIApplication.shared.delegate?.window??.rootViewController = nvc
                self.navigationController?.pushViewController(mainViewController, animated: false)
                self.view.removeBluerLoader()
                
            }
           
        }
        else
        {
          //  ["ClassName":"bookantestdrive","responce":"\(json)"]
        //    print(userinfo)
            
            if userinfo!["responce"] as! String == "1"
            {
                 AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Sucessfully Updated")
                
            }
            else
            {
                 AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Unable to Update at this time")
                
            }
        }

    }

    
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        print("function dismissed")
    }
    func caluclateccessories(dic:JSON)-> Float
    {
        var value : Float = 0
      
        for val in dic.array!
        {
            if val["isdefault"] == "True"
            {
              print(val["unitPrice"].string!)
                let untprice = Float(val["unitPrice"].string!)!
                
                value = untprice + value
                
            }
            
//            for (key,value) in val.dictionary!
//            {
//                if key == "isdefault"
//                {
//                   if value == "True"
//                }
//            }
            
        }
        
        return value
        
    }
    
    
    
    func replaceNullValues(dictionary:JSON) ->  Dictionary<String,String>
    {
       // var dic = dictionary.dictionary
        var dict = Dictionary<String,String>()
        
        for (key, value) in dictionary
        {
            if (value == JSON.null) {
            dict[key] = "null"
                
            }
            else
            {
                dict[key] = value.string
            }
       
        }
        
        return dict
    }

    @IBAction func didTapOnaccessorieButton(_ sender: Any) {
       // Accessories
        
      //  vc.parsedataList = productList[0][1]
        var vc = Accessories()
        vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Accessories") as! Accessories
        vc.parsedataList = productList[1]
        vc.productId = productList[0][0]["prodId"].string!
        vc.sellerId = productList[0][0]["sellerId"].string!
        presentUsingHalfSheet(
           vc
    )
        
    }
    
    func sendrdetails()
    {
         let parseDatadic = appdelegate?.parseDatadic
        let str = "_usercode=\(UserDefaults.standard.string(forKey: "_User_Id")!)&Productid=\(self.productList[0][0]["prodId"].string!)&Modelid=\(parseDatadic!["modelId"] ?? "1")&seller=\(self.productList[0][0]["sellerId"].string!)"
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_EXITS_LoanStage?"+str, viewController: self, success: { (json2,responce) in
            print(json2)
            if json2.isEmpty == true
            {
                
            }
            else{
                let stage = json2[0]["Stage_on"]
                if stage == "0"
                {
                    var dic = [String:String]()
                    dic["ProductID"] = self.productList[0][0]["prodId"].string!
                    dic["Category_Id"] = "1"
                    dic["loan_name"] = "Two wheeler"
                    let values = productvalues(ProductID: self.productList[0][0]["prodId"].string!, Category_Id: "1", loan_name: "Two wheeler")
                    print(values)
                    self.performSegue(withIdentifier: "KYCVC", sender:dic)

                }
                else if stage == "1"
                {
//                    http://loan2buy.com/api/master/GET_DETALS_BY_LOAN_CATEGORY_FILTER? emailid=1866&Mobile=9101010101&Pcno=VID2018137220180312011227445&_p_no=1&UserID=1866&city=9
                    let str = "emailid=\(UserDefaults.standard.string(forKey: "_User_Id")!)&Mobile=\(UserDefaults.standard.string(forKey: "_Mobile_No")!)&Pcno=\(self.productList[0][0]["prodId"].string!)&_p_no=\(parseDatadic!["modelId"] ?? "1")"
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_DETALS_BY_LOAN_CATEGORY_FILTER?"+str, viewController: self, success: { (json2,responce) in
                        print(json2)
                        if json2.isEmpty == true
                        {
                            
                        }
                        else{
                             self.performSegue(withIdentifier: "loanList1", sender:nil)
            }
                    },failure: { (error) in
                        
                    })
                    
                   
                }
                else
                {
                     AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "you are alredy taken loan")
                    
                }
            }
        },failure: { (error) in
            
        })
        
    }
    
    
    
    @IBAction func didTapOnGetLoanButton(_ sender: Any) {
        
        self.GET_LoanQuestion_Details()
    
        //  self.performSegue(withIdentifier: "loanList1", sender:nil)
   
        
    }
    func GET_LoanQuestion_Details()
    {
        let parseDatadic = appdelegate?.parseDatadic
        let productList =  appdelegate?.setproductDetails.productdetails
        let str = "_usercode=\(UserDefaults.standard.string(forKey: "_User_Id")!)&EmailId=\(UserDefaults.standard.string(forKey: "_EmailId")!)&mobilenumber=\(UserDefaults.standard.string(forKey: "_Mobile_No")!)&Productid=\(productList!["prodId"].string!)&Modelid=\(parseDatadic!["modelId"] ?? "1")"
        
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_LoanQuestion_Details?"+str, viewController: self, success: { (json2,responce) in
            print(json2)
            if json2.isEmpty == true
            {
                var dic = [String:String]()
                dic["ProductID"] = self.productList[0][0]["prodId"].string!
                dic["Category_Id"] = "1"
                dic["loan_name"] = "Two wheeler"
                let values = productvalues(ProductID: self.productList[0][0]["prodId"].string!, Category_Id: "1", loan_name: "Two wheeler")
                print(values)
                self.performSegue(withIdentifier: "KYCVC", sender:dic)
                
            }
            else
            {
                print(json2)
                if json2[0]["Stage_on"].int == 0
                {
                    var dic = [String:String]()
                    dic["ProductID"] = self.productList[0][0]["prodId"].string!
                    dic["Category_Id"] = "1"
                    dic["loan_name"] = "Two wheeler"
                    let values = productvalues(ProductID: self.productList[0][0]["prodId"].string!, Category_Id: "1", loan_name: "Two wheeler")
                    print(values)
                    self.performSegue(withIdentifier: "KYCVC", sender:dic)
                }
                else if json2[0]["Stage_on"].int == 1
                {
                    self.GET_EXITS_PreApproval()
                }
            }
        }, failure: { (error) in
            
        })
    }
   func GET_EXITS_LoanStage()
   {
    let parseDatadic = appdelegate?.parseDatadic
    let str = "_usercode=\(UserDefaults.standard.string(forKey: "_User_Id")!)&Productid=\(self.productList[0][0]["prodId"].string!)&Modelid=\(parseDatadic!["modelId"] ?? "1")&seller=\(self.productList[0][0]["sellerId"].string!)"
    AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_EXITS_LoanStage?"+str, viewController: self, success: { (json2,responce) in
        print(json2)
        if json2.isEmpty == true
        {
            
        }
        else{
            let stage = json2[0]["Stage_on"]
            if stage == 0
            {
                              self.getexistingLoanStage ()
            }
            else if stage == 1
            {
                //                    http://loan2buy.com/api/master/GET_DETALS_BY_LOAN_CATEGORY_FILTER? emailid=1866&Mobile=9101010101&Pcno=VID2018137220180312011227445&_p_no=1&UserID=1866&city=9
                let str = "emailid=\(UserDefaults.standard.string(forKey: "_User_Id")!)&Mobile=\(UserDefaults.standard.string(forKey: "_Mobile_No")!)&Pcno=\(self.productList[0][0]["prodId"].string!)&_p_no=\(parseDatadic!["modelId"] ?? "1")&UserID=\(UserDefaults.standard.string(forKey: "_User_Id")!)&city=\(parseDatadic!["cityId"] ?? "9")"
                AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_DETALS_BY_LOAN_CATEGORY_FILTER?"+str, viewController: self, success: { (json2,responce) in
                    print(json2)
                    if responce.response?.statusCode == 400
                    {
                        AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Some thing went Wrong please try after Some time!")
                        
                    }
                    else {
                        if json2.isEmpty == true
                        {
                            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Data Not Avilable!")
                        }
                        else{
                            self.performSegue(withIdentifier: "loanList1", sender:json2)
                        }
                    }
                },failure: { (error) in
                    
                })
                
                
            }
            else
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "you are alredy taken loan")
                
            }
        }
    },failure: { (error) in
        
    })
    }
    func GET_EXITS_PreApproval()
    {
        let str = "_usercode=\(UserDefaults.standard.string(forKey: "_User_Id")!)"
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_EXITS_PreApproval?"+str, viewController: self, success: { (json2,responce) in
            print(json2)
            if json2[0]["stage_on"].string == "0"
            {
               self.GET_EXITS_LoanStage()
            }
            else
            {
              //  Loan application already generated...Thank you
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Loan application already generated...Thank you")
                
            }
            
        },failure: { (error) in
            
            
        })
        
    }
    
    func getexistingLoanStage ()
    {
       //  let str = "_usercode=\(UserDefaults.standard.string(forKey: "_User_Id")!)"
        let parseDatadic = appdelegate?.parseDatadic
        let proid = self.productList[0][0]["prodId"].string!
        print(self.productList)
        let selelenaderId = "0"
        let selecedsellerId = self.productList[0][0]["sellerId"].string!

//        params.put("Product_Id", productId);
//        params.put("Category_Id", "1");
//        params.put("Selected_Lender", "0");
//        params.put("Selected_Seller",sellerID);
//        params.put("User_Code", user_Id);
//        params.put("Model", modelNo);
        let perameters = ["Product_Id":proid,"Category_Id":"1","Selected_Lender":selelenaderId,"Selected_Seller":selecedsellerId,"User_Code":UserDefaults.standard.string(forKey: "_User_Id")!,"Model":"\(parseDatadic!["modelId"] ?? "1")"]
       
        
        AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"setLoanStage", viewController: self, params:  perameters as [String : AnyObject], headers: nil, success: { (json, responce) in
            print(json)
            if json.isEmpty == false
            {
                self.GET_LoanQuestion_Details()
            }
                
        }, failure: { (erro) in
            
            print(erro)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //   print(sender)
        navigationItem.title = nil
        if segue.identifier == "KYCVC"{
            let vc = segue.destination as! KycVc
            vc.userdetailsdic = sender as! [String:String]
            //   vc.navigationItem.backBarButtonItem?.title = ""
        }
        else if segue.identifier == "loanList1"
        {
            let vc = segue.destination as! LoansListVC
            vc.loanlist = sender as! JSON
            let dic = productList[0][0].dictionary
//            cell.bikeNameLable.text = (dic!["makeName"]?.string)! + (dic!["modelName"]?.string)!
//            let str = dic!["grossAmount"]?.int
            vc.productDetails = ["MaKe":"\((dic!["makeName"]?.string)!)","Model":"\((dic!["modelName"]?.string!)!)","Price":"\((dic!["grossAmount"]?.int!)!)","Color":" "]
        }
        //loanList1
        
    }

    
    @IBAction func didTapOnBookAnOfferButton(_ sender: Any) {
       // self.performSegue(withIdentifier: "loanList1", sender:nil)
//         AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Not yet Implemented")
        //let txnParam = Payumo
        PayUServiceHelper.sharedManager().getPayment(self, "mail@mymail.com", "8686410525", "Name", "0011", "521001", didComplete: { (dict, error) in
            if error != nil{
                print("Error")
            }else {
                print("Sucess")
            }
        }) { (error) in
            print("Payment Process Breaked")
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Product Specification"
         vc.coredelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.saveButtonClicked(notification:)), name: notificationName, object: nil)
    }
    @objc func saveButtonClicked(notification: Notification)
    {
        print(notification.userInfo as Any)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
         if appdelegate?.parseDatadic["Pro_Used_New"] == "2"
         {
            return 2
        }
        else
         {
        if expandbool == true
        {
            return headersArray.count+1
        }
        else
        {
            return 3
        }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
            
        }
        else if section == 1
        {
            if appdelegate?.parseDatadic["Pro_Used_New"] == "2"
            {
                return oldvechicleArray.count
            }
            else
            {
          return summaryONroadPrice.count
            }
        }
            else if section == 2
        {
            return 0
        }
        else
        {
          
            if expandbool == true
            {
                if section == 3
                {
                    return CapacityArray.count
                }
                    else if section == 4
                {
                    return Bodydimensions.count
                }
                    else if section == 5
                {
                    return engiearray.count
                }
                    else if  section == 6
                {
                    return TransmissionArray.count
                }
                    else if section == 7
                {
                    return electricalsArray.count
                }
                    else if section == 8
                {
                    return tyresBreaksArray.count
                }
                    else if section == 9
                {
                    return framesuspension.count
                }
                    
                else
                {
                   return empArray.count
                }
            }
            else
            {
                return 0
            }
        }
            
        
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
        return 350
        }
        else
        {
        return 40
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let parsedic = appdelegate?.parseDatadic
        if indexPath.section == 0
        {
            let cell:productDisPlayCell = self.tableView.dequeueReusableCell(withIdentifier: "productDisPlayCell") as! productDisPlayCell
            let dic = productList[0][0].dictionary
       //     cell.contenImageView.image = UIImage(named:imageArray[indexPath.section])
            let url = URL(string: (dic!["imaGe"]?.string)!)
            cell.providerLable.text = dic!["organizationName"]?.string
            cell.bikeNameLable.text = (dic!["makeName"]?.string)! + (dic!["modelName"]?.string)!
            let str = dic!["grossAmount"]?.int
            cell.priceLable.text = "₹" + "\(str ?? 100000)"
            if parsedic!["Pmttype"] == "0"
            {
                 cell.GetLoanOfferbutton.isHidden = true
                var frame =  cell.BookanOfferButton.frame
                frame = CGRect(x: cell.contentView.frame.size.width/2 - frame.size.width/2, y: frame.minY, width: frame.size.width, height: frame.size.height)
                cell.BookanOfferButton.frame = frame

                cell.BookanOfferButton.updateConstraints()
            }
            else
            {
                cell.GetLoanOfferbutton.isHidden = false
            }
            
         //   cell.mileageLable.text = "Mileage" + " " + (dic!["kmPl"]?.string)! + " KMPL"
            cell.contenImageView.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            return cell
        }
        else
        {
            let cell:priceandDetailsCell = self.tableView.dequeueReusableCell(withIdentifier: "priceandDetailsCell") as! priceandDetailsCell
            cell.accessoriesShowButton.isHidden = true
          
            if indexPath.section == 1
            {
                if appdelegate?.parseDatadic["Pro_Used_New"] == "2"
                {
                  //  return oldvechicleArray.count
                     cell.discreptionLabel.text = oldvechicleArray[indexPath.row]
                    if Float(oldvechicalValueArray[indexPath.row]) != nil
                    {
                        let val = Int(round(Float(oldvechicalValueArray[indexPath.row])!))
                        cell.ValueLabel.text =  "\(val)"
                    }
                    else
                    {
                        cell.ValueLabel.text =  oldvechicalValueArray[indexPath.row]
                    }
                    
                }
                else
                {
                cell.discreptionLabel.text = summaryONroadPrice[indexPath.row]
                let str = summaryONroadPrice[indexPath.row]
                if(str == "Accessories")
                {
                     cell.accessoriesShowButton.isHidden = false
                }
                if Float(summaryonvalr[indexPath.row]) != nil
                {
                    let val = Int(round(Float(summaryonvalr[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                     cell.ValueLabel.text =  summaryonvalr[indexPath.row]
                }
               
                }
            }
             else if indexPath.section == 3
              {
                cell.discreptionLabel.text = CapacityArray[indexPath.row]
               
            
              //  cell.ValueLabel.text = capasityValuesarray[indexPath.row]
                if Float(capasityValuesarray[indexPath.row]) != nil
                {
                    let val = Int(round(Float(capasityValuesarray[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                    cell.ValueLabel.text =  capasityValuesarray[indexPath.row]
                }
              }
                else if indexPath.section == 4
            {
                cell.discreptionLabel.text = Bodydimensions[indexPath.row]
                if Float(bodydimesionsValuearray[indexPath.row]) != nil
                {
                    let val = Int(round(Float(bodydimesionsValuearray[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                    cell.ValueLabel.text =  bodydimesionsValuearray[indexPath.row]
                }
              //  cell.ValueLabel.text = bodydimesionsValuearray[indexPath.row]
            }
            else if indexPath.section == 5
            {
                cell.discreptionLabel.text = engiearray[indexPath.row]
               // cell.ValueLabel.text = enginValueValuearray[indexPath.row]
                if Float(enginValueValuearray[indexPath.row]) != nil
                {
                    let val = Int(round(Float(enginValueValuearray[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                    cell.ValueLabel.text =  enginValueValuearray[indexPath.row]
                }
            }
            else if indexPath.section == 6
            {
                cell.discreptionLabel.text = TransmissionArray[indexPath.row]
               // cell.ValueLabel.text = transmissionValuearray[indexPath.row]
                if Float(transmissionValuearray[indexPath.row]) != nil
                {
                    let val = Int(round(Float(transmissionValuearray[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                    cell.ValueLabel.text =  transmissionValuearray[indexPath.row]
                }
            }
            else if indexPath.section == 7
            {
                cell.discreptionLabel.text = electricalsArray[indexPath.row]
               // cell.ValueLabel.text = eletriclesValuearray[indexPath.row]
                if Float(eletriclesValuearray[indexPath.row]) != nil
                {
                    let val = Int(round(Float(eletriclesValuearray[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                    cell.ValueLabel.text =  eletriclesValuearray[indexPath.row]
                }
            }
            else if indexPath.section == 8
            {
                cell.discreptionLabel.text = tyresBreaksArray[indexPath.row]
            //    cell.ValueLabel.text = tyresbrakeValueArray[indexPath.row]
                if Float(tyresbrakeValueArray[indexPath.row]) != nil
                {
                    let val = Int(round(Float(tyresbrakeValueArray[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                    cell.ValueLabel.text =  tyresbrakeValueArray[indexPath.row]
                }
            }
            else if indexPath.section == 9
            {
                cell.discreptionLabel.text = framesuspension[indexPath.row]
                //cell.ValueLabel.text = frameandsuspensionValuearray[indexPath.row]
                if Float(frameandsuspensionValuearray[indexPath.row]) != nil
                {
                    let val = Int(round(Float(frameandsuspensionValuearray[indexPath.row])!))
                    cell.ValueLabel.text =  "\(val)"
                }
                else
                {
                    cell.ValueLabel.text =  frameandsuspensionValuearray[indexPath.row]
                }
            }

            else
            {
                cell.discreptionLabel.text = "EX Showroom price"
                cell.discreptionLabel.text = "5000"
            }
           
            return cell
            
        }
      
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        print("sectin: \(Int(section))")
        if section == 2
        {
            let headerView = self.tableView.dequeueReusableCell(withIdentifier: "header_cell") as? HeaderCell
            if expandbool == true
            {
                headerView?.headerView_obj.backgroundColor = UIColor(red:0/255, green: 180/255, blue: 109/255, alpha: 1.0)
               // headerView?.headerView_obj.backgroundColor = UIColor.green
            }
            else
            {
                
                headerView?.headerView_obj.backgroundColor = UIColor.orange
            }
            headerView?.header_lbl.text = headersArray[section - 1]
            return headerView?.contentView
        }
        else
        {
            let headerView = self.tableView.dequeueReusableCell(withIdentifier: "header_cell2") as? HeaderCell2
            headerView?.headerLable.text = headersArray[section - 1]
            if section != 1
            {
                headerView?.hraderView.backgroundColor = UIColor(red:0/255, green: 121/255, blue: 185/255, alpha: 1.0)
            }
           // headerView?
            return headerView?.contentView
            
        }
        
       
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
//        params.put("Fname", Firstname);
//        params.put("UserId", userId);
//        params.put("Address", address);
//        params.put("cDate", date);
//        params.put("Visit", Visit);
//        params.put("sellerID", sellerid);
//        params.put("Amt", amount);
//        params.put("Type", type);
//        params.put("ProductID", productid);
//        params.put("UserEmail", email_id);
//        params.put("UserName", USERNAMAE);
//        params.put("scheduledate", date);
//        params.put("ModelName", modelName);
        
        let Fname = UserDefaults.standard.string(forKey: "_User_name")
        let userID = UserDefaults.standard.string(forKey: "_User_Id")
         let email_id = UserDefaults.standard.string(forKey: "_EmailId")
        let UserName =  UserDefaults.standard.string(forKey: "_User_name")
        let proid = self.productList[0][0]["prodId"].string!
        let selecedsellerId = self.productList[0][0]["sellerId"].string!
        let modelName = self.productList[0][0]["modelName"].string!
        let userdetailsdic = ["Firstname":Fname,"userID":userID,"email_id":email_id,"UserName":UserName,"proid":proid,"selecedsellerId":selecedsellerId,"modelName":modelName]

        if item == appointmentbarButton
        {
            var vc = InputVC()
           // var vc = Download()
            vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "inputVC") as! InputVC
            vc.userdetailsdic = userdetailsdic as! [String : String]
            presentUsingHalfSheet(
//                UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "inputVC")
                vc
            )
        }
        else if item == makeanOfferbarbutton
        {
            print("Make an Offer Booked")
            var vc = MakeanOffer()
            // var vc = Download()
            vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MakeanOffer") as! MakeanOffer
            vc.userdetailsdic = userdetailsdic as! [String : String]
            presentUsingHalfSheet(
                vc
            )
        }
        else if item == downloadbarButton
        {
            print("Download")
            var vc = Download()
            vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Download") as! Download
            vc.productdetails = productList[0]
            //            vc.productId = productList[0][0]["prodId"].string!
            //            vc.sellerId = productList[0][0]["sellerId"].string!
            presentUsingHalfSheet(
                vc
            )
            
//            print("Download")
//            presentUsingHalfSheet(
//                UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Download")
//            )
        }
            else if item == usedbikedownloadButton
        {
            print("Download")
            var vc = Download()
            vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Download") as! Download
            vc.productdetails = productList[0]
//            vc.productId = productList[0][0]["prodId"].string!
//            vc.sellerId = productList[0][0]["sellerId"].string!
            presentUsingHalfSheet(
                vc
            )
//            presentUsingHalfSheet(
//                UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Download")
//            )
        }
            
        else
        {
            print("bookantestdrive")
            var vc = bookantestdrive()
            // var vc = Download()
            vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bookantestdrive") as! bookantestdrive
            vc.userdetailsdic = userdetailsdic as! [String : String]
            presentUsingHalfSheet(
                vc
            )
//            presentUsingHalfSheet(
//                UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bookantestdrive")
//            )
            
        }
        //This method will be called when user changes tab.
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
      if section == 0
      {
        return 0
        }
        else
      {
        return 44
        }
    }
    
    @IBAction func didTapOnHeaderbutton(_ sender: Any) {
        expandbool = !expandbool
        self.tableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}





class InputVC: UIViewController, HalfSheetPresentableProtocol, HalfSheetTopVCProviderProtocol {
    
    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }
    
    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()
    
    private var isKeyboardUp = false {
        didSet {
            let keyboardHeight: CGFloat = isKeyboardUp ? 300.0 : 0.0
            sheetHeight = 250.0 + keyboardHeight
            didUpdateSheetHeight()
        }
    }
    let datePickerView:UIDatePicker = UIDatePicker()
    
   
    
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var enterTimeTxfld: UITextField!
    
    @IBOutlet weak var enterDateTxfld: UITextField!
    @IBOutlet weak var enterAddressTxfld: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var subViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var visittoSellerSwitch: UISwitch!
    @IBOutlet weak var sellerTovistme: UISwitch!
    @IBOutlet weak var textField: UITextField?
    var storedSheetHeight = CGFloat()
    var sheetHeight: CGFloat? = 250.0
    var userdetailsdic = [String:String]()
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        visittoSellerSwitch.addTarget(self, action:#selector(didChangeonSwitchState(_:)), for: .valueChanged)
        super.viewWillAppear(true)
        submitButton.layer.borderWidth = 0.8
        submitButton.layer.cornerRadius = 10
        submitButton.layer.borderColor = UIColor.lightGray.cgColor
       
        subView.isHidden = true
        sheetHeight = visittoSellerSwitch.frame.maxY + 100
        didUpdateSheetHeight()
        let color = UIColor(red:0/255, green: 130/255, blue: 193/255, alpha: 1.0)
         AlWrapper.setupTextfieldLeftView(imageString: "address.png",textfield:enterAddressTxfld, backgroundcolor: color,backgroundbool: true)
        AlWrapper.setupTextfieldLeftView(imageString: "time.png",textfield:enterTimeTxfld, backgroundcolor: color,backgroundbool: true)
         AlWrapper.setupTextfieldLeftView(imageString: "calendar.png",textfield:enterDateTxfld, backgroundcolor: color,backgroundbool: true)
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        if datebool == true
        {
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            
            self.enterDateTxfld.text = dateFormatter.string(from: sender.date)
        }
        if timebool == true
        {
            dateFormatter.dateStyle = DateFormatter.Style.none
            
            dateFormatter.timeStyle = DateFormatter.Style.medium
            
            self.enterTimeTxfld.text = dateFormatter.string(from: sender.date)
        }
       
        
    }
    @IBAction func didChangeonSwitchState(_ sender: UISwitch) {
        if (sender.isOn == true){
            
            if sender.tag == 0
            {
                subView.isHidden = false
                sheetHeight = subView.frame.maxY + 50
                storedSheetHeight = sheetHeight!
                enterAddressTxfld.isHidden = false
                visittoSellerSwitch.isOn = false
                didUpdateSheetHeight()
                
            }
            else
            {
                subView.isHidden = false
                sheetHeight = subView.frame.maxY
                 storedSheetHeight = sheetHeight!
                sellerTovistme.isOn = false
                enterAddressTxfld.isHidden = true
                didUpdateSheetHeight()
                
            }
            
            print(sender.tag)
            
        }
        else{
            print(sender.tag)
        }
    }
    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }
    
    @IBAction func didTapOnSubbmitButton(_ sender: Any) {
       
        
        if sellerTovistme.isOn == true
        {
            if enterAddressTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the Address")
                
            }
            else if enterDateTxfld.text?.isEmpty == true
            {
                 AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Date")
            }
            else if enterTimeTxfld.text?.isEmpty == true
            {
                 AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Time")
            }
            else
            {

                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: Date()) // string purpose I add here
                // convert your string to date
               // let yourDate = formatter.date(from: myString)
                
                let userdetails = ["Fname":userdetailsdic["Firstname"]!,"UserId":userdetailsdic["userID"]!,"Address":enterAddressTxfld.text!,"cDate":myString,"Visit":1,"sellerID":userdetailsdic["selecedsellerId"]!,"Amt":0,"Type":1,"ProductID":userdetailsdic["proid"]!,"UserEmail":userdetailsdic["email_id"]!,"UserName":userdetailsdic["UserName"]!,"scheduledate":enterDateTxfld.text! + enterTimeTxfld.text!,"ModelName":userdetailsdic["modelName"]!] as [String : Any]
                
                AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"insertAppointment", viewController: self, params: userdetails as [String : AnyObject], headers: nil, success: { (json, responce) in
                    print(json)
                    self.dismiss()
                     NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"InputVC","responce":"\(json)"])
                    
                }) { (erro) in
                    
                    print(erro)
                    self.dismiss()
                      NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"InputVC","responce":"\(erro)"])
                }
                
                
                
            }
            
        }
        else if visittoSellerSwitch.isOn == true
        {
           if enterDateTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Date")
            }
            else if enterTimeTxfld.text?.isEmpty == true
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Time")
            }
            else
            {
//                AlWrapper.showAlertMessage(vc: self, titleStr: "Sucess!", messageStr: "Request Taken sucessfully")
//                dismiss(animated: true)
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: Date()) // string purpose I add here
                // convert your string to date
                // let yourDate = formatter.date(from: myString)
                
                let userdetails = ["Fname":userdetailsdic["Firstname"]!,"UserId":userdetailsdic["userID"]!,"Address":"","cDate":myString,"Visit":1,"sellerID":userdetailsdic["selecedsellerId"]!,"Amt":0,"Type":2,"ProductID":userdetailsdic["proid"]!,"UserEmail":userdetailsdic["email_id"]!,"UserName":userdetailsdic["UserName"]!,"scheduledate":enterDateTxfld.text! + enterTimeTxfld.text!,"ModelName":userdetailsdic["modelName"]!] as [String : Any]
                
                AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"insertAppointment", viewController: self, params: userdetails as [String : AnyObject], headers: nil, success: { (json, responce) in
                    print(json)
                    self.dismiss()
                      NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"InputVC","responce":"\(json)"])
                    
                }) { (erro) in
                    
                    print(erro)
                    self.dismiss()
                      NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"InputVC","responce":"\(erro)"])
                }
            }
        }
    }
    @IBAction func dismiss() {
        dismiss(animated: true)
    }
    var datebool : Bool = false
    var timebool : Bool =  false
    
    @IBAction func editingDidBegin(_ sender: UITextField) {
        
        if sender != enterAddressTxfld
        {
        
        if sender == enterDateTxfld
        {
            datebool = true
            timebool = false
             datePickerView.datePickerMode = UIDatePickerMode.date
            datePickerView.minimumDate = Date()
        }
        else
        {
            timebool = true
            datebool = false
            datePickerView.datePickerMode = UIDatePickerMode.time
        }
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        }
        isKeyboardUp = true
    }
    
    @IBAction func editingDidEnd(_ sender: Any) {
        isKeyboardUp = false
        sheetHeight = storedSheetHeight
            didUpdateSheetHeight()
    }
}
class MakeanOffer: UIViewController, HalfSheetPresentableProtocol,HalfSheetTopVCProviderProtocol {
    
    var userdetailsdic = [String:String]()
    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }
    
    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()
    
    private var isKeyboardUp = false {
        didSet {
            let keyboardHeight: CGFloat = isKeyboardUp ? 300.0 : 0.0
            sheetHeight = 250.0 + keyboardHeight
            didUpdateSheetHeight()
        }
    }
    
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var enterTimeTxfld: UITextField!
    
    @IBOutlet weak var enterDateTxfld: UITextField!
    @IBOutlet weak var enterAddressTxfld: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var subViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var visittoSellerSwitch: UISwitch!
    @IBOutlet weak var sellerTovistme: UISwitch!
    @IBOutlet weak var textField: UITextField?
    var storedSheetHeight = CGFloat()
    var sheetHeight: CGFloat? = 250.0
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        submitButton.layer.borderWidth = 0.8
        submitButton.layer.cornerRadius = 10
        submitButton.layer.borderColor = UIColor.lightGray.cgColor
         let color = UIColor(red:0/255, green: 130/255, blue: 193/255, alpha: 1.0)
        AlWrapper.setupTextfieldLeftView(imageString: "offer.png",textfield:textField!, backgroundcolor: color,backgroundbool: true)
      //  subView.isHidden = true
      //  sheetHeight = visittoSellerSwitch.frame.maxY + 100
       // didUpdateSheetHeight()
        
    }
    
    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }
    
    @IBAction func didTapOnSubbmitButton(_ sender: Any) {
    if textField?.text?.isEmpty == true
    {
          AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Enter Amount")
        }
        else
    {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        // let yourDate = formatter.date(from: myString)
        let userdetails = ["Fname":userdetailsdic["Firstname"]!,"UserId":userdetailsdic["userID"]!,"Address":"","cDate":myString,"Visit":0,"sellerID":userdetailsdic["selecedsellerId"]!,"Amt":textField?.text! ?? "0","Type":3,"ProductID":userdetailsdic["proid"]!,"UserEmail":userdetailsdic["email_id"]!,"UserName":userdetailsdic["UserName"]!,"scheduledate":"","ModelName":userdetailsdic["modelName"]!] as [String : Any]
        
        AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"insertDiscountPrice", viewController: self, params: userdetails as [String : AnyObject], headers: nil, success: { (json, responce) in
            print(json)
            self.dismiss()
            
            NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"MakeanOffer","responce":"\(json)"])
            
        }) { (erro) in
            
            print(erro)
            self.dismiss()
            
            NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"MakeanOffer","responce":"\(erro)"])
        }
        }
    }
    @IBAction func dismiss() {
        dismiss(animated: true)
    }
    
    @IBAction func editingDidBegin(_ sender: Any) {
        isKeyboardUp = true
    }
    
    @IBAction func editingDidEnd(_ sender: Any) {
        isKeyboardUp = false
//        sheetHeight = storedSheetHeight
//        didUpdateSheetHeight()
    }
}


/*Download Class Starts From Here*/

class Download: UIViewController, HalfSheetPresentableProtocol,HalfSheetTopVCProviderProtocol {
    
    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }
    
    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()
    
    private var isKeyboardUp = false {
        didSet {
            let keyboardHeight: CGFloat = isKeyboardUp ? 300.0 : 0.0
            sheetHeight = 250.0 + keyboardHeight
            didUpdateSheetHeight()
        }
    }
    

    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var subViewHeightConstraint: NSLayoutConstraint!
    var productdetails = JSON()
    var storedSheetHeight = CGFloat()
    var sheetHeight: CGFloat? = 200.0
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        submitButton.layer.borderWidth = 0.8
        submitButton.layer.cornerRadius = 10
        submitButton.layer.borderColor = UIColor.lightGray.cgColor
        //  subView.isHidden = true
        //  sheetHeight = visittoSellerSwitch.frame.maxY + 100
        // didUpdateSheetHeight()
        
    }
    
    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }
    
    @IBAction func didTapOnSubbmitButton(_ sender: Any) {
        
//            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Not Implemented!")
            dismiss(animated: true)
//        http://loan2buy.com/api/master/Download_Quote/?sellerid="+sellerID+"&userid="+user_Id+"&productid="+productId+"&ModelNo="+modelNo+"&pmtype="+financeOrCash
         print(appdelegate?.parseDatadic)
        print(productdetails)
      let productid = productdetails[0]["prodId"].string!
         let sellerID = productdetails[0]["sellerId"].string!
//        Optional(["Pro_Used_New": "1", "modelId": "1", "Pmttype": "1", "cityId": "9", "makeID": "1", "Pcno": "1"])
//         _usercode=\(UserDefaults.standard.string(forKey: "_User_Id")!)&EmailId=\(UserDefaults.standard.string(forKey: "_EmailId")!)&mobilenumber=\(UserDefaults.standard.string(forKey: "_Mobile_No")!)&Productid=\(productList!["prodId"].string!)&Modelid=\(parseDatadic!["modelId"] ?? "1")
        let str = "pmtype=Cash&ModelNo=\(appdelegate?.parseDatadic["makeID"] ?? "1")&productid=\(productid)&sellerid=\(sellerID)&userid=\(UserDefaults.standard.string(forKey: "_User_Id")!)"
        
        AlWrapper.requestGETURL(My_Apis.MoterMart_api+"Download_Quote/?"+str, viewController: self, success: { (json2,responce) in
            print(json2)
            if json2.isEmpty == true
            {
                var str = json2.string as! String
                str.remove(at: str.startIndex)
                let url = URL(string: "http://loan2buy.com" + str)
               // let endindex = str.last
                self.DownloadDocumnt(url: url!, filename: "\(productid)")

            }
            else
            {
                
//                appdelegate?.setloanQuestiondetails.Loanquestiondetails = json2[0]
//                self.performSegue(withIdentifier: "preapprovalSegue", sender:nil)
            }
        }, failure: { (error) in
            
        })
        
//        http://loan2buy.com/api/master/Download_Quote/?sellerid="+sellerID+"&userid="+user_Id+"&productid="+productId+"&ModelNo="+modelNo+"&pmtype="+financeOrCash
       
    }
    
    func DownloadDocumnt(url: URL,filename:String)
    {
    //    print("Selected URL: \(self.SelectedDownloadURL)")
        let fileURL = url
        
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("Motor specification-\(filename).pdf")
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil
            {
                if let statusCode = (response as? HTTPURLResponse)?.statusCode
                {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                do
                {
                    if(FileManager.default.fileExists(atPath: destinationFileUrl.path))
                    {
                        try FileManager.default.removeItem(at: destinationFileUrl)
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    }
                    else
                    {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    }
                    
                    if let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents")
                    {
                        
                        if(!FileManager.default.fileExists(atPath: iCloudDocumentsURL.path, isDirectory: nil))
                        {
                            try FileManager.default.createDirectory(at: iCloudDocumentsURL, withIntermediateDirectories: true, attributes: nil)
                        }
                    }
                    
                    let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents").appendingPathComponent("Motor specification-\(filename)")
                    self.dismiss()

                    NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"Download","FilePath":"\(destinationFileUrl)"])
                    print(iCloudDocumentsURL as Any)
                    
                    if let iCloudDocumentsURL = iCloudDocumentsURL
                    {
                        var isDir:ObjCBool = false
                        if(FileManager.default.fileExists(atPath: iCloudDocumentsURL.path, isDirectory: &isDir))
                        {
                            try FileManager.default.removeItem(at: iCloudDocumentsURL)
                            try FileManager.default.copyItem(at: tempLocalUrl, to: iCloudDocumentsURL)
                        }
                        else
                        {
                            try FileManager.default.copyItem(at: destinationFileUrl, to: iCloudDocumentsURL)
                        }
                    }
                    
                }
                catch (let writeError)
                {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            }
            else
            {
                print("Error took place while downloading a file. Error description");
            } 
        }
        task.resume()
    }

    
    
    
    
    func copyDocumentsToiCloudDrive() {
        let localDocumentsURL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: .userDomainMask).last! as NSURL
        let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents")
        
        if let url = iCloudDocumentsURL, !FileManager.default.fileExists(atPath: url.path, isDirectory: nil) {
            do {
                try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func dismiss() {
        dismiss(animated: true)
    }
    
    @IBAction func editingDidBegin(_ sender: Any) {
        isKeyboardUp = true
    }
    
    @IBAction func editingDidEnd(_ sender: Any) {
        isKeyboardUp = false
        //        sheetHeight = storedSheetHeight
        //        didUpdateSheetHeight()
    }
}



/*bookantestdrive Class Starts From Here*/



class bookantestdrive: UIViewController, HalfSheetPresentableProtocol, HalfSheetTopVCProviderProtocol {
    
    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }
    
    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()
    
    private var isKeyboardUp = false {
        didSet {
            let keyboardHeight: CGFloat = isKeyboardUp ? 300.0 : 0.0
            sheetHeight = 250.0 + keyboardHeight
            didUpdateSheetHeight()
        }
    }
    var datebool : Bool = false
    var timebool : Bool =  false
    
    var userdetailsdic = [String:String]()
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var enterTimeTxfld: UITextField!
    
    @IBOutlet weak var enterDateTxfld: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var subViewHeightConstraint: NSLayoutConstraint!
    var storedSheetHeight = CGFloat()
    var sheetHeight: CGFloat? = 200.0
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        subView.isHidden = true
        submitButton.layer.borderWidth = 0.8
        submitButton.layer.cornerRadius = 10
        submitButton.layer.borderColor = UIColor.lightGray.cgColor
         let color = UIColor(red:0/255, green: 130/255, blue: 193/255, alpha: 1.0)
        AlWrapper.setupTextfieldLeftView(imageString: "time.png",textfield:enterTimeTxfld, backgroundcolor: color,backgroundbool: true)
        AlWrapper.setupTextfieldLeftView(imageString: "calendar.png",textfield:enterDateTxfld, backgroundcolor: color,backgroundbool: true)
        sheetHeight =  subView.frame.maxY
        storedSheetHeight = sheetHeight!
        didUpdateSheetHeight()
        
    }
   
    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }
  
    @IBAction func didTapOnSubbmitButton(_ sender: Any) {
        if enterDateTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Date")
        }
        else if enterTimeTxfld.text?.isEmpty == true
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please Select the Time")
        }
        else
        {
//            AlWrapper.showAlertMessage(vc: self, titleStr: "Sucess!", messageStr: "Request Taken sucessfully")
//            dismiss(animated: true)
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let myString = formatter.string(from: Date()) // string purpose I add here
            // convert your string to date
            // let yourDate = formatter.date(from: myString)
            
            let userdetails = ["Fname":userdetailsdic["Firstname"]!,"UserId":userdetailsdic["userID"]!,"Address":"","cDate":myString,"Visit":2,"sellerID":userdetailsdic["selecedsellerId"]!,"Amt":0,"Type":0,"ProductID":userdetailsdic["proid"]!,"UserEmail":userdetailsdic["email_id"]!,"UserName":userdetailsdic["UserName"]!,"scheduledate":enterDateTxfld.text! + enterTimeTxfld.text!,"ModelName":userdetailsdic["modelName"]!] as [String : Any]
            
            AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+"insertAppointment", viewController: self, params: userdetails as [String : AnyObject], headers: nil, success: { (json, responce) in
                print(json)
                self.dismiss()
                
                NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "TabBarDissmissed"), object:nil, userInfo: ["ClassName":"bookantestdrive","responce":"\(json)"])
                
            }) { (erro) in
                
                print(erro)
            }
        }
     //   dismiss(animated: true)
       
        
    }
    @IBAction func dismiss() {
        dismiss(animated: true)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        if datebool == true
        {
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            
            self.enterDateTxfld.text = dateFormatter.string(from: sender.date)
        }
        if timebool == true
        {
            dateFormatter.dateStyle = DateFormatter.Style.none
            
            dateFormatter.timeStyle = DateFormatter.Style.medium
            
            self.enterTimeTxfld.text = dateFormatter.string(from: sender.date)
        }
        
        
    }
     let datePickerView:UIDatePicker = UIDatePicker()
    
    @IBAction func editingDidBegin(_ sender: UITextField) {
        
  
            
            if sender == enterDateTxfld
            {
                  datebool = true
                timebool = false
                datePickerView.datePickerMode = UIDatePickerMode.date
                datePickerView.minimumDate = Date()
            }
            else
            {
                timebool = true
                datebool = false
                datePickerView.datePickerMode = UIDatePickerMode.time
            }
            sender.inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        

        isKeyboardUp = true
    }
    
    @IBAction func editingDidEnd(_ sender: Any) {
        isKeyboardUp = false
        sheetHeight = storedSheetHeight
        didUpdateSheetHeight()
    }
}
private let arrayParametersKey = "arrayParametersKey"
extension Array {
    /// Convert the receiver array to a `Parameters` object.
    func asParameters() -> Parameters {
        return [arrayParametersKey: self]
    }
}



protocol Accessoriesdelegate:class{
    func didtapOnSavebutton()
}
class Accessories: UIViewController, HalfSheetPresentableProtocol, HalfSheetTopVCProviderProtocol ,UITableViewDelegate,UITableViewDataSource{
    var parsedataList = JSON()
    var sellerId = String()
    var productId = String()
    var sentArray = Array<[String:String]>()
    weak var coredelegate : Accessoriesdelegate!

    @IBAction func didTaponSaveButton(_ sender: Any) {
        
      //  let dic = ["work":"work"]

        let url = "http://loan2buy.com/api/master/INSERT_FITTING_BY_USERID_LIST/"
      //  var parms = sentArray as [[String:String]]
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let values = sentArray
        print(values)
        request.httpBody = try! JSONSerialization.data(withJSONObject: values)
        
        
        
        
        Alamofire.request(url, method: .post, parameters: values.asParameters(), encoding: ArrayEncoding(), headers: nil).responseJSON { (responseObject) -> Void in
            print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                print()
                if responseObject.response?.statusCode == 400
                {
                    
                }
                else
                {
                    print(responseObject.result)
                    print(responseObject.data as Any)
                    print(responseObject.result.value as Any)
                    //   success(resJson,responseObject)
                    let modelid = appdelegate?.parseDatadic["modelId"]
                    let userId = UserDefaults.standard.string(forKey: "_User_Id")
                    self.view.removeBluerLoader()
                    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["Renish":"Dadhaniya"])
                    let dataString = "SellerId=\(self.sellerId)&ModelNo=\(modelid!)&UserId=\(userId!)&PId=\(self.productId)"
                    AlWrapper.requestGETURL(My_Apis.MoterMart_api+"GET_Accessories?"+dataString, viewController: self, success: { (json2,responce) in
                        print(json2)
                        let tap = ["userinfo":json2]
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo:tap)
                        self.dismiss(animated: true)
                        
                    }, failure: { (error) in
                        
                    })


                }
               
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
            //    failure(error)
                self.view.removeBluerLoader()
            }
        }
      //  coredelegate?.didtapOnSavebutton()
    }
    
    public struct ArrayEncoding: ParameterEncoding {
        
        /// The options for writing the parameters as JSON data.
        public let options: JSONSerialization.WritingOptions
        
        
        /// Creates a new instance of the encoding using the given options
        ///
        /// - parameter options: The options used to encode the json. Default is `[]`
        ///
        /// - returns: The new instance
        public init(options: JSONSerialization.WritingOptions = []) {
            self.options = options
        }
        
        public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
            var urlRequest = try urlRequest.asURLRequest()
            
            guard let parameters = parameters,
                let array = parameters[arrayParametersKey] else {
                    return urlRequest
            }
            
            do {
                let data = try JSONSerialization.data(withJSONObject: array, options: options)
                
                if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                    urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                
                urlRequest.httpBody = data
                
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
            
            return urlRequest
        }
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }
    
    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()
    
    private var isKeyboardUp = false {
        didSet {
            let keyboardHeight: CGFloat = isKeyboardUp ? 300.0 : 0.0
            sheetHeight = 250.0 + keyboardHeight
            didUpdateSheetHeight()
        }
    }
    

    var storedSheetHeight = CGFloat()
    var sheetHeight: CGFloat? = 500
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
      print(parsedataList)
        
    }
    var butindexPath = IndexPath()
    var addDeleteBtnArr = [Int]()
    @IBAction func didTapOnCheckButton(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to:self.tableView )
        butindexPath = tableView.indexPathForRow(at: buttonPosition)!
        
//        "seller_product_id":"VID2018140620180314221759976","product_category":"1","product_type":"1","User_Code":"VID20181406","model":"1","cityAvailable":"9","fitting":"19","User_id":"1845","base_price":"200","cheked":"1"
        var dic = parsedataList[butindexPath.row]
        var sentdic = [String:String]()
        sentdic["product_type"] = "1"
        sentdic["cityAvailable"] = appdelegate?.parseDatadic["cityId"]
        sentdic["product_category"] = appdelegate?.parseDatadic["Pcno"]
        sentdic["User_id"] = UserDefaults.standard.string(forKey: "_User_Id")
        sentdic["User_Code"] = UserDefaults.standard.string(forKey: "_User_code")
        sentdic["seller_product_id"] = dic["product_id"].string
        sentdic["fitting"] = dic["accessoryId"].string
        sentdic["base_price"] = dic["unitPrice"].string
        
        if addDeleteBtnArr.contains(butindexPath.row)
        {
            let index = addDeleteBtnArr.index(of: butindexPath.row)
            dic["isdefault"] = "False"
            sentdic["cheked"] = "0"
            self.addDeleteBtnArr.remove(at:index!)
        }
        else
        {
             self.addDeleteBtnArr.append(butindexPath.row)
            dic["isdefault"] = "True"
            sentdic["cheked"] = "1"
        }
   //     print(sentdic)
        parsedataList[butindexPath.row] = dic
//        if sentArray.contains(sentdic)
//        {
//            sentArray.append(sentdic)
//        }
//        let index = addDeleteBtnArr.index(of: butindexPath.row)
//
        let str = dic["accessoryId"].string!
        var index:Int?
             print("beforing appending\(sentArray)")
        for i in 0..<sentArray.count
        {
            let indic = sentArray[i]
            print(indic["fitting"] as Any )
            if indic["fitting"] == str
            {
                index = i
            }
        }
        print("")
        
       if index != nil
       {
        
        sentArray.remove(at: index!)
        }
//        let index = sentArray.index {
//            if let dic1 = $0 as? [String:String] {
//                if let value = dic1["sell_product_id"], value == str{
//                    return true
//                }
//            }
//            return false
//        }
        print(index)
        print("after removing\(sentArray)")
        sentArray.append(sentdic)
        print("after appending\(sentArray)")
        self.tableView.reloadData()
    }
    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (parsedataList.array?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell:acessoriesCell = self.tableView.dequeueReusableCell(withIdentifier: "acessoriesCell") as! acessoriesCell
        let dic = parsedataList[indexPath.row].dictionary
        
        cell.typeLable.text = dic!["Descriotion"]?.string
        cell.valueLable.text =  dic!["unitPrice"]?.string
        if Float((dic!["unitPrice"]?.string)!) != nil
        {
            let val = Int(round(Float((dic!["unitPrice"]?.string)!)!))
            cell.valueLable.text =  "\(val)"
        }
        else
        {
            cell.valueLable.text =  dic!["unitPrice"]?.string
        }

        if dic!["isdefault"]?.string == "True"
        {
            addDeleteBtnArr.append(indexPath.row)
        }
        if addDeleteBtnArr.contains(indexPath.row)
        {
           // cell.add.image = UIImage(named: "RemoveImg.png")!
            cell.checkButton.setImage(UIImage(named: "check.png"), for: .normal)
           
            
        }else{
             cell.checkButton.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
            
            return cell
            
        }

}


