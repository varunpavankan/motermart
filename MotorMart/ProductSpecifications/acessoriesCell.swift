//
//  acessoriesCell.swift
//  MotorMart
//
//  Created by Nennu on 10/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class acessoriesCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var valueLable: UILabel!
    @IBOutlet weak var typeLable: UILabel!
    @IBOutlet weak var adddeleteacessoriesSwitch: UISwitch!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
