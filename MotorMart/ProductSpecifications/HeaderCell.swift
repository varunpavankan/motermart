//
//  HeaderCell.swift
//  MotorMart
//
//  Created by Nennu on 09/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {
    @IBOutlet weak var header_lbl: UILabel!
    @IBOutlet weak var header_btn: UIButton!
    
    @IBOutlet weak var scrngCount_lbl: UILabel!
    
    @IBOutlet weak var arrowUpDown_img: UIImageView!
    
    @IBOutlet weak var headerDate_lbl: UILabel!
    
    @IBOutlet weak var headerView_obj: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
