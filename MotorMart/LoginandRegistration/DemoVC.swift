//
//  DemoVC.swift
//  MotorMart
//
//  Created by Nennu on 08/07/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class DemoVC: UIViewController, UIScrollViewDelegate  {
    @IBOutlet weak var howItWorkLable: UILabel!
    let scrollView = UIScrollView(frame: CGRect(x:0, y:0, width:320,height: 300))
    var pageint = 0
    var colors:[UIColor] = [UIColor.red, UIColor.blue, UIColor.green, UIColor.yellow]
    var imagearray = ["select_vehicle.png","get_quotes.png","provide_details.png","upload_documents.png","get_approval.png"]
    let stepArray = ["Step 1","Step 2","Step 3","Step 4","Step 5"]
    var titlearray = ["SELECT VEHICLE","GET QUOTES","PROVIDE DETAILS","UPLOAD DOCUMENTS","GET APPROVAL"]
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
   // var pageControl : UIPageControl = UIPageControl(frame: CGRect(x:50,y: 300, width:200, height:50))
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
//        pageControl.frame = CGRect(x: self.view.frame.size.width/2 - 100, y: scrollView.frame.maxY + 20 , width: 200, height: 50)
        configurePageControl()
        
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        self.view.addSubview(scrollView)
        for index in 0..<5 {
            
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
            frame.size = self.scrollView.frame.size
            
            let subView = UIView(frame: frame)
            // imageview.frame = subView.frame
            let StepLable = UILabel(frame: CGRect(x:subView.frame.midX - 175, y:howItWorkLable.frame.maxY + 10, width:350,height: 50))
            StepLable.text = stepArray[index]
            StepLable.textAlignment = .center
            StepLable.textColor = UIColor.white
            StepLable.font = UIFont.boldSystemFont(ofSize: 20.0)
            self.scrollView.addSubview(StepLable)
            
            let imageview = UIImageView(frame: CGRect(x:subView.frame.midX - 100, y:StepLable.frame.maxY+10, width:200,height: 200))
            imageview.image = UIImage(named:imagearray[index])
            
            imageview.layer.cornerRadius = imageview.frame.size.width/2
            imageview.layer.masksToBounds = true
            // subView.backgroundColor = colors[index]
            self.scrollView .addSubview(subView)
            self.scrollView .addSubview(imageview)
            let titlelabel = UILabel(frame: CGRect(x:subView.frame.midX - 175, y:imageview.frame.maxY + 10, width:350,height: 50))
            titlelabel.text = titlearray[index]
            titlelabel.textAlignment = .center
            titlelabel.textColor = UIColor.white
            titlelabel.font = UIFont.boldSystemFont(ofSize: 20.0)
            self.scrollView.addSubview(titlelabel)
            
        }
        
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.size.width * 5,height: self.scrollView.frame.size.height)
//        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
//        self.pageControl.numberOfPages = colors.count
//        self.pageControl.currentPage = 0
//        self.pageControl.tintColor = UIColor.red
//        self.pageControl.pageIndicatorTintColor = UIColor.black
//        self.pageControl.currentPageIndicatorTintColor = UIColor.green
//        self.view.addSubview(pageControl)
        
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @objc func changePage(sender: AnyObject) -> () {
//        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
//        scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
        
    }
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
//        pageControl.currentPage = Int(pageNumber)
        
    }
    @IBAction func didTapOnSkipButton(_ sender: Any) {
    }
    @IBAction func didTapOnNextButton(_ sender: Any) {
        
        pageint = pageint + 1
        if pageint < 5
        {
            let x = CGFloat(pageint) * scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
        }
        else
        {
           // pageint = 0
        }
      
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
//        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
//        pageControl.currentPage = Int(pageNumber)
        
    }
    
}
