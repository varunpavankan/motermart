//
//  LoginViewController.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var PasswordTextfield: ACFloatingTextfield!
    @IBOutlet weak var mobileNumberTextField: ACFloatingTextfield!
    override func viewDidLoad() {
        super.viewDidLoad()
            self.navigationController?.navigationBar.isHidden = false
//        AlWrapper.setupTextfieldLeftView(imageString: "if_ic_message.png", textfield: mobileNumberTextField)
        // Do any additional setup after loading the view.
    }
    @IBAction func didTapOnLoginButton(_ sender: Any) {
        
        if mobileNumberTextField.text == ""
        {
            AlWrapper.showAlertMessage(vc: self, titleStr:"Alert!", messageStr:"Please enter mobile number")
        }
        else if PasswordTextfield.text == ""
        {
            AlWrapper.showAlertMessage(vc: self, titleStr:"Alert!", messageStr:"Please enter Password!")
        }
        else
        {
            let usertoken = "Authenticate_User"
            let parameters = ["Mobile_No":mobileNumberTextField.text as Any,"Password":PasswordTextfield.text as Any] as [String : AnyObject]?
            AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+usertoken, viewController: self, params: parameters, headers: nil, success: { (json, responce) in
                print(json)
                if json == 400
                {
                  AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter the valid mobile number and password")
                }
                else
                {
                    
             
                   
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mainViewController = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
                    //    mainViewController.mobileNumber = self.mobileNumberTextField.text!
                       mainViewController.mobileNumber = self.mobileNumberTextField.text!
                    let leftViewController = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                    
                    //            let nvc: ENSideMenuNavigationController = ENSideMenuNavigationController.init(menuViewController: leftViewController, contentViewController: mainViewController)
                    
                    let nvc:MyNavigationController = MyNavigationController.init(menuViewController: leftViewController, contentViewController: mainViewController)
                    UINavigationBar.appearance().barTintColor = UIColor(red:1/255, green: 82/255, blue: 135/255, alpha: 1.0)
                    UINavigationBar.appearance().tintColor = UIColor.white
                    nvc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
                    UserDefaults.standard.set(true, forKey: "Login")
                    
                    UIApplication.shared.delegate?.window??.rootViewController = nvc
                }
            }) { (erro) in
                
                print(erro)
            }
            
        }
    

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
