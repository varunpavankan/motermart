//
//  RegistrationViewController.swift
//  MotorMart
//
//  Created by NanoHealth on 14/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    @IBOutlet weak var reenterPasswordTxfld: ACFloatingTextfield!
    
    @IBOutlet weak var enterPasswordTxfld: ACFloatingTextfield!
    @IBOutlet weak var enterEmailTxfld: ACFloatingTextfield!
    @IBOutlet weak var enterMobileTxfld: ACFloatingTextfield!
    @IBOutlet weak var enterNameTxfld: ACFloatingTextfield!
    override func viewDidLoad() {
        super.viewDidLoad()
            self.navigationController?.navigationBar.isHidden = false

        // Do any additional setup after loading the view.
    }
    @IBAction func didTapOnRegistrationButton(_ sender: Any) {
        if enterNameTxfld.text == ""
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "please enter Name")
            
        }
        else if enterMobileTxfld.text == ""
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "please enter Mobile Number")
        }
        else if enterEmailTxfld.text == ""
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "please enter Email ID")

        }
        else if enterPasswordTxfld.text == ""
        {
             AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "please enter Password")
        }
        else if reenterPasswordTxfld.text == ""
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "please enter Re-enter Password")
        }
        else if enterPasswordTxfld.text != reenterPasswordTxfld.text
        {
            AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "please check password and confrim password mismatchs")
        }
        else
        {
            let  bool = isValidEmail(testStr: enterEmailTxfld.text!)
            if bool == false
            {
                AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Please enter a Valid Email ID")
            }
            else{
                
                let registrationUrl = "registeruser"
                let parameters = ["_EmailId":enterEmailTxfld.text!,"_Mobile_No":enterMobileTxfld.text!,"_User_name":enterNameTxfld.text!,"_Password":enterPasswordTxfld.text!,"_OTP":"1234"] as [String : String]?
                
                print(parameters!)
             //    AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Sucess!")
                
                AlWrapper.requestPOSTURL(My_Apis.MoterMart_api+registrationUrl, viewController: self, params: parameters! as [String : AnyObject] , headers: nil, success: { (json, responce) in
                    print(json)
                    if json == 0
                    {
                        AlWrapper.showAlertMessage(vc: self, titleStr: "Alert!", messageStr: "Mobile Number Alredy Exits Please Try Again")
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Success", message: "Registration Completed Please Login!", preferredStyle: UIAlertControllerStyle.alert);
                        let OK = UIAlertAction(
                            title: "Ok",
                            style: .default){ action -> Void in
                                self.navigationController?.popViewController(animated: false)
                                //other action
                        }
                        alert.addAction(OK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }) { (erro) in
                    
                    print(erro)
                }
            }
            
            
           
            
            
        }
        
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
