//
//  AlWrapper.swift
//  fatpages
//
//  Created by NanoHealth on 23/06/18.
//  Copyright © 2018 NanoHealth. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class AlWrapper: NSObject {
    
    //TODO :-
    /* Handle Time out request alamofire */
    class func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
    let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert);
    let OK = UIAlertAction(
        title: "Ok",
        style: .default){ action -> Void in
            
            //other action
    }
    alert.addAction(OK)
    vc.present(alert, animated: true, completion: nil)
    }
    
    
    
    class func setupTextfieldLeftView(imageString:String,textfield:UITextField,backgroundcolor:UIColor,backgroundbool:Bool)
    {
       
            let imageView = UIImageView()
            let view = UIView()
            let image = UIImage(named: imageString)
            imageView.image = image
            imageView.tintColor = UIColor.blue
       
        
       // imageView.contentMode = .scaleAspectFill
                view.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            imageView.frame = CGRect(x: 5, y: 5, width: 30, height: 30)
        if backgroundbool == true
        {
            view.backgroundColor = backgroundcolor
        }
            view.addSubview(imageView)
       // imageView.contentMode = .scaleAspectFit
            textfield.leftView = view
            textfield.leftViewMode = .always
      
    }
    
    class func setupTextfieldLeftViewwithborder(imageString:String,textfield:UITextField,backgroundcolor:UIColor,backgroundbool:Bool)
    {
        
        let imageView = UIImageView()
        let view = UIView()
        let image = UIImage(named: imageString)
        imageView.image = image
        imageView.tintColor = UIColor.blue
        
        
        // imageView.contentMode = .scaleAspectFill
        view.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
         let color = UIColor(red:122/255, green: 192/255, blue: 228/255, alpha: 1.0)
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = 0.9
        view.clipsToBounds = true
        imageView.frame = CGRect(x: 5, y: 5, width: 30, height: 30)
        imageView.tintColor = UIColor.blue
        if backgroundbool == true
        {
            view.backgroundColor = backgroundcolor
        }
        view.addSubview(imageView)
        // imageView.contentMode = .scaleAspectFit
        textfield.leftView = view
        textfield.leftViewMode = .always
        
    }
    class func setupTextfieldrightViewwithborder(imageString:String,textfield:UITextField,backgroundcolor:UIColor,backgroundbool:Bool)
    {
        
        let imageView = UIImageView()
        let view = UIView()
        let image = UIImage(named: imageString)
        imageView.image = image
        imageView.tintColor = UIColor.blue
        
        
        // imageView.contentMode = .scaleAspectFill
        view.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
      //  let color = UIColor(red:122/255, green: 192/255, blue: 228/255, alpha: 1.0)
       // view.layer.borderColor = color.cgColor
      //  view.layer.borderWidth = 0.9
       // view.clipsToBounds = true
        imageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
        imageView.tintColor = UIColor.blue
        if backgroundbool == true
        {
            view.backgroundColor = backgroundcolor
        }
        view.addSubview(imageView)
        // imageView.contentMode = .scaleAspectFit
        textfield.rightView = view
        textfield.rightViewMode = .always
        
    }
    
  class  func replaceNullValues(dictionary:JSON) ->  Dictionary<String,String>
    {
        // var dic = dictionary.dictionary
        var dict = Dictionary<String,String>()
        
        for (key, value) in dictionary
        {
            if (value == JSON.null) {
                dict[key] = "null"
                
            }
            else
            {
                dict[key] = value.string
            }
            
        }
        
        return dict
    }
    
 class  func emiCalculation_flaterate_typ(loanAmount:Float,intrest:Float,numberOfmonths:Int) -> Float
    {
        
        let TotalInterest = (loanAmount * intrest/100 * Float(numberOfmonths)) / 12;
        let TotalPayable = loanAmount + TotalInterest;
        let EMI = TotalPayable / Float(numberOfmonths);
        return EMI;
        // return d
    }
class    func emiCalculation_Reducing_balance_type(loanAmount:Float,intrest:Float,numberOfmonths:Int) -> Float
    {
        
        let permonth = (intrest / 100) / 12;
        print(permonth)
        let Interestrate_powerterms = Float(pow(Double(1+permonth), Double(numberOfmonths)))
        print(Interestrate_powerterms)
        let payment = (Float(loanAmount) * (Interestrate_powerterms * Float(permonth)) ) / (( Interestrate_powerterms) - 1);
        return payment;
    }
    
    
    
    
    class func requestGETURL(_ strURL: String, viewController: UIViewController,success:@escaping (JSON,DataResponse<Any>) -> Void, failure:@escaping (Error) -> Void)
    {
        viewController.view.showBlurLoader()
       // self.callLoader(viewController)
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                //let title = resJson["title"].string
                //print(title!)
                success(resJson, responseObject)
                viewController.view.removeBluerLoader()
                 viewController.view.removeBluerLoader()
            }
            
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func requestPOSTURL(_ strURL : String,viewController: UIViewController, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON,DataResponse<Any>) -> Void, failure:@escaping (Error) -> Void){
         viewController.view.showBlurLoader()
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 1000 // seconds
      //  configuration.timeoutIntervalForResource = 1000
        let alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        alamoFireManager.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
             let _ = alamoFireManager
            print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                print(responseObject.result)
                print(responseObject.data as Any)
                print(responseObject.result.value as Any)
                success(resJson,responseObject)
                 viewController.view.removeBluerLoader()
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
                 viewController.view.removeBluerLoader()
            }
        }
    }
}
extension UIView{
    func showBlurLoader(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        blurEffectView.contentView.addSubview(activityIndicator)
        activityIndicator.center = blurEffectView.contentView.center
        
        self.addSubview(blurEffectView)
    }
    
    func removeBluerLoader(){
        self.subviews.flatMap {  $0 as? UIVisualEffectView }.forEach {
            $0.removeFromSuperview()
        }
    }
}
extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
extension Float {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
extension Float {
    func roundTo(places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}
